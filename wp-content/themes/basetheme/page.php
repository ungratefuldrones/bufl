
<?php get_header(); ?>

<div id="pageWrapper" <?php if ( embed() ) echo "class='embedded-tool'"; ?> >

	<div id="pageTitle">
		<?php $legal = (strpos($_SERVER["REQUEST_URI"], 'legal') ? $legal = true : $legal = false); ?>
		<?php if ($legal) : ?>
			<a class="back-to-tool" href="<?php echo (pll_current_language() == 'en' ? "/".get_embed_url() : "/es/".get_embed_url()); ?>">&#8249; Back to Tool</a>
		<?php endif ?>
		<h1><?php the_title(); ?></h1>
	</div>

	<div id="pageContent" class="clearfix">
		<div id="pageImage">
			<div id="pledgeButton">
				<a href="<?php the_field('pledge_button_link'); ?>"><img src="<?php the_field('pledge_button'); ?>" /></a>
			</div>
			<img src="<?php the_field('page_image'); ?>" class="pageImageLarge"/>
			<img src="<?php the_field('page_image_mobile'); ?>" class="pageImageSmall"/>
		</div>

		<?php if (get_field('page_top_content')) { ?>
			 <div id="pageTop" class="clearfix">
				<?php the_field('page_top_content'); ?>
			</div>
		<?php } ?>

		<?php if (get_field('page_left_content')) { ?>
		<div id="pageLeft">
			<?php the_field('page_left_content'); ?>
		</div>
		<?php } ?>

		<?php if (get_field('page_sidebar_content')) { ?>
		<div id="pageSidebar">
			<?php the_field('page_sidebar_content'); ?>
		</div>
		<?php } ?>

		<?php if (get_field('twitter_widget') || get_field('facebook_widget')) { ?>

		<div id="pageWidgets">

			<?php if (get_field('twitter_widget')) { ?>
			<div class="pageWidget">
				<h2>Tweets</h2>
				<a class="twitter-timeline"  href="https://twitter.com/BuckleUpForLife"  data-tweet-limit="5" data-link-color="#ff0000" width="100%" height="600" data-widget-id="372056826268762112" data-chrome="noheader nofooter transparent">Tweets by @BuckleUpForLife</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
			<?php } ?>

			<?php if (get_field('facebook_widget')) { ?>
			<div class="pageWidget">
				<div class="facebookBox">
					<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fbuckleupforlife&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=361848313907575" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:62px; background-color:#d9eaf3;" allowTransparency="true"></iframe>
				</div>

				<div class="facebookBox">
					<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fabrochate&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=361848313907575" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:62px; background-color:#d9eaf3;" allowTransparency="true"></iframe>
				</div>
			</div>
			<?php } ?>

		</div>

		<?php } ?>

	</div>

</div>

<?php get_footer() ?>
