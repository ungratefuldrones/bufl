$(document).ready(function() {
	startKnowSlides();
	setupKnowButtons();
	setupToolButtons();
	setupHeaderButtons();
	setupTabberTabs();
	setupPledgeButton();
	
});

$(window).load(function() {	
	startHeroSlides();
	setupHeroButtons();
	setupHeroSlideColor();

});

///////////////////////////////
///////////////////////////////
//HEADER CODE
///////////////////////////////
///////////////////////////////
function setupHeaderButtons() {
	$("#menuButton").click(function() {
		$("#menu").css("display", "block");
		TweenMax.to("#menu", 1, {autoAlpha:1, ease:Circ.easeOut});
	});
	$("#menuClose").click(function() {
		TweenMax.to("#menu", 1, {autoAlpha:0, ease:Circ.easeOut});
	});
}


///////////////////////////////
///////////////////////////////
//HERO CODE
///////////////////////////////
///////////////////////////////

var totalHeroSlides = null;
var currentHeroSlide = 1;
var previousHeroSlide = null;
var heroSlidesMoving = false;
var heroEaseLength = 2;
var heroEaseType = "Circ.easeInOut";
var firstHeroLoad = true;
var heroTimer = null;
var moveHeroLeft = null;
var moveHeroRight = null;
var cycleOnce = true;

function startHeroSlides() {
	previousHeroSlide = totalHeroSlides = $(".heroSlide").length;
	moveHeroLeft = ($("#heroWrapper").width() * -1) + "px";
	moveHeroRight = $("#heroWrapper").width() + "px";
	for (var i=0;i < totalHeroSlides;i++) {
		var slideNumberToAdd = i+1;
		$("#heroNav").append( "<div id='heroDot"+slideNumberToAdd+"' class='slideDot'>"+slideNumberToAdd+"</div>" );
		if (slideNumberToAdd != 1) {
			$("#heroSlide" + slideNumberToAdd).css("left", moveHeroRight);
		}
	}
	var adjustNav = (($("#heroNav").width() / 2) * -1) + "px";
	$("#heroNav").css("margin-left", adjustNav);
	if ( totalHeroSlides > 1 ) {
		timeHeroSlides();
		//TweenMax.to("#heroNav", 2, {autoAlpha:1, ease:Circ.easeOut});
		
		//TweenMax.to("#heroWrapper .slideArrowLeft", 2, {autoAlpha:1, ease:Circ.easeOut});
		//TweenMax.to("#heroWrapper .slideArrowRight", 2, {autoAlpha:1, ease:Circ.easeOut});
	}
	changeHeroSlideRight(1);
	TweenMax.to("#loadingLogo", 1, {autoAlpha:0, ease:Circ.easeOut});
}

function timeHeroSlides() {
	heroTimer = setTimeout("changeHeroSlides()",8000);
}

function setupHeroButtons() {
	$("#heroNav .slideDot").click(function() {
		var index = $(this).index() + 1; // Index is 0 based
		clearTimeout(heroTimer);
		if (currentHeroSlide > index) {
			changeHeroSlideLeft(index);
		} else if (currentHeroSlide < index) {
			changeHeroSlideRight(index);
		}
	});
	$("#heroWrapper .slideArrowRight").click(function() {
		if (!heroSlidesMoving) {
			heroRightButton();
			clearTimeout(heroTimer);
		}
	});
	$("#heroWrapper .slideArrowLeft").click(function() {
		if (!heroSlidesMoving) {
			heroLeftButton();
			clearTimeout(heroTimer);
		}
	});
	$(".heroSlideContent").click(function() {
		var linkToGoTo = $(this).find("a").attr("href");
		document.location.href = linkToGoTo;
	});
	$("#heroWrapper").on("mouseover", function() {
                TweenMax.to("#heroWrapper .slideArrowLeft", 1, {autoAlpha:1, ease:Circ.easeOut});
                TweenMax.to("#heroWrapper .slideArrowRight", 1, {autoAlpha:1, ease:Circ.easeOut});
        });
        $("#heroWrapper").on("mouseout", function() {
                TweenMax.to("#heroWrapper .slideArrowLeft", 1, {autoAlpha:0, ease:Circ.easeOut});
                TweenMax.to("#heroWrapper .slideArrowRight", 1, {autoAlpha:0, ease:Circ.easeOut});
        });
}


function changeHeroSlides() {
	heroRightButton();
	timeHeroSlides();
	if ( cycleOnce ) {
		if ( currentHeroSlide === totalHeroSlides ) {
			clearTimeout(heroTimer);
		}
	}
}


function heroRightButton() {
	if (currentHeroSlide == totalHeroSlides) {
		changeHeroSlideRight(1);
	} else {
		changeHeroSlideRight(currentHeroSlide+1);
	}
}

function heroLeftButton() {
	if (currentHeroSlide == 1) {
		changeHeroSlideLeft(totalHeroSlides);
	} else {
		changeHeroSlideLeft(currentHeroSlide-1);
	}
}

function changeHeroSlideRight(heroSlideNumber) {
	if (!firstHeroLoad) {
		previousHeroSlide = currentHeroSlide;
		$("#heroNav .slideDot").removeClass("activeDot");
	}
	heroSlidesMoving = true;
	currentHeroSlide = heroSlideNumber;
	$("#heroDot" + currentHeroSlide).addClass("activeDot");
	if (firstHeroLoad) {
		heroSlidesMoving = false;
		TweenMax.to("#heroSlide" + currentHeroSlide, heroEaseLength, {left:"0px", autoAlpha:1, ease:heroEaseType});
		firstHeroLoad = false;
	} else {
		$("#heroSlide" + currentHeroSlide).css("visibility", "visible");
		$("#heroSlide" + currentHeroSlide).css("left", moveHeroRight);
		TweenMax.to("#heroSlide" + currentHeroSlide, heroEaseLength, {left:"0px", ease:heroEaseType});
		TweenMax.to("#heroSlide" + previousHeroSlide, heroEaseLength, {left:moveHeroLeft, ease:heroEaseType, onComplete:hidePreviousHeroSlide});
	}
}

function changeHeroSlideLeft(heroSlideNumber) {
	previousHeroSlide = currentHeroSlide;
	$("#heroNav .slideDot").removeClass("activeDot");
	heroSlidesMoving = true;
	currentHeroSlide = heroSlideNumber;
	$("#heroDot" + currentHeroSlide).addClass("activeDot");
	$("#heroSlide" + currentHeroSlide).css("visibility", "visible");
	$("#heroSlide" + currentHeroSlide).css("left", moveHeroLeft);
	TweenMax.to("#heroSlide" + currentHeroSlide, heroEaseLength, {left:"0px", ease:heroEaseType});
	TweenMax.to("#heroSlide" + previousHeroSlide, heroEaseLength, {left:moveHeroRight, ease:heroEaseType, onComplete:hidePreviousHeroSlide});
}

function hidePreviousHeroSlide() {
	heroSlidesMoving = false;
	$("#heroSlide" + previousHeroSlide).css("visibility", "hidden");
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}  

function setupHeroSlideColor() {
	var rgba_array = new Array();
	
	// heroTxtColor is set in home.php because it's defined per slide in the CMS
	$(heroTxtColor).each(function () {
		rgba_array.push(hexToRgb(this));
	});
	
	var count = 0;
	$('.heroSlideContent').each(function () {
		var bg_css = 'rgba('+rgba_array[count].r+','+rgba_array[count].g+','+rgba_array[count].b+',0.7)';
		$(this).css('background-color', bg_css);
		count += 1;
	});
}


///////////////////////////////
///////////////////////////////
//KNOW CODE
///////////////////////////////
///////////////////////////////

var totalKnowSlides = null;
var currentKnowSlide = 1;
var previousKnowSlide = null;
var knowSlidesMoving = false;
var knowEaseLength = 2;
var knowEaseType = "Circ.easeInOut";
var firstKnowLoad = true;
var knowTimer = null;
var moveKnowLeft = null;
var moveKnowRight = null;

function startKnowSlides() {
	previousKnowSlide = totalKnowSlides = $(".knowSlide").length;
	moveKnowLeft = ($("#knowWrapper").width() * -1) + "px";
	moveKnowRight = $("#knowWrapper").width() + "px";
	for (var i=0;i < totalKnowSlides;i++) {
		var slideNumberToAdd = i+1;
		$("#knowNav").append( "<div id='knowDot"+slideNumberToAdd+"' class='slideDot'>"+slideNumberToAdd+"</div>" );
		if (slideNumberToAdd != 1) {
			$("#knowSlide" + slideNumberToAdd).css("left", moveKnowRight);
		}
	}
	var adjustNav = (($("#heroNav").width() / 2) * -1) + "px";
	$("#knowNav").css("margin-left", adjustNav);
	if ( totalKnowSlides > 1 ) {
		//timeKnowSlides();
		TweenMax.to("#knowNav", 2, {autoAlpha:1, ease:Circ.easeOut});
		TweenMax.to("#knowWrapper .slideArrowLeft", 2, {autoAlpha:1, ease:Circ.easeOut});
		TweenMax.to("#knowWrapper .slideArrowRight", 2, {autoAlpha:1, ease:Circ.easeOut});
	}
	changeKnowSlideRight(1);
	TweenMax.to("#loadingLogo", 1, {autoAlpha:0, ease:Circ.easeOut});
}

function timeKnowSlides() {
	knowTimer = setTimeout("changeKnowSlides()",8000);
}

function setupKnowButtons() {
	$("#knowNav .slideDot").click(function() {
		var index = $(this).index() + 1; // Index is 0 based
		clearTimeout(knowTimer);
		if (currentKnowSlide > index) {
			changeKnowSlideLeft(index);
		} else if (currentKnowSlide < index) {
			changeKnowSlideRight(index);
		}
	});
	$("#knowWrapper .slideArrowRight").click(function() {
		if (!knowSlidesMoving) {
			knowRightButton();
			clearTimeout(knowTimer);
		}
	});
	$("#knowWrapper .slideArrowLeft").click(function() {
		if (!knowSlidesMoving) {
			knowLeftButton();
			clearTimeout(knowTimer);
		}
	});
}

function changeKnowSlides() {
	knowRightButton();
	timeKnowSlides();
}


function knowRightButton() {
	if (currentKnowSlide == totalKnowSlides) {
		changeKnowSlideRight(1);
	} else {
		changeKnowSlideRight(currentKnowSlide+1);
	}
}

function knowLeftButton() {
	if (currentKnowSlide == 1) {
		changeKnowSlideLeft(totalKnowSlides);
	} else {
		changeKnowSlideLeft(currentKnowSlide-1);
	}
}

function changeKnowSlideRight(knowSlideNumber) {
	if (!firstKnowLoad) {
		previousKnowSlide = currentKnowSlide;
		$("#knowNav .slideDot").removeClass("activeDot");
	}
	knowSlidesMoving = true;
	currentKnowSlide = knowSlideNumber;
	$("#knowDot" + currentKnowSlide).addClass("activeDot");
	if (firstKnowLoad) {
		knowSlidesMoving = false;
		TweenMax.to("#knowSlide" + currentKnowSlide, knowEaseLength, {left:"0px", autoAlpha:1, ease:knowEaseType});
		firstKnowLoad = false;
	} else {
		$("#knowSlide" + currentKnowSlide).css("visibility", "visible");
		$("#knowSlide" + currentKnowSlide).css("left", moveKnowRight);
		TweenMax.to("#knowSlide" + currentKnowSlide, knowEaseLength, {left:"0px", ease:knowEaseType});
		TweenMax.to("#knowSlide" + previousKnowSlide, knowEaseLength, {left:moveKnowLeft, ease:knowEaseType, onComplete:hidePreviousKnowSlide});
	}
}

function changeKnowSlideLeft(knowSlideNumber) {
	previousKnowSlide = currentKnowSlide;
	$("#knowNav .slideDot").removeClass("activeDot");
	knowSlidesMoving = true;
	currentKnowSlide = knowSlideNumber;
	$("#knowDot" + currentKnowSlide).addClass("activeDot");
	$("#knowSlide" + currentKnowSlide).css("visibility", "visible");
	$("#knowSlide" + currentKnowSlide).css("left", moveKnowLeft);
	TweenMax.to("#knowSlide" + currentKnowSlide, knowEaseLength, {left:"0px", ease:knowEaseType});
	TweenMax.to("#knowSlide" + previousKnowSlide, knowEaseLength, {left:moveKnowRight, ease:knowEaseType, onComplete:hidePreviousKnowSlide});
}

function hidePreviousKnowSlide() {
	knowSlidesMoving = false;
	$("#knowSlide" + previousKnowSlide).css("visibility", "hidden");
}

///////////////////////////////
///////////////////////////////
//TOOL CODE
///////////////////////////////
///////////////////////////////

var toolOpen = false;
//var page1_1Selection = null;
var currentSection = "section1";
var currentSection1Page = "page1-1";
var currentSection2Page = "page2-1";
var currentSection3Page = "page3-1";
window.page1_1Selection = "";
window.page1_2Selection = "";
window.page2_1Selection = "";
window.page2_2Selection = "";

function setupToolButtons() {
	$("#tab1").click(function() {
		// add hash to url
		window.location.hash='safety-step1';
		if (!toolOpen) {
			if ( !embed_tool ) {
				$("#toolWrapper").css("height", "auto");
			}
		}
		$("#section1").css("display", "block");
		$("#tab1").css("display", "block");
		$("#section2").css("display", "none");
		$("#tab2").css("display", "none");
		$("#section3").css("display", "none");
		$("#tab3").css("display", "none");
		toolOpen = true;
		$("#closeButtonMobile").css("display", "block");
	});
	$("#tab2").click(function() {
		// add hash to url
		window.location.hash='safety-step2';
		if (!toolOpen) {
			if ( !embed_tool ) {
				$("#toolWrapper").css("height", "auto");
			}
		}
		$("#section1").css("display", "none");
		$("#tab1").css("display", "none");
		$("#section2").css("display", "block");
		$("#tab2").css("display", "block");
		$("#section3").css("display", "none");
		$("#tab3").css("display", "none");
		toolOpen = true;
		$("#closeButtonMobile").css("display", "block");
	});
	$("#tab3").click(function() {
		// add hash to url
		window.location.hash='safety-step3';
		if (!toolOpen) {
			if ( !embed_tool ) {
				$("#toolWrapper").css("height", "auto");
			}
		}
		$("#section1").css("display", "none");
		$("#tab1").css("display", "none");
		$("#section2").css("display", "none");
		$("#tab2").css("display", "none");
		$("#section3").css("display", "block");
		$("#tab3").css("display", "block");
		toolOpen = true;
		$("#closeButtonMobile").css("display", "block");
	});
	$("#closeButtonMobile").click(function() {
		// remove hash from url
		if ( window.history && window.history.pushState ) { 
			history.pushState('', document.title, window.location.pathname); // for everything else
		} else { 
		    window.location.hash='';
		}
		$("#section1").css("display", "none");
		$("#tab1").css("display", "block");
		$("#section2").css("display", "none");
		$("#tab2").css("display", "block");
		$("#section3").css("display", "none");
		$("#tab3").css("display", "block");
		$(this).css("display", "none");
		//TweenMax.to("#toolWrapper", 1, {height:"234px", ease:"Circ.easeOut"});
		toolOpen = false;
	});
	$(".helpButton").click(function(event) {
		// change hash to third section
		window.location.hash='safety-step3';
		event.preventDefault();
		showSection3Page("page3-1");
	});
	$(".selectionButton").click(function() {
		$(this).parent().parent().find(".selectionButton").each(function( index ) {
			$(this).removeClass("selectionButtonSelected");
		});
		$(this).addClass("selectionButtonSelected");
		saveSelection($(this).parent().parent().attr('id'), $(this).data('selection'));
	});
	$(".seatButton").click(function() {
		// change hash to second section
		window.location.hash='safety-step2';
		$(this).parent().parent().find(".seatButton").each(function( index ) {
			$(this).removeClass("seatButtonSelected");
		});
		$(this).addClass("seatButtonSelected");
		saveSelection($(this).parent().parent().attr('id'), $(this).data('selection'));
	});
	$(".videoButton").click(function() {
		$(this).parent().parent().find(".videoButtonImage").each(function( index ) {
			$(this).removeClass("videoButtonImageSelected");
		});
		$(this).find(".videoButtonImage").addClass("videoButtonImageSelected");
		var videoID = $(this).data('video');
		var html = videoHTML(videoID);
		var videoHeading = $(this).find(".videoDescription").html();
		$('#page2-3 #page2-3Heading').empty();
   		$('#page2-3 #page2-3Heading').html(videoHeading);
		$('#page2-3 #videoHolder').empty();
   		$('#page2-3 #videoHolder').html(html);
		showSection2Page("page2-3");
	});
	$(".backButton").click(function() {
		if ($(this).data('section')) {
			var sectionToDisplay = $(this).data('section');
			$("#" + currentSection).css("display", "none");
			$("#" + sectionToDisplay).css("display", "block");
			if (sectionToDisplay == "section1") {
				$("#tab1").trigger('click');
				// change hash to first section
				window.location.hash='safety-step1';
			}
			if (sectionToDisplay == "section2") {
				$("#tab2").trigger('click');
				// change hash to second section
				window.location.hash='safety-step2';
			}
			if (sectionToDisplay == "section3") {
				$("#tab3").trigger('click');
				// change hash to third section
				window.location.hash='safety-step3';
			}
		} else {
			var pageToDisplay = $(this).data('page');
			if (pageToDisplay == "page1-2") {
				if (window.page1_1Selection == "4+") {
					showSection1Page("page1-2");
				} else {
					showSection1Page("page1-1");
				}
			} 
			if (pageToDisplay == "page2-1") {
				if (window.page1_3Selection == "4+") {
					showSection1Page("page1-3");
				} else {
					showSection2Page("page2-1");
				}
			}
			if (pageToDisplay == "page2-2") {
				if (window.page1_1Selection != "" && window.page1_3Selection != "") {
					if (window.page1_3Selection == "booster" || window.page1_3Selection == "seatbelt") {
						showSection1Page("page1-3");
					} else {
						showSection2Page("page2-1");
					}
				} else {
					if (window.page2_1Selection == "before 2002") {
						$("#page2-2").find('[data-vehicle="after 2002"]').css("display", "none");
						showSection2Page("page2-2");
					} 
					if (window.page2_1Selection == "after 2002") {
						$("#page2-2").find('[data-vehicle="before 2002"]').css("display", "none");
						showSection2Page("page2-2");
					}
				}
			} 
			if (pageToDisplay == "page1-1" || pageToDisplay == "page1-3") {
				showSection1Page(pageToDisplay);
			}
		}
	});
}

function saveSelection(thePage, theSelection) {
	//alert("saving " + theSelection + " to " + thePage);
	/*
	if (thePage == "page1-1" && window.page1_1Selection != theSelection) {
		window.page1_2Selection = "";
	}
	if (thePage == "page2-1" && window.page2_1Selection != theSelection) {
		window.page2_2Selection = "";
	}c */
	var storePageSelection = thePage.replace("-", "_");
	window[storePageSelection + "Selection"] = theSelection;
	if (thePage == "page1-1" && theSelection == "0-2") {
		showSection1Page("page1-3");
	}
	if (thePage == "page1-1" && theSelection == "2-4") {
		showSection1Page("page1-3");
	}
	if (thePage == "page1-1" && theSelection == "4+") {
		showSection1Page("page1-2");
	}
	if (thePage == "page1-2") {
		showSection1Page("page1-3");
	}
	if (thePage == "page1-3" && theSelection == "infant carseat") {
		showSection2Page("page2-1");
	}
	if (thePage == "page1-3" && theSelection == "rear carseat") {
		showSection2Page("page2-1");
	}
	if (thePage == "page1-3" && theSelection == "forward carseat") {
		showSection2Page("page2-1");
	}
	if (thePage == "page1-3" && theSelection == "booster") {
		var videoID = $('#page2-2').find('[data-age="4+"][data-height="under height"]').data('video');
		var html = videoHTML(videoID);
		var videoHeading = $('#page2-2').find('[data-age="4+"][data-height="under height"]').find(".videoDescription").html();
		//var videoHeading = $('#page2-2 .booster').find(".videoDescription").html();
		$('#page2-3 #page2-3Heading').empty();
   		$('#page2-3 #page2-3Heading').html(videoHeading);
		$('#page2-3 #videoHolder').empty();
   		$('#page2-3 #videoHolder').html(html);
		showSection2Page("page2-3");
	}
	if (thePage == "page1-3" && theSelection == "seatbelt") {
		var videoID = $('#page2-2').find('[data-age="4+"][data-height="over height"]').data('video');
		var html = videoHTML(videoID);
		var videoHeading = $('#page2-2').find('[data-age="4+"][data-height="over height"]').find(".videoDescription").html();
		$('#page2-3 #page2-3Heading').empty();
   		$('#page2-3 #page2-3Heading').html(videoHeading);
		$('#page2-3 #videoHolder').empty();
   		$('#page2-3 #videoHolder').html(html);
		showSection2Page("page2-3");
	}
	if (thePage == "page2-1" && window.page1_1Selection == "") {
		showSection2Page("page2-2");
	}
	if (thePage == "page2-1" && window.page1_1Selection == "0-2" && window.page1_3Selection == "infant carseat") {
		if (theSelection == "before 2002") {
			var videoID = $('#page2-2').find('[data-age="0-2"][data-carseat="infant carseat"][data-vehicle="before 2002"]').data('video');
			var videoHeading = $('#page2-2').find('[data-age="0-2"][data-carseat="infant carseat"][data-vehicle="before 2002"]').find(".videoDescription").html();
		} else {
			var videoID = $('#page2-2').find('[data-age="0-2"][data-carseat="infant carseat"][data-vehicle="after 2002"]').data('video');
			var videoHeading = $('#page2-2').find('[data-age="0-2"][data-carseat="infant carseat"][data-vehicle="after 2002"]').find(".videoDescription").html();
		}
		var html = videoHTML(videoID);
		$('#page2-3 #page2-3Heading').empty();
   		$('#page2-3 #page2-3Heading').html(videoHeading);
		$('#page2-3 #videoHolder').empty();
   		$('#page2-3 #videoHolder').html(html);
		showSection2Page("page2-3");
	}
	if (thePage == "page2-1" && window.page1_1Selection == "0-2" && window.page1_3Selection == "rear carseat") {
		if (theSelection == "before 2002") {
			var videoID = $('#page2-2').find('[data-age="0-2"][data-carseat="rear carseat"][data-vehicle="before 2002"]').data('video');
			var videoHeading = $('#page2-2').find('[data-age="0-2"][data-carseat="rear carseat"][data-vehicle="before 2002"]').find(".videoDescription").html();
		} else {
			var videoID = $('#page2-2').find('[data-age="0-2"][data-carseat="rear carseat"][data-vehicle="after 2002"]').data('video');
			var videoHeading = $('#page2-2').find('[data-age="0-2"][data-carseat="rear carseat"][data-vehicle="after 2002"]').find(".videoDescription").html();
		}
		var html = videoHTML(videoID);
		$('#page2-3 #page2-3Heading').empty();
   		$('#page2-3 #page2-3Heading').html(videoHeading);
		$('#page2-3 #videoHolder').empty();
   		$('#page2-3 #videoHolder').html(html);
		showSection2Page("page2-3");
	}
	if (thePage == "page2-1" && window.page1_1Selection == "2-4" && window.page1_3Selection == "forward carseat") {
		if (theSelection == "before 2002") {
			var videoID = $('#page2-2').find('[data-age="2-4"][data-vehicle="before 2002"]').data('video');
			var videoHeading = $('#page2-2').find('[data-age="2-4"][data-vehicle="before 2002"]').find(".videoDescription").html();
		} else {
			var videoID = $('#page2-2').find('[data-age="2-4"][data-vehicle="after 2002"]').data('video');
			var videoHeading = $('#page2-2').find('[data-age="2-4"][data-vehicle="after 2002"]').find(".videoDescription").html();
		}
		var html = videoHTML(videoID);
		$('#page2-3 #page2-3Heading').empty();
   		$('#page2-3 #page2-3Heading').html(videoHeading);
		$('#page2-3 #videoHolder').empty();
   		$('#page2-3 #videoHolder').html(html);
		showSection2Page("page2-3");
	}
}

function showSection1Page(displayPage) {
	$("#section1").css("display", "block");
	$("#tab1").css("display", "block");
	$("#section2").css("display", "none");
	$("#tab2").css("display", "none");
	$("#section3").css("display", "none");
	$("#tab3").css("display", "none");
	currentSection = "section1";
	$("#" + currentSection1Page).css("display", "none");
	if (displayPage == "page1-3") {
		if (window.page1_1Selection == "0-2") {
			$("#page1-3 .option1").css("display", "block");
			$("#page1-3 .option2").css("display", "none");
			$("#page1-3 .option3").css("display", "none");
			$("#page1-3 .option4").css("display", "none");
		} 
		if (window.page1_1Selection == "2-4") {
			$("#page1-3 .option1").css("display", "none");
			$("#page1-3 .option2").css("display", "block");
			$("#page1-3 .option3").css("display", "none");
			$("#page1-3 .option4").css("display", "none");
		}
		if (window.page1_1Selection == "4+") {
			if (window.page1_2Selection == "under height") {
				$("#page1-3 .option1").css("display", "none");
				$("#page1-3 .option2").css("display", "none");
				$("#page1-3 .option3").css("display", "block");
				$("#page1-3 .option4").css("display", "none");
			} else {
				$("#page1-3 .option1").css("display", "none");
				$("#page1-3 .option2").css("display", "none");
				$("#page1-3 .option3").css("display", "none");
				$("#page1-3 .option4").css("display", "block");
			}
		}
	}
	$("#" + displayPage).css("display", "block");
	currentSection1Page = displayPage;
}

function showSection2Page(displayPage) {
	$("#section1").css("display", "none");
	$("#tab1").css("display", "none");
	$("#section2").css("display", "block");
	$("#tab2").css("display", "block");
	$("#section3").css("display", "none");
	$("#tab3").css("display", "none");
	currentSection = "section2";
	if (displayPage == "page2-2") {
		$(".videoButton").css("display", "block");
		if (window.page2_1Selection == "before 2002") {
			$("#page2-2").find('[data-vehicle="after 2002"]').css("display", "none");
		} 
		if (window.page2_1Selection == "after 2002") {
			$("#page2-2").find('[data-vehicle="before 2002"]').css("display", "none");
		}
	}
	$("#" + currentSection2Page).css("display", "none");
	$("#" + displayPage).css("display", "block");
	currentSection2Page = displayPage;
}

function showSection3Page(displayPage) {
	$("#section1").css("display", "none");
	$("#tab1").css("display", "none");
	$("#section2").css("display", "none");
	$("#tab2").css("display", "none");
	$("#section3").css("display", "block");
	$("#tab3").css("display", "block");
	currentSection = "section3";
	$("#" + currentSection3Page).css("display", "none");
	$("#" + displayPage).css("display", "block");
	currentSection3Page = displayPage;
}

function videoHTML ( videoID ) {
	var html = '';
	
	if ( videoID.substr(0,1) == '/' ) {
		// Image
		var urls = videoID.split(',');
		if ( urls.length == 2 ) 
			html += '<a href="' + urls[1] + '" target="_blank">';
		html += '<img src="' + urls[0] + '">';
		if ( urls.length == 2 ) 
			html += '</a>';
		
	} else {
		// YouTube Video
		html = '<iframe width="460" height="259" src="http://www.youtube.com/embed/' + videoID + '" frameborder="0" allowfullscreen ></iframe>';
	}
	
	return html;
}

///////////////////////////////
///////////////////////////////
//TABBER CODE
///////////////////////////////
///////////////////////////////

function setupTabberTabs() {
	$(".tabberTab").click(function() {
		$(this).parent().find(".tabberTab").each(function( index ) {
			$(this).removeClass("tabberSelectedTab");
		});
		$(this).parent().parent().parent().find(".tabberContent").each(function( index ) {
			$(this).css("display", "none");
		});
		$(this).addClass("tabberSelectedTab");
		var contentToShow = $(this).data('content');
		$(this).parent().parent().parent().find("#"+contentToShow).css("display", "block");
	});
}

///////////////////////////////
///////////////////////////////
//PLEDGE CODE
///////////////////////////////
///////////////////////////////
function setupPledgeButton() {
	// Javascript Fallback for < IE9
	// No :checked psuedo class in < IE9
	$('#pledgeForm .pledge .wpcf7-list-item input').change(function ( ) {
		var $this = $(this);
		
		if ( $this.is(':checked') ) {
			$this.addClass('checked');
		} else {
			$this.removeClass('checked');
		}
	});
	
	$("#facebookShare").click(function() {
		window.open(
      'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
      'facebook-share-dialog', 
      'width=626,height=436'); 
	});
	$("#twitterShare").click(function() {
		window.open(
      'https://twitter.com/intent/tweet?url='+encodeURIComponent(location.href)+'&text='+encodeURIComponent($(this).data('status')), 
      'twitter-share-dialog', 
      'width=550,height=420'); 
	});
}


/////////////////////////////////////////

$(window).resize(function() {
	moveHeroLeft = moveKnowLeft = ($("#heroWrapper").width() * -1) + "px";
	moveHeroRight = moveKnowRight= $("#heroWrapper").width() + "px";
});





/*******************************************************
	for homepage tabs to have their own social sharing 
*******************************************************/

/* on page load if #car-seat-safety-1 then init */

$(function() {

	$(window).load(function() {
		$('#tab3 .sharing').hide();
		if(document.location.toString().indexOf('#safety-step1')!=-1){
	    	$('#tab1').trigger('click');
		}
		if(document.location.toString().indexOf('#safety-step2')!=-1){
			$('#tab2').trigger('click');
		}
		if(document.location.toString().indexOf('#safety-step3')!=-1){
			$('#tab3').trigger('click');
		}
	});

});

	
