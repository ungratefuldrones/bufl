app = app || {};

app.models.hospital = Backbone.Model.extend ({
	defaults : {
		'post_title' : '',
		'post_name' : '',
		'administrator' : {},
		'contact_information' : ''
	},
});

app.collections.hospitals = Backbone.Collection.extend ({
	model : app.models.hospital,
});