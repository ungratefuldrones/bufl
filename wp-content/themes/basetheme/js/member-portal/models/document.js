app = app || {};

app.models.message = Backbone.Model.extend ({

});

app.models.doc = Backbone.Model.extend ({
	defaults : {
		'id' : '',
		'post_title' : '',
		'post_author' : '',
		'document' : '',
		'post_modified' : '',
		'post_status' : '',
		'document_type' : '',
		'document_mime_type' : '',
		'document_language' : '',
		'comments' : '',
		'actions' : {},
		'can_destroy' : false,
		'category_id' : -1,
		'category_slug' : '',
		'audience' : '', // true for all, false for ONLY admins
		'show_status' : false,
		'nice_status' : '',
		'hospital_partner' : ''
	},
	initialize : function() {
		this.refresh_permissions();
		this.set_nice_status();
	},
	refresh_permissions : function() {
		this.set(
			{ 'actions' : app.mediator.document_actions(this) },
			{ silent : true}
		);
	},
	set_nice_status : function() {
		var status;
		if (this.get('post_status') == 'trash') status = 'Rejected';
		if (this.get('post_status') == 'publish') status = 'Approved';
		if (this.get('post_status') == 'pending') status = 'Pending';
		this.set(
			{ nice_status : status}, 
			{ silent: true }
		);
	}
});

app.collections.documents = Backbone.Collection.extend ({
	model : app.models.doc,

	initialize : function() {
		this.comparator = function(model) {
			return model.get('post_modified');
		}
	}
	
});