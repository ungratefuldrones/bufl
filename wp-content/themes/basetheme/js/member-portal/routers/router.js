app = app || {};

// this is the main controller for the member portal, contains most if not all routing functions.
app.router = Backbone.Router.extend ({

	routes: {
		'' : 'home',
		'reporting' : 'reporting',
		'media-toolbox' : 'media_toolbox',
		'program-implementation' : 'program_implementation',
		'additional-resources' : 'additional_resources',
		'archive' : 'archive',
		'search/:query' : 'search',
		'hospital/:partner' : 'hospital'
	},

	// not implemented yet
	search : function(query) {
		app.debug('route: home');
		// var view = new app.views.search();
		// this.start_display(view);
	},

	hospital : function(partner) {
		app.debug('route: hospital');
		var model = app.hospitals.findWhere({ post_name : partner });
		var view = new app.views.hospital({ model : model });
		this.start_display(view);
	},

	home : function() {
		app.debug('route: home');
		var view = new app.views.home();
		this.start_display(view);
	},

	archive : function() {
		app.debug('route: archive');
		view = this.get_document_view({
			class_name : 'archive',
			show_status : true
		});
		this.start_display(view);
	},

	reporting : function() {
		app.debug('route: reporting');
		view = this.get_document_view({
			parent_category : 'reporting',
			filter_object : {
				category_slug : 'reports', // the tab to show on page load.
				audience : true
			},
			class_name : 'media_toolbox'
		});
		this.start_display(view);
	},

	media_toolbox : function() {
		app.debug('route: media-toolbox');
		view = this.get_document_view({
			parent_category : 'media-toolbox',
			filter_object : {
				category_slug : 'logo-kits',
				audience : true
			},
			class_name : 'media_toolbox'
		});
		this.start_display(view);
	},

	program_implementation : function() {
		app.debug('route: program-implementation');
		view = this.get_document_view({
			parent_category : 'program-implementation',
			filter_object : {
				category_slug : 'getting-started',
				audience : true
			},
			class_name : 'program-implementation'
		});
		this.start_display(view);
	},

	additional_resources : function() {
		app.debug('route: additional-resources');
		view = this.get_document_view({
			parent_category : 'additional-resources',
			filter_object : {
				category_slug : 'tutorials-examples',
				audience : true
			},
			class_name : 'additional-resources'
		});
		this.start_display(view);
	},

	get_document_view : function(options) {		
		if (options.parent_category) {
			
			var cat_obj = {};
			for (var i = member_data.document_types.length - 1; i >= 0; i--) {
				if (member_data.document_types[i].slug == options.parent_category) cat_obj = member_data.document_types[i];
			};
			options.document_hierarchy = cat_obj;
		}

		model = new app.models.documentList( options );

		return new app.views.documentList({
			model : model
		});
	},

	start_display : function(view) {
		
		if (this.current_list_view) {
			this.current_list_view.remove();
		}

		this.current_list_view = view;
		app.interface.render(view.$el);
		view.render();

	}

});