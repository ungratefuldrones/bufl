<script type="text/template" id="home">
 <div class="module">
    <div class="module-description">
    <h1><span class="icon-bg icon-home"></span>Member Portal Home</h1>
    </div>
    <div class="module-header">
      <div class="subtitle-wrap clearfix">
        <% if (is_admin) { %> 
          <h3 class="subtitle">Recent Uploads</h3>
        <% } else { %>
          <h3 class="subtitle">Uploaded by Me</h3>
        <% } %>
      </div>
    </div>
    <div id="documents"></div>
  </div>

  <% if (is_admin) { %> 
    <div id="hospitals"></div>
  <% } %>

</script>

<!-- HOSPITAL PARTNER TEMPLATES -->

<script type="text/template" id="hospital-partners-template">
  <div class="module-header">
    <div class="subtitle-wrap clearfix">
      <h3 class="subtitle">Hospital Partners</h3>
      <form class="search-portal">
        <input class="text-input" type="search" name="search-partners" placeholder="Search">
      </form>
    </div>
  </div>
  
  <div class="list-table cols-7" id="hospital-partner-list">
    <div class="row">
      <div class="col-item colspan-3">Hospital</div>
      <div class="col-item colspan-1">Administrator</div>
      <div class="col-item colspan-3">Contact Information</div>
    </div>
  </div>
</script>

<script type="text/template" id="hospital-partner-list-item">
  <div class="col-item colspan-3"><%= post_title %></div>
  <div class="col-item colspan-1"><%= administrator.display_name %></div>
  <div class="col-item colspan-3"><%= contact_information %></div>
</script>

<script type="text/template" id="hospital">
  <header>
    <h1><%= post_title %></h1>
    <h2><%= administrator.display_name %></h2>
    <% if (contact_information !== false) { %>
      <p><%= contact_information %></p>
    <% } else { %>
      <p>Hospital user does not have contact information</p>
    <% } %>
  </header>
  <div id="documents"></div>
</script>

<!-- DOCUMENT TEMPLATES -->

<script type="text/template" id="document-list-item">
  <div class="col-item colspan-2">
    <div class="icon <%= category_slug %>"></div>
    <%= post_title %>
    
  </div>
  <div class="col-item colspan-2">Updated: <%= post_modified %></div>
  <div class="col-item colspan-1"><%= document_mime_type %></div>
  <div class="col-item colspan-1 caps language"><%= document_language %></div>
  <% if (show_status) { %>
    <div class="col-item colspan-1 doc-status <%= post_status %>">
      <span><%= nice_status %></span>
    </div>
  <% } %>
  <div class="col-item colspan-1 caps actions-wrap">
    <span class='actions'>Actions</span>
    <ul class="dropdown-options">
      <% $.each(actions, function(index, value) { %>
        <li class="document-action <%= index %>"><%= value %></li>
      <% }); %>
    </ul>
  </div>
</script>

<script type="text/template" id="document-list-folder">
  <div class="document-folder row" data-folder=<%= slug %>> 
    <div class="col-item colspan-7 folder-head">
      <div class="icon expand"></div>
      <span class="name"><%= name %></span>
      <span class="download">Download ZIP File</span>
    </div>
  </div>
</script>

<script type="text/template" id="document-overlay">
  <h2>TODO</h2>
</script>

<script type="text/template" id="document-edit">
  <h2>TODO</h2>
</script>

<script type="text/template" id="document-list">
  <div class="module">
  <% console.log('document hierarchy in the document list template');
  console.log(document_hierarchy); %>
    <% if (typeof document_hierarchy.description !== 'undefined') { %>
    <div class="module-description">
      <h1><span class="icon-bg icon-archived"></span><%= name %></h1>
      
      <% if (document_hierarchy.description.trim() === '') { %>
        <p>This message shows when the category description is blank</p>
      <% } else { %>
        <p><%= document_hierarchy.description %></p>
      <% } %>

    </div>
    <% } %>

    <% if (typeof document_hierarchy.tabs !== 'undefined' && document_hierarchy.tabs.length) { %>
    <div id="tabs" class="tabs">
      <ul class="tabs-nav">
        <% for (var i = 0; i < document_hierarchy.tabs.length; i++) { %>
          <li class="nav-action <%= document_hierarchy.tabs[i].slug %>" data-filter="<%= document_hierarchy.tabs[i].slug %>">
            <div class="nav"><span><%= document_hierarchy.tabs[i].name %></span></a>
          </li>
        <% }; %>
      </ul>
    <% } %>

      <div class="tab">
        <div class="search-for-tab">
          <div class="module-header">
            <div class="subtitle-wrap clearfix">
              <form class="search-portal">
                <input class="text-input" id="search-docs" type="search" name="search-partners"
                <% if (searchString) { %>
                  value="<%= searchString %>"
                <% } else { %>
                  placeholder="Search"
                <% } %>
                />
              </form>
              <% if (typeof pagination !== 'undefined' && pagination) { %>
                <div class="pagination">
                  <span class="pagi-count">1 - X of XX</span><div class="pagi pagi-left"><a href="#"><span></span></a></div><div class="pagi pagi-right"><a href="#"><span></span></a></div>
                </div>
              <% } %>
            </div>
          </div>
        </div>
        <div class="list-table cols-8">
          <div class="row heading">
            <div class="col-item colspan-2">Document</div>
            <div class="col-item colspan-2">Created/Modified</div>
            <div class="col-item colspan-1">Type</div>
            <div class="col-item colspan-1">Language</div>
            <% if (show_status) { %>
              <div class="col-item colspan-1">Status</div>
            <% } %>
            <div class="col-item colspan-1"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</script>

<!-- OVERLAY TEMPLATES -->

<script type="text/template" id="overlay-upload">

  <h2>
    <div class="overlay-title">Upload New File</div>
    <div class="overlay-close" title="close"></div>
  </h2>

  <div class="form-wrapper">
    <div class="frm_forms" id="form">

    <form enctype="multipart/form-data" method="post" class="frm-show-form member-form">

      <input type="hidden" name="mp_form_type" value="upload">

      <div class="frm_description">
        <p>DESCRIPTION TO DO</p>
      </div>
      
      <div class="frm_form_fields frm_page_num_1">
        <fieldset>
          <div>
            <div class="frm_form_field form-field  frm_required_field frm_top_container no-br no-border no-padding">
              <label for="doc_file" class="frm_primary_label hidden-label">
                  <span class="frm_required"></span>
              </label>
              <input type="file" class="required" name="doc_file" id="doc_file"><br>
            </div>
            <div class="frm_form_field form-field  frm_top_container">
              <label for="doc_title" class="frm_primary_label">Document Title
                <span class="frm_required">*</span>
              </label>
              <input type="text" class="required" id="doc_title" name="doc_title" value="">
            </div>
            
            <div class="frm_form_field form-field  frm_top_container">
              <label for="doc_type" class="frm_primary_label">Document Type
                <span class="frm_required">*</span>
              </label>
              <select id="doc_type" name="doc_id">
                <option value="-1" disabled>Select a Category</option>
                <% for (var i = docs.length - 1; i >= 0; i--) { %>
                  <option value="<%= docs[i].cat_ID %>" disabled><%= docs[i].cat_name %></option>
                  <% for (var j = 0; j < docs[i].tabs.length; j++) { %>
                    <option value="<%= docs[i].tabs[j].cat_ID %>">- <%= docs[i].tabs[j].cat_name %></option>
                    <% for (var k = 0; k < docs[i].tabs[j].folders.length; k++) { %>
                      <option value="<%= docs[i].tabs[j].folders[k].cat_ID %>">-- <%= docs[i].tabs[j].folders[k].cat_name %></option>
                    <% }; %>
                  <% }; %>
                <% }; %>
              </select>
            </div>

            <% if (app.mediator.is_admin()) { %>
              <div class="frm_form_field form-field frm_top_container side-label">
                <input type="checkbox" id="for_partners" name="for_partners" />
                <label for="for_partners" class="frm_primary_label">Make Document Available to all Hospital Partners</label>                
              </div>
            <% } %>

            <div class="frm_form_field form-field  frm_top_container">
              <label for="doc_language" class="frm_primary_label">Document Language
                <span class="frm_required">*</span>
              </label>
              <select class="required" id="doc_language" name="doc_language">
                <option value="english-spanish">English & Spanish</option>
                <option value="english">English Only</option>
                <option value="spanish">Spanish Only</option>
              </select>
            </div>
            <div class="frm_form_field form-field  frm_top_container">
              <label for="doc_comments" class="frm_primary_label">Additional Comments
                  <span class="frm_required"></span>
              </label>
              <textarea name="doc_comments" id="doc_comments" rows="5"></textarea>
            </div>
          </div>
        </fieldset>
      </div>

      <div class="frm_submit">
        <input type="submit" value="Submit" class="button-primary">
      </div>

    </form>

  </div>
</script>

<script type="text/template" id="overlay-document">

  <h2>
    <div class="overlay-title"><%= post_title %></div>
    <div class="overlay-close" title="close"></div>

  </h2>

  <div class="form-wrapper">
    <div class="frm_forms" id="form">

    <form enctype="multipart/form-data" method="post" class="frm-show-form member-form disabled">

      <input type="hidden" name="mp_form_type" value="upload">
      
      <div class="frm_form_fields frm_page_num_1">
        <fieldset>
          <div>
            <div class="frm_form_field form-field  frm_required_field frm_top_container no-br no-border no-padding">
              <label><%= document.title %> <%= document_mime_type %></label>
              <label for="doc_file" class="frm_primary_label hidden-label">
                  <span class="frm_required"></span>
              </label>
              <input type="file" disabled='true' name="doc_file" id="doc_file"><br>
            </div>

            <div class="frm_form_field form-field  frm_top_container">
              <label for="doc_title" class="frm_primary_label">Document Title
                <span class="frm_required">*</span>
              </label>
              <input type="text" disabled='true' value="<%= post_title %>" class="required" id="doc_title" name="doc_title" value="">
            </div>
            
            <% if (audience && app.mediator.is_admin()) { %>
              <div class="frm_form_field form-field  frm_top_container">
                <label for="doc_type" class="frm_primary_label">Document Type
                  <span class="frm_required">*</span>
                </label>
                <select id="doc_type" disabled="true" name="doc_id">
                  <option value="-1" disabled>Select a Category</option>
                  <% console.log('document categories'); console.log(docs); %>
                  <% for (var i = docs.length - 1; i >= 0; i--) { %>
                    <option value="<%= docs[i].cat_ID %>" disabled><%= docs[i].cat_name %></option>
                    <% for (var j = 0; j < docs[i].tabs.length; j++) { %>
                      <option value="<%= docs[i].tabs[j].cat_ID %>">- <%= docs[i].tabs[j].cat_name %></option>
                      <% for (var k = 0; k < docs[i].tabs[j].folders.length; k++) { %>
                        <option value="<%= docs[i].tabs[j].folders[k].cat_ID %>">-- <%= docs[i].tabs[j].folders[k].cat_name %></option>
                      <% }; %>
                    <% }; %>
                  <% }; %>
                </select>
              </div>
            <% } %>

            <% if (app.mediator.is_admin()) { %>
              <div class="frm_form_field form-field frm_top_container side-label">
                <input type="checkbox" disabled="true" <% if (audience) { %>checked<% } %> id="doc_consumption" name="doc_consumption" />
                <label for="doc_consumption" class="frm_primary_label">Make Document Available to all Hospital Partners</label>                
              </div>
            <% } %>

            <div class="frm_form_field form-field  frm_top_container">
              <label for="doc_language" class="frm_primary_label">Document Language
                <span class="frm_required">*</span>
              </label>
              <select class="required" disabled="true" id="doc_language" name="doc_language">
                <option <% if (document_language == 'english-spanish') { %>selected<% } %> value="english-spanish">English & Spanish</option>
                <option <% if (document_language == 'english') { %>selected<% } %> value="english">English Only</option>
                <option <% if (document_language == 'spanish') { %>selected<% } %> value="spanish">Spanish Only</option>
              </select>
            </div>
            <div class="frm_form_field form-field  frm_top_container">
              <label for="doc_comments" class="frm_primary_label">Additional Comments
                  <span class="frm_required"></span>
              </label>
              <textarea name="doc_comments" id="doc_comments" disabled="true" rows="5"><% if (comments) { %><%= comments %><% } %> </textarea>
            </div>
          </div>

          <input type="hidden" id="document_id" name="document_id" value="<%= ID %>" ></input>

        </fieldset>
      </div>

      <% if (app.mediator.document_user_can('edit', id)) { %>
        <div class="frm_submit submit">
          <input type="submit" value="Submit" class="button-primary">
        </div>

        <div class="frm_submit edit">
          <div type="submit" class="button-primary">Enable Editing</div>
        </div>
      <% } %>

    </form>

  </div>
</script>

<script type="text/template" id="overlay-compose">
  <h2>
    <div class="overlay-title">Compose</div>
    <div class="overlay-close" title="close"></div>
  </h2>
  <div class="form-wrapper">
    <?php echo FrmEntriesController::show_form(6, $key = 'h3seoa', $title=false, $description=false); // 11 on Tz's Dev  6 on staging ?>
  </div>
</script>

<!-- MISC TEMPLATES -->

<script type="text/template" id="notification">
  <div class="alert">
    <h3><span class="icon-bg icon-megaphone"></span>Important Message:</h3>
    <p><%= message %></p><a href="#overlay-delete" class="trigger-overlay alert-close"></a>
  </div>
</script>