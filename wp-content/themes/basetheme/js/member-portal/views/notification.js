app = app || {};

app.models.notification = Backbone.Model.extend ( {

	message : ''

} );

app.views.notification = Backbone.View.extend ( {

	className : 'notification',

	events : {
		'click .alert-close' : 'close_alert'
	},

	initialize : function (options) {

		// model / collection should be passed in as options

		this.template = _.template($('#notification').html());

	},

	render : function () {

		this.$el.html(this.template(this.model.toJSON()));

		//todo fadeout and then destroy.

	},

});