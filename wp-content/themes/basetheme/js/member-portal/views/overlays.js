app = app || {};

app.views.overlay = Backbone.View.extend ( {

	className : 'overlays-wrapper',

	events : {
		'click .overlay-close' : 'destroy',
		'submit form' : 'validate_form'
	},

	initialize : function (options) {
		if (typeof options.inner_view === 'undefined') {
			console.error('overlay view initialize expects an initialized backbone view called inner_view');
			return false;
		} else if (!options.inner_view instanceof Backbone.View) {
			console.error('inner_view arg is not a backbone view');
			return false;
		}
		this.inner_view = options.inner_view;
	},

	render : function () {
		
		// append the inner view
		this.$el.append(this.inner_view.$el);
		this.inner_view.render();

		this.$el.fadeIn();
		this.setHeight();

	},

	setHeight : function () {
		
		var sum = 0,
			$inner_overlay = this.$el.find('.overlay')
		;

		$inner_overlay.children().each(function () {
			sum += sum + $(this).innerHeight(); 
		})
		$inner_overlay.css('height', sum);
	
	},

	validate_form : function (e) {

		var $form = this.$el.find('form'),
			$required = $form.find('input.required'),
			validated = true
		;

		$required.each(function(index, input) {
			if ($(input).val() == '') {
				validated = false
			}
		})

		console.log('validate form:' + validated);

		if (validated) {
			return true;
		} else {
			return false;
		}

	},

	destroy : function () {
		
		var view = this;
		this.$el.fadeOut(250, function() {
			view.remove();
		});

	}

});

// overlays (views that are rendered into the overlay)

app.views.overlays = {};

app.views.overlays.upload = Backbone.View.extend({

	className : 'overlay overlay-upload',

	initialize : function() {
		this.template = _.template($('#overlay-upload').html());
	},

	render : function() {
		this.$el.append(this.template({docs: member_data.document_types}));
	}

});

app.views.overlays.compose = Backbone.View.extend({

	className : 'overlay overlay-compose',

	initialize : function() {
		this.template = _.template($('#overlay-compose').html());
	},

	render : function() {
		this.$el.append(this.template({}));
	}

});

app.views.overlays.document = Backbone.View.extend({

	className : 'overlay overlay-document',

	events : {
		'click .edit .button-primary' : 'toggle_editing'
	},

	initialize : function() {
		this.template = _.template($('#overlay-document').html());
	},

	toggle_editing : function() {
		this.$el.find('input, select, textarea').each(function() {
			$(this).removeAttr('disabled');
		});
		this.$el.find('form').removeClass('disabled');
	},

	render : function() {
		var obj_for_template = this.model.toJSON();
		obj_for_template.docs = member_data.document_types;
		this.$el.append(this.template(obj_for_template));
	}

});