app = app || {};

app.views.hospital = Backbone.View.extend ( {

	className: 'hospital',

	initialize : function (options) {
		this.template = _.template($('#hospital').html());
	},

	render : function () {
		this.$el.html(this.template(this.model.toJSON()));

		// get documents, append, and render
		this.hospital_documents_view = this.getHospitalDocuments();
		this.$el.find('#documents').append(this.hospital_documents_view.$el);
		this.hospital_documents_view.render();

		// if (is_admin) {
		// 	// get hospital partners, append, and render
		// 	this.hospitals_view = this.getHospitals();
		// 	this.$el.find('#hospitals').append(this.hospitals_view.$el);
		// 	this.hospitals_view.render();
		// }

	},

	getHospitalDocuments : function () {
		var documentList_model = {
			filter_object : {
				hospital_name : this.model.get('post_name')
			},
			show_status : true
		};

		// if (!app.mediator.is_admin()) documentList_model.filter_doc_author = app.mediator.get_user_id();

		return new app.views.documentList({
			model : new app.models.documentList(documentList_model)
		});
	},

	remove : function () {
		// this.recent_documents_view.remove();
		Backbone.View.prototype.remove.call(this);
	}

});