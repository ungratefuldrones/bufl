app = app || {};

app.models.documentList = Backbone.Model.extend ( {
	
	defaults: {
		show_status : false, // category or date
		filter_object : {},
		parent_category : false,
		document_hierarchy : false, // // Document hierarchy is the parent wordpress category for the current section.
		className : '',
		searchString : false
	}

} );

app.views.documentList = Backbone.View.extend ( {

	className : 'document-list',

	events : {
		'click .actions' : 'actions',
		'click .nav-action' : 'toggle_tab_event',
		'click .document-folder' : 'toggle_folder',
		'submit .search-portal' : 'search',
		'keyup #search-docs' : 'search'
	},

	initialize : function (options) {

		// model and template should be passed in as options

		this.collection = app.member_documents; // this is a global collection created in app.js
		
		this.template = _.template($('#document-list').html());

		// array of all document views
		this.views = [];

		console.log('the model');
		console.log(this.model);

		var view = this;
		this.listenTo(this.model, 'change', function() {
			view.renderList();
		});
		this.listenTo(this.collection, "remove add", function() {
			// console.log('the collection has things added: ');
			// console.log(this);
			//view.render();
		});

		this.interval_id = false;
		this.check_for_update = _.bind(this.check_for_update, this);

	},

	render : function () {

		// Document hierarchy is the parent category (wordpress category) for the current section.
		this.$el.html(this.template(this.model.toJSON()));
		this.renderList();
		this.$el.addClass(this.model.get('class_name'));

		var view = this;
		if (!this.interval_id) {
			this.interval_id = window.setInterval(function () {
				view.check_for_update();
			}, app.ajax_update_interval_doc_list);
		}

	},

	check_for_update : function() {

		$.ajax({
			type : "post",
			url : '/wp-admin/admin-ajax.php',
			data : {
				action : 'mp_get_document_list',
			},
			dataType : 'json',
			context : this,
			success: function(response) {
				if (response.success) {
					var models = [];
					for (var i = 0; i < response.documents.length; i++) {
						var model = new app.models.doc(response.documents[i]);
						if (this.model.get('show_status')) model.set({'show_status' : true}, {silent : true});
						models.push(model);
					};
					this.collection.set(models);

				} else {
					app.debug('ERROR - get document list');
					app.debug(response);
				}
			},
			error : function(response, text, error) {
				console.log('ajax ERROR');
				console.log(response);
				console.log(text);
				console.log(error);
			}
		});

	},

	renderList : function () {

		var tab_class = this.model.get('filter_object').category_slug ? '.' + this.model.get('filter_object').category_slug : '';
			$list_el = this.$el.find('.list-table'),
			views = this.views,
			searchString = this.model.get('searchString'),
			filter_args = this.model.get('filter_object'),
			result = { 
				rows_to_append : [],
				views : []
			};

		// set the tab to active
		$('.nav-action').removeClass('ui-tabs-active');
		$('.nav-action' + tab_class).addClass('ui-tabs-active');

		// remove all the current views
		for (var i = this.views.length - 1; i >= 0; i--) {
			this.views[i].destroy();
		};

		views = this.views = [];

		// remove all current folders
		$list_el.find('.document-folder').remove();

		result = this._filterList(filter_args);

		// I think this is stupid. 2n and should be 1n probably.
		// rows to append != views to append, rows may contain folders which dont have a view associated.
		for (var i = 0; i < result.rows_to_append.length; i++) {
			$list_el.append(result.rows_to_append[i]);
		};
		this.views = result.views;
		for (var i = 0; i < result.views.length; i++) {
			if (this.model.get('show_status')) result.views[i].model.set({'show_status' : true},{silent : true});
			result.views[i].render();
		};

	},

	// this is a big ugly method that filters the collection based on the filter object. I hate it.
	_filterList : function(filter_args) {
		
		// filter the collection and create a view for each document
		var rows_to_append = [],
			views = [],
			searchString = this.model.get('searchString');

		var _filterCollection = function() {

			// this seems like a dumb way to do this.
			// _.where with an empty object for attributes returns nothing instead of everythin
			if (_.isEmpty(filter_args)) {
				this.collection.each(function(doc) {
					if (searchString) {
						if ( this._searchDoc(doc) ) {
							var doc_view = new app.views.document({
								model: doc
							});
							rows_to_append.push(doc_view.$el);
							//doc_view.render('list');
							views.push(doc_view);
						}
					} else {
						var doc_view = new app.views.document({
							model: doc
						});
						rows_to_append.push(doc_view.$el);
						//doc_view.render('list');
						views.push(doc_view);
					}
				}, this);
			} else {
				_.each(this.collection.where(filter_args), function(doc) {
					if (searchString) {
						if ( this._searchDoc(doc) ) {
							var doc_view = new app.views.document({
								model: doc
							});
							rows_to_append.push(doc_view.$el);
							//doc_view.render('list');
							views.push(doc_view);
						}
					} else {
						var doc_view = new app.views.document({
							model: doc
						});
						rows_to_append.push(doc_view.$el);
						//doc_view.render('list');
						views.push(doc_view);
					}
				}, this);
			}
		};
		_filterCollection = _.bind(_filterCollection, this);

		if (this.model.get('document_hierarchy') !== false) {
			for (var i = this.model.get('document_hierarchy').tabs.length - 1; i >= 0; i--) {
				if (this.model.get('document_hierarchy').tabs[i].slug == filter_args.category_slug) {
					var tab_cat = this.model.get('document_hierarchy').tabs[i];
				}
			}
			// loop through folders if they exist and render those.
			if (typeof tab_cat !== 'undefined' && tab_cat.folders) {
				for (var i = 0; i < tab_cat.folders.length; i++) {
					var category_object = tab_cat.folders[i];
					var folder_template = _.template($('#document-list-folder').html());
					var $folder = $(folder_template(tab_cat.folders[i]).trim());				
					rows_to_append.push($folder);

					filter_args.category_slug = tab_cat.folders[i].slug;
					_filterCollection();
				}
			}
		}

		// filter the remaining documents (not in folders)
		// filter_args.category_slug = filter;

		// console.log('filter object:');
		// console.log(filter_args);
		// console.log('collection:');
		// console.log(this.collection);

		_filterCollection();

		return {
			rows_to_append : rows_to_append,
			views : views
		};

	},

	// expects a document model
	_searchDoc : function(doc) {
		searchString = this.model.get('searchString');
		return (!!doc.get('category_name') && doc.get('category_name').toLowerCase().indexOf(searchString) > -1 ||
				!!doc.get('document').title && doc.get('document').title.toLowerCase().indexOf(searchString) > -1 ||
				!!doc.get('document').mime_type && doc.get('document').mime_type.toLowerCase().indexOf(searchString) > -1 ||
				!!doc.get('comments') && doc.get('comments').toLowerCase().indexOf(searchString) > -1 ||
				!!doc.get('post_title') && doc.get('post_title').toLowerCase().indexOf(searchString) > -1 ||
				!!doc.get('post_status') && doc.get('post_status').toLowerCase().indexOf(searchString) > -1 ||
				!!doc.get('nice_status') && doc.get('nice_status').toLowerCase().indexOf(searchString) > -1 ||
				!!doc.get('document_language') && doc.get('document_language').toLowerCase().indexOf(searchString) > -1 );
	},

	toggle_tab_event : function(e) {
		var filter_object = this.model.get('filter_object');
		filter_object.category_slug = $(e.currentTarget).data('filter');
		this.model.set('filter_object', filter_object);
		// mdoel wont notice the change because filter object is just an object, model doesnt track it's properties.
		this.model.trigger('change');
	},

	toggle_folder : function(e) {
		$(e.currentTarget).toggleClass('collapsed');
		var folder_slug = $(e.currentTarget).data('folder');
		this.$el.find('.document.' + folder_slug).toggleClass('hidden');
	},

	actions : function(e) {
		
		var already_open = $(e.target).closest('.actions-wrap').hasClass('active-menu');
		this.$el.find('.actions-wrap').removeClass('active-menu');
		if (already_open) {
			// do nothing, already closed.
		} else {
			$(e.target).closest('.actions-wrap').addClass('active-menu');
		}
		e.stopPropagation();

	},

	search : function(e) {
		// DO THIS 
		e.preventDefault();
		var searchString = this.$el.find('#search-docs').val();
		this.model.set('searchString', searchString);
		//this.render();
		this.renderList();
	},

	remove : function() {
		if (this.interval_id) {
			window.clearInterval(this.interval_id);
		}
		Backbone.View.prototype.remove.call(this);
	}

});

