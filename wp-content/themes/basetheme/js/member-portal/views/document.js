app = app || {};

app.views.document = Backbone.View.extend ( {

	className : 'row document',	

	events : {
		'click .actions-wrap .approve' : 'approve',
		'click .actions-wrap .reject' : 'reject',
		'click .actions-wrap .view' : 'view',
		'click .actions-wrap .pending' : 'pending',
		'click .actions-wrap .destroy' : 'destroyDoc',
		'click .actions-wrap .download' : 'download'
	},

	// this view is initialized with a model.
	initialize : function (options) {
		this.template = _.template($('#document-list-item').html());

		var view = this;
		this.model.on('change', function() {
			if (typeof this.changed.show_status !== 'undefined') return;
			this.refresh_permissions();
			this.set_nice_status();
			console.log(this.get('show_status'));
			console.log(this.changed);
			view.render();
			view.$el.addClass('updated');
			setTimeout(function() {
				view.$el.removeClass('updated');
			}, 1000);
		});
		this.interval_id = false;
		this.check_for_update = _.bind(this.check_for_update, this);
	},

	render : function() {
		this.$el.html(this.template(this.model.toJSON()));
		this.$el.addClass(this.model.get('category_slug'));
		switch (this.model.get('post_status')) {
			case 'pending':
				this.$el.removeClass('publish trash');
				this.$el.addClass('pending');
				break;
			case 'publish':
				this.$el.removeClass('pending trash');
				this.$el.addClass('public');
				break;
			case 'trash':
				this.$el.removeClass('pending publish');
				this.$el.addClass('trash');
				break;
			default:
				this.$el.removeClass('pending publish trash');
				break;
		}
		var view = this;
		if (!this.interval_id) {
			this.interval_id = window.setInterval(function () {
				view.check_for_update();
			}, app.ajax_update_interval_doc);
		}
	},

	download : function() {
		if (this.model.get('document') !== false) {
			window.open(this.model.get('document').url,'_blank');
		} else {
			alert('this document has no file attached.');
		}
	},

	check_for_update : function() {
		this._create_ajax('mp_get_document')
			.done(function(response) {
				if (response.success) {
					this.model.set(response.document);
				} else {
					app.debug('ERROR - Check for update');
					app.debug(response);
				}
			});
	},

	approve : function(e) {
		this._create_ajax('mp_approve_document')
			.done(function(response) {
				if (response.success) {
					app.debug('approve document, ajax response:');
					app.debug(response)
					this.model.set(response.document);
				} else {
					app.debug('ERROR - Approving Doc');
					app.debug(response);
				}
			});
	},

	reject : function(e) {
		this._create_ajax('mp_reject_document')
			.done(function(response) {
				if (response.success) {
					app.debug('trashed document, ajax response:');
					app.debug(response);
					this.model.set(response.document);
				} else {
					app.debug('ERROR - Rejecting Doc');
					app.debug(response);
				}
			});
	},

	pending : function(e) {
		this._create_ajax('mp_pending_document')
			.done(function(response) {
				if (response.success) {
					app.debug('pending document, ajax response:');
					app.debug(response)
					this.model.set(response.document);
				} else {
					app.debug('ERROR - Pending Doc');
					app.debug(response);
				}
			});
	},

	destroyDoc : function(e) {
		this._create_ajax('mp_destroy_document')
			.done(function(response) {
				if (response.success) {
					app.debug('destroying document, ajax response:');
					app.debug(response)
					this.model.set(response.document);
				} else {
					app.debug('ERROR - Destroying Doc');
					app.debug(response);
				}
			});
	},

	_create_ajax : function(action) {
		return $.ajax({
			type : "post",
			url : '/wp-admin/admin-ajax.php',
			context : this,
			data : {
				action : action,
				document_id : this.model.get('id')
			},
			dataType : 'json',
			fail : function(response, text, error) {
				console.log('ajax ERROR');
				console.log(response);
				console.log(text);
				console.log(error);
			}
		});
	},

	view : function(e) {
		app.interface.open_overlay_document(this.model);
	},

	destroy : function() {
		this.remove();
		if (this.interval_id) {
			window.clearInterval(this.interval_id);
		}
		this.$el.empty();
		this.$el.remove();
	}

});