app = app || {};

app.views.hospitalListItem = Backbone.View.extend ( {

	className : 'row hospital route',	

	events : {},

	// this view is initialized with a model.
	initialize : function (options) {
		this.template = _.template($('#hospital-partner-list-item').html());
	},

	render : function() {
		this.$el.html(this.template(this.model.toJSON()));
		this.$el.attr('data-route', 'hospital/' + this.model.get('post_name'));
	}
});