app = app || {};

app.views.home = Backbone.View.extend ( {

	className: 'home',

	initialize : function (options) {
		this.template = _.template($('#home').html());
	},

	render : function () {
		var is_admin = app.mediator.is_admin();
		this.$el.html(this.template({
			is_admin : is_admin
		}));

		// get documents, append, and render
		this.recent_documents_view = this.getHomeDocuments();
		this.$el.find('#documents').append(this.recent_documents_view.$el);
		this.recent_documents_view.render();

		if (is_admin) {
			// get hospital partners, append, and render
			this.hospitals_view = this.getHospitals();
			this.$el.find('#hospitals').append(this.hospitals_view.$el);
			this.hospitals_view.render();
		}

	},

	getHomeDocuments : function () {

		var filter_object = {};

		if (!app.mediator.is_admin()) filter_object.post_author = app.mediator.get_user_id();

		return new app.views.documentList({
			model : new app.models.documentList({
				filter_object : filter_object,
				show_status : true
			})
		});
	},

	getHospitals : function () {
		return new app.views.hospitalList();
	},

	remove : function () {
		this.recent_documents_view.remove();
		Backbone.View.prototype.remove.call(this);
	}

});