app = app || {};

// this is the main controller for the member portal, contains most if not all routing functions.
app.views.interface = Backbone.View.extend ( {
	
	el : '#member-portal',	
	
	initialize : function (options) {
		app.debug('initialized interface view');
		this.$content_area = this.$el.find('#content-area');

		if (typeof member_data.message_type !== 'undefined') {
			console.log(member_data.message_type);
		}
	},
	
	events : {
		'click .route' : 'route',
		'click .trigger-overlay.upload' : 'open_overlay_upload',
		'click .trigger-overlay.compose' : 'open_overlay_compose',
		'click' : 'close_open_active_menu'
	},
	
	render : function ($inner_el) {
		this.$content_area.append($inner_el);
	},
	
	route : function (e) {
		e.preventDefault();
		var dest = $(e.target).data('route');
		if (typeof dest === 'undefined') {
			dest = $(e.currentTarget).data('route');
		}
		if (typeof dest !== 'undefined') {
			app.debug('update route: ' + dest);
			app.router.navigate(dest, {trigger: true});
		} else {
			console.log('issue with route, here is e:');
			console.log(e);
		}
	},

	reject_document : function(e) {
		e.preventDefault();
	},

	open_overlay_upload : function(e) {
		var overlay_view = new app.views.overlays.upload();
		this.open_overlay(overlay_view);
	},

	open_overlay_compose : function(e) {
		var overlay_view = new app.views.overlays.compose();
		this.open_overlay(overlay_view);
	},

	// this gets called from the document view
	open_overlay_document : function(model) {
		var overlay_view = new app.views.overlays.document({model: model});
		this.open_overlay(overlay_view);
	},

	open_overlay : function(inner_view) {
		var overlay = new app.views.overlay({
			'inner_view' : inner_view
		})
		this.$el.append(overlay.$el);
		overlay.render();
	},

	close_open_active_menu : function(e) {
		if ($('.active-menu').length) {
			$('.active-menu').removeClass('active-menu');
		};
	},

	destroy : function () {

	}

});