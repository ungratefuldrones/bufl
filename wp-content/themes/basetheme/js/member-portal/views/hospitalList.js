app = app || {};

app.views.hospitalList = Backbone.View.extend ( {

	className : 'hospital-list',

	events : {
		'click .hospital' : ''
	},

	initialize : function (options) {
		this.collection = app.hospitals; // this is a global collection created in app.js
		this.template = _.template($('#hospital-partners-template').html());
	},

	render : function () {
		this.$el.html(this.template());
		var $list_wrap = this.$el.find('#hospital-partner-list');
		this.collection.each(function(hospital_model) {
			var view = new app.views.hospitalListItem({
				model : hospital_model
			});
			$list_wrap.append(view.$el);
			view.render();
			console.log(view.$el);
		});
	}

} );