app = app || {};

app.models.reportList = Backbone.Model.extend ( {
	
	defaults: {
		active_filter : 'report',
	}

} );

app.views.reportList = Backbone.View.extend ( {

	className : 'reporting',

	events : {
		'click .report-nav-action' : 'toggle_table_event'
	},

	initialize : function (options) {

		this.template = _.template($('#reportList').html());
		this.collection = new app.collections.Documents(member_data.documents);

		this.model = new app.models.reportList();
		this.listenTo(this.model, 'change', this.renderList);
		this.views = [];

	},

	render : function () {
		this.$el.html(this.template({}));
		this.renderList();
	},

	renderList : function () {
		
		var filter = this.model.get('active_filter'),
			$tab = $('.report-nav-action.' + filter),
			container = document.createDocumentFragment()
		;

		// remove all the current views
		for (var i = this.views.length - 1; i >= 0; i--) {
			this.views[i].destroy();
		};
		this.views = [];
		var views = this.views;

		// filter the collection and create a view for each document
		// render the view and append to container fragment
		_.each(this.collection.where({document_type: filter}), function(doc) {
			var doc_view = new app.views.document({
				model: doc
			});
			container.appendChild(doc_view.render()[0]);
			views.push(doc_view);
		});

		// finally append the document fragment to the list wrapper
		this.$el.find('.list-table').append(container);

		// set the tab as active
		$('.report-nav-action').removeClass('ui-tabs-active');
		$tab.addClass('ui-tabs-active');

	},

	toggle_table_event : function (e) {
		this.model.set('active_filter', $(e.currentTarget).data('filter'));
	}

});