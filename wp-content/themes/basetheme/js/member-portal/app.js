app = {
	models : {},
	views : {},
	collections : {},
	router : {},
	debugFlag : true,	
	ajax_update_interval_doc : 7000, // 7 seconds
	ajax_update_interval_doc_list : 120000, // 2 minutes. When doc and doc_list update at the same time, gui problems can occur. Not sure if there is any solution for this
	member_documents : {},

	init: function () {
		
		this.debug('started backbone');

		this.member_documents = new this.collections.documents(member_data.documents);
		this.hospitals = new this.collections.hospitals(member_data.hospital_partners);

		this.interface = new this.views.interface();

		this.router = new this.router();
		Backbone.history.start({root : '/'});

	},

	debug : function(msg) {
		if (app.debugFlag) {
			console.log(msg);
		}
	}

}

/*
	status possibilities:
		approved - maps to wordpress publish
		rejected - maps to wordpress trash
		pending - maps to wordpress pending
*/

app.mediator = {

	document_actions : function(document_model) {
		if (_.contains(member_data.userinfo.roles, 'administrator')) {
			switch (document_model.get('post_status')) {
				case 'publish' :
					return {
						'view' : 'View/Edit',
						'download' : 'Download',
						'reject' : 'Reject',
					}
				case 'pending' :
					return {
						'view' : 'View/Edit',
						'download' : 'Download',
						'approve' : 'Approve',
						'reject' : 'Reject',
					}
				case 'trash' :
					return {
						'view' : 'View/Edit',
						'download' : 'Download',
						'pending' : 'Move to Pending',
						'destroy' : 'Delete Permanently'
					}
			}
		} else if (_.contains(member_data.userinfo.roles, 'member')) {
			switch (document_model.get('post_status')) {
				case 'publish' :
					return {
						'view' : 'View/Edit',
						'download' : 'Download'
					}
				case 'pending' :
					return {
						'view' : 'View/Edit',
						'download' : 'Download',
						'destroy' : 'Delete Permanently'
					}
				case 'trash' :
					return {
						'view' : 'View/Edit',
						'download' : 'Download',
						'destroy' : 'Delete Permanently'
					}
			}
		}
	},

	document_user_can : function(action, id) {
		switch (action) {
			case 'edit' :
				if (_.contains(member_data.userinfo.roles, 'administrator')) {
					return true;
				} else if (_.contains(member_data.userinfo.roles, 'member')) {
					// im wondering if this should also check the status of the post,
					// not sure if member should be allowed to edit things that are already published.
					if ( parseInt(member_data.userinfo.ID) ===  parseInt(app.member_documents.get(id).get('post_author')) ) return true;
				}
				return false;
		}
	},

	message_user_can : function(action, id) {
		// todo
	},

	is_admin : function() {
		if (_.contains(member_data.userinfo.roles, 'administrator')) {
			return true;
		}
		return false;
	},

	get_user_id : function() {
		return member_data.userinfo.ID;
	}

}

// start the application
$(function(){

	app.init();

});