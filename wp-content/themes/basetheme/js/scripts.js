$(function() {


	/********************************************
		mp survey
	********************************************/

	var $surveyWrapQs = $('#survey .wrap .questions'),
		$surveyBackBtn = $surveyWrapQs.find('.back'),
		$surveyNextBtn = $surveyWrapQs.find('.next'),
		$animationBarBlue = $surveyWrapQs.find('.bar .bar-animation .blue'),
		$animationBarP = $surveyWrapQs.find('.bar p span');

	var mpSurveyArr = [];
	// mpSurveyArr[0] = Language
	// mpSurveyArr[1] = ID#
	// mpSurveyArr[2] = Date (MM/DD/YYYY)
	// mpSurveyArr[3] = Class
	// mpSurveyArr[4] = Ethnicity
	// mpSurveyArr[5] = Age Group (Adult/Teen)
	// mpSurveyArr[6] = Question 1
	// mpSurveyArr[7] = Question 2
	// mpSurveyArr[8] = Question 3
	// mpSurveyArr[9] = Question 4
	// mpSurveyArr[10] = Question 5
	// mpSurveyArr[11] = Question 6
	// mpSurveyArr[12] = Question 7
	// mpSurveyArr[13] = Question 8
	// mpSurveyArr[14] = Question 9
	// mpSurveyArr[15] = Question 10
	// mpSurveyArr[16] = Survey Count
	
	
	// survey count
	var survey_counter = parseInt ($('.counter-wrapper .counter').text());
	updateSurveyCounter (survey_counter);
	
	// add today's date automatically in pre-screen 2
	if (document.getElementById('date')) {
		todayDate = new Date();
		var dateString = formatDate(todayDate);
		document.getElementById('date').value = dateString;
	}

	// first, third and fourth pre-screen clicks
	$('.pre1 a, .pre3 a, .pre4 a').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		mpSurveyArr.push( $this.data('info') );
		
		$this.closest('.pre').fadeOut(500, function() {
			if ( $this.closest('.pre').hasClass('pre1') || $this.closest('.pre').hasClass('pre3') ) {
				$this.closest('.pre').next().fadeIn(500);
			}
			else {
				var findClass = mpSurveyArr[0] + "-" + $this.data('info');
				$('#survey .wrap .questions.' + findClass).fadeIn(500);
				$('#survey .wrap .questions.' + findClass + ' .top fieldset.active').fadeIn(100);
			}
		});
	});

	// second pre-screen clicks
	var $pre2 = $('#survey .wrap .pre2');

	$pre2.find('.purple-survey-btn').click(function(e) {
		e.preventDefault();

		// check that there is something in id input
		var idNumber =  $('#id-number').val();
		if ( idNumber.length > 0 ) {
        	$('#survey .wrap .pre2').addClass('one');
        }
        else {
        	$('#errors .id').fadeIn(300);
        	setTimeout(function() {
        		$('#errors .id').fadeOut(300);
        	}, 1000);
        }

        // check that date format is correct in input
		var txtVal =  $('#date').val();
        if ( isDate(txtVal) ) {
            $pre2.addClass('two');
        }
        else {
        	$('#errors .date').fadeIn(300);
        	setTimeout(function() {
        		$('#errors .date').fadeOut(300);
        	}, 1000);
        }

        // check that there is something in id input
        if ( $('#class1').is(':checked') || $('#class2').is(':checked') ) {
        	$pre2.addClass('three');
        }
        else {
        	$('#errors .class').fadeIn(300);
        	setTimeout(function() {
        		$('#errors .class').fadeOut(300);
        	}, 1000);
        }

		if ( $pre2.hasClass('one') &&  $pre2.hasClass('two') && $pre2.hasClass('three') ) {
			mpSurveyArr.push( $('#id-number').val() );
			mpSurveyArr.push( $('#date').val() );
			if ( $('#class1').is(':checked') ) {
        		mpSurveyArr.push( $('#class1').val() );
        	}
        	else {
        		mpSurveyArr.push( $('#class2').val() );
        	}
			
			$pre2.fadeOut(500, function() {
				$('.pre3').fadeIn(500);
			});
		}

	});
	
	/* on change of questions radio buttons */

	$surveyWrapQs.find('input[type="radio"]').change(function() {
		$(this).siblings().toggleClass('active');
		$(this).closest('.item').siblings().find('span').removeClass('active');

		var arrayNum = $(this).closest('fieldset').index() + 6;
		if(typeof mpSurveyArr[arrayNum] == 'undefined') {
		    mpSurveyArr.push( $(this).val() );
		}
		else {
		    mpSurveyArr[arrayNum] = $(this).val();
		}
		

		$surveyNextBtn.addClass('active');
	});
	
	/* next click */

	var AJAX_URL = '/wp-admin/admin-ajax.php';
	
	$surveyNextBtn.click(function(e) {
		e.preventDefault();
		if ( $(':animated').length ) {
        	return false;
    	}		
		var $this = $(this),
			$fieldset = $this.closest('.questions').find('.top fieldset'),
			$fieldsetActive = $this.closest('.questions').find('.top fieldset.active'),
			$activeIndex = $fieldsetActive.index();
		if ( $this.hasClass('active') ) {
			if ( $activeIndex == 9 ) {
				$(this).closest('.questions').fadeOut(500, function() {
					$('#survey .wrap .final').fadeIn(500);
										
					var ajax_input = {
				            'language' : mpSurveyArr [0],
				            'person_id' : mpSurveyArr [1],
				            'date' : mpSurveyArr [2],
				            'pre_post_class' : mpSurveyArr [3],
				            'ethnicity' : mpSurveyArr [4],
				            'age_group' : mpSurveyArr [5],
				            'q1' : mpSurveyArr [6],
				            'q2' : mpSurveyArr [7],
				            'q3' : mpSurveyArr [8],
				            'q4' : mpSurveyArr [9],
				            'q5' : mpSurveyArr [10],
				            'q6' : mpSurveyArr [11],
				            'q7' : mpSurveyArr [12],
				            'q8' : mpSurveyArr [13],
				            'q9' : mpSurveyArr [14],
				            'q10' : mpSurveyArr [15],
				            'user_id' : parseInt ($this.parents ('form').attr ('data-user-id')),
				            action: 'bufl_ajax_survey_input'
				    };
					
					$.ajax({
						type: 'POST',						
						//url: '/wp-content/themes/basetheme/assets/inc-ajax-survey-form-input.php',
						url: AJAX_URL,
						data: ajax_input,		
						success:function(response){ 
							// console_log ('ajax_finished');
							
						}
					});	
					
					survey_counter++;
					updateSurveyCounter (survey_counter);
				});
			}
			else {
				$fieldset.eq($activeIndex).addClass('complete').removeClass('active').fadeOut(200, function() {
				$fieldset.eq($activeIndex + 1).addClass('active').fadeIn(500);
				});

				if ( !$fieldset.eq($activeIndex+1).hasClass('complete') ) {
					$this.removeClass('active').siblings().addClass('active');
				}
				else {
					$this.siblings().addClass('active');
				}
				if ($activeIndex == 8) {
					$(this).text('Complete & Submit');
				}
				$animationBarBlue.animate({'width': ($activeIndex+1)*10 + '%'});
				$animationBarP.html($activeIndex+1);				
			}
		}
	});

	/* back click */
	
	$surveyBackBtn.click(function(e) {
		e.preventDefault();
		if ( $(':animated').length ) {
        	return false;
    	}
		var $this = $(this),
			$fieldset = $this.closest('.questions').find('.top fieldset'),
			$fieldsetActive = $this.closest('.questions').find('.top fieldset.active');
		if ( $this.hasClass('active') ) {
			var $activeIndex = $fieldsetActive.index();
			$fieldset.eq($activeIndex).removeClass('active').fadeOut(200, function() {
				$fieldset.eq($activeIndex - 1).addClass('active').fadeIn(500);
			});
			if ( !$fieldset.eq($activeIndex).hasClass('complete') ) {
				$this.siblings().addClass('active');
			}
			if ($activeIndex == 1) {
				$(this).removeClass('active');
			}
			else if ($activeIndex == 9) {
				$(this).siblings().text('Next Question');
			}
			$animationBarBlue.animate({'width': ($activeIndex-1)*10 + '%'});
			$animationBarP.html($activeIndex-1);
		}
	});
	
	
	/* reload page */

	$('a.reload').click(function(e) {
		e.preventDefault();
		location.reload(true);
	});




	/********************************************
		Grant Application page
	********************************************/
		
	$('.trigger-expand').click ( function (e) {
		
		e.preventDefault();
		
		var $this = $(this);
		
		var $content = $(this).siblings('.content-expand');
		
		if (!$this.hasClass ('clicked')) {
			
			var height = 0;
			var $children = $content.children();
			$children.each ( function() {
				height += $(this).outerHeight (true);
			});
			$this.addClass ('clicked');
			setTimeout ( function () {				
				$content.animate ( {
					height: height
				}, 300);
			}, 200);
			
		}
		else {
			
			$content.animate ( {
				height: 0
			}, 300, function () { 
				$this.removeClass ('clicked');
			});
			
		}
		
	});
	
	$('.show-form').click ( function (e) {
		e.preventDefault ();
		
		$(this).parent().next('.form-wrapper').fadeIn (300);
	});
	
	// placeholders for grant application form, section 1
	$('.form-wrapper label.as-placeholder').each ( function () {
		var $this = $(this);
		
		var label_copy = cleanLabel ($this.html());
		$this.next('input').attr ('placeholder', label_copy);
	});
	
	// detect file upload
    $(document).on('change', '.file :file', function() {	
        var input = $(this),
	        numFiles = input.get(0).files ? input.get(0).files.length : 1,	
	        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	        
	    //$('.file .file-upload').addClass('clicked').html (label + ' attached.');
	    $('.file .file-upload').addClass('clicked');
	    $('.form-field.file').append (label + ' attached.');
	        
    });

	/********************************************
		hospital partners page
	********************************************/

	/* init slider */

	function partnerSlider() {
		$('#partner-slider').cycle({ 
			speed: 500,
			timeout: 10000000000000,
		    fx: 'slide',
		    slides: '.detail',
		    log: false,
		    autoHeight: 'calc'
		});
	}

	/* on click of grid image, go to that slide */

	$('.partner-wrap .grid .detail').click(function() {
		var $thisClass = $(this).attr('class').substr(7),
			$thisIndex = $thisClass - 1;

		$('.partner-wrap .grid').fadeOut(500, function() {
			$('#partner-slider').fadeIn(500);
			partnerSlider();
			$('#partner-slider').cycle($thisIndex);
		});
		
	});

	/* go back to grid */

	$('.partner-wrap .detailed-wrap .detail .left .view-all').click(function() {
		$('#partner-slider').fadeOut(500, function() {
			$('.partner-wrap .grid').fadeIn(500);
		});
	});






	/********************************************
		campaign page
	********************************************/

	/* init audio.js */

	audiojs.events.ready(function() {
		var as = audiojs.createAll();
	});

	
	/********************************************
	 * Member Portal
	 ********************************************/
	
	$('.trigger-overlay').click ( function (e) {
		//e.preventDefault();
		
		var $this = $(this),
			$target = $(e.currentTarget),
			target_overlay_id = $this.attr('href');

		if (target_overlay_id == '#overlay-delete') {
			$this.addClass ('delete-me');
			e.preventDefault();
		}
		
		openOverlay (target_overlay_id);		
	});
	
	$('.overlay-close').click ( function(e) {
		e.preventDefault();
		if ($(this).parents('.overlay').attr ('id') == 'overlay-delete') {
			$('.delete-me').removeClass ('delete-me');
		}
		
		closeOverlay ();
		window.location.hash = '';
	});

	// detect a hash in the URL and open the corresponding overlay
	if (window.location.hash && window.location.hash != '') {
		openOverlay (window.location.hash);		
	}
	
	// placeholders for login form
	$('.overlay form#loginform label').each ( function () {
		var $this = $(this);
		
		var label_copy = $this.text();
		$this.next('input').attr ('placeholder', label_copy);
	});
	
	// placeholders for formidable forms
	$('.overlay .frm_form_fields label').each ( function () {
		var $this = $(this);
		
		var label_copy = $this.html().split ('<span')[0];	// removed the *required span, if present
		$this.next('input').attr ('placeholder', label_copy);
	});
	
	$("#overlay-delete .button-primary").click ( function () {
		closeOverlay();
		
		var $delete = $('.delete-me');
		var $row = $delete.parents ('.row-div');
		if ($row.length) {
			$row.animate ( {
				opacity: 0
			}, 600,
				function () {
					$(this).remove();
				});
		}
		else {
			var $alert = $delete.parents ('.alert');
			
			if ($alert.length) {
				$alert.animate ( {
					opacity: 0
				}, 600,
					function () {
						$(this).remove();
					});
			}
		}
		
	});
	
	$('.sidebar-nav li').hover ( function () {
		$(this).addClass ('hover');
	}, function () {
		$(this).removeClass ('hover');
	});
	
	// clicking Cancel on the Upload overlay form
	$('.frm_cancel').click ( function () {

		$('.overlay-close').trigger ('click');
	});
	
	// MP sidebar height
	if ($('#member-portal .sidebar').length) {
		$('#member-portal .main').height ($(window).height() - $('#member-portal .header').height() - $('#member-portal .top-line').height());		
	}
	
	/****************************************
		MP Accordion Table Categories
	****************************************/
	
	$('.table-toggable .category-header').click(function(e){
		var $this = $(this);
		var $catItems = $this.next('.category-items');
	
		$this.toggleClass('toggle-open').siblings('.category-header').removeClass('toggle-open');
		$catItems.toggleClass('category-items-active').siblings().removeClass('category-items-active');
	
	});
	
	/****************************************
		MP Dropdowns
	****************************************/
	
	$('.drop-link').click(function(e){
		e.preventDefault();
	
		$(this).siblings('ul').toggle();
	
	
	// $(this).parent('ul').mouseleave(function(){
	//    	$(this).hide();
	// });
	
	});
	
	/****************************************
		MP Favorites Toggling
	****************************************/
	
	$('.cell-favorites .icon-star').click(function(e){
		e.preventDefault();
	
		$(this).toggleClass('star-on');
	});
	
	$( "#accordion" ).accordion({
		"header":'h3'
	});
	   //capture the click on the a tag
	   $(".accordion h3 a").click(function() {
	      window.location = $(this).attr('href');
	      return false;
	   });
	$( "#tabs" ).tabs();

	
	/****************************************
		placeholders viewable in IE8 + 9
	****************************************/
	
	if (! placeholderIsSupported() ) {
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		    	input.removeClass('placeholder');
		  	}
		}).blur(function() {
			var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
		    	input.addClass('placeholder');
		    	input.val(input.attr('placeholder'));
		  	}
		}).blur();
	}
	
	// load homepage slideshow
	var t = setTimeout ( function () {
		$('#homepage-slideshow .slide').fadeIn(200);
	}, 400);
	
	});

	if (supports_html5_storage) console.log ('supports html5 storage')
	else console.log ('does not support html5 storage');

///////////////////////////////
///////////////////////////////
//Member Portal
///////////////////////////////
///////////////////////////////

//Overlays

function fixBody () {
	$('body').addClass ('fixed');
}

function unfixBody () {
	$('body').removeClass ('fixed');
}

function openOverlay (overlay_id) {
	
	// close menu
	/*if ($('#menu').is(":visible")) {
		$("#menuClose").trigger ('click');
	}
	
	if (overlay_id == '#overlay-login') {
		if ($('body').hasClass ('logged-in')) return;
	}
	
	closeOverlay ();
	setTimeout ( function () {		
		fixBody();
		$('.overlays-wrapper').fadeIn ( 300, function () {
			$(overlay_id).fadeIn (100, function () {
				afterOpen($(this));
			});
			beforeOpen ($(overlay_id));
		});
	}, 100);*/

}

function beforeOpen ($form) {
	// var sum = 0;
	// $form.children().each(function () { 
	// 	sum += sum + $(this).innerHeight(); 
	// })
	// $form.css('height', sum);
}

function afterOpen ($form) {

	// var $all_recip_input = $form.find('#frm_field_60_container input'),
	// 	$single_recip_select = $form.find('#frm_field_56_container select'),
	// 	$frm_message = $form.find('.frm_message')
	// ;

	// $form.addClass ('active');

	// // remove the confirmation message after time limit
	// if ($frm_message.length) {
	// 	setTimeout(function() {
	// 		$frm_message.css('margin-top', '-32px');
	// 	}, 7500);
	// }

	// if ($all_recip_input.length) {
	// 	$all_recip_input.on('click', function(){
	// 		$single_recip_select[0].disabled = !($single_recip_select[0].disabled);
	// 		$single_recip_select[0].selectedIndex = 0;
	// 	});
	// }

	// /*
	// if ($single_recip_select.length) {
	// 	$single_recip_select.on('change', function() {
	// 		if($(this).val() != 'Enter Recipient') {
	// 			$all_recip_input[0].disabled = true;
	// 		} else {
	// 			$all_recip_input[0].disabled = false;
	// 		}
	// 	});
	// }*/

}

function closeOverlay () {

	// clearForm();
	
	// $('.overlay.active').fadeOut ( 100, function () {
	// 	$(this).removeClass ('active');	
	// 	$('.overlays-wrapper').fadeOut (100);
	// 	unfixBody();
	// });

} 

function placeholderIsSupported() {
    var test = document.createElement('input');
    return ('placeholder' in test);
}

function cleanLabel (label) {
	return label.split('<')[0].replace(/\r\n/g, "")
}

function supports_html5_storage() {
	try {
		return 'localStorage' in window && window['localStorage'] !== null;
	} catch (e) {
		return false;
	}
}

function clearForm () {
	var $form = $('.overlay.active form');
	
	if ($form.length) {
		$form.find ('input[type="text"], input[type="file"], textarea').each (function () {
			$(this).val('');
		});
		
		$form.find ('select').each ( function () {
			$(this).val ($(this).find ('option:first-child').val());
		});
		
		$form.find ('input[type="checkbox"]').each ( function () {
			$(this).attr ('checked', false);
		});
		
		$form.find ('.frm_error_style, .frm_error').each ( function () {
			$(this).remove();
		});
	}	
}



function formatDate(date) {
	if ( date.getMonth()+1 < 10 ) {
		var month = ("0" + (date.getMonth() + 1)).slice(-2);
	}
	else {
		var month = date.getMonth()+1;
	}
	if ( date.getDate() < 10 ) {
		var day = ("0" + date.getDate()).slice(-2);
	}
	else {
		var day = date.getDate();
	}
		return month + "/" + day + "/" + date.getFullYear();
}
function isDate(txtDate) {
    var currVal = txtDate;
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    return true;
}

function updateSurveyCounter (num) {	
	var counter = $('.counter-wrapper .counter');
	var wrapper = $('.counter-wrapper');
	counter.text (num);

	if (num > 0) {
		wrapper.addClass ('show');
	}
	else {
		wrapper.removeClass ('show');
	}
}

function console_log (literal) {
	return window.console && console.log (literal);
}