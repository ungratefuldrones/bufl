$(document).ready(function() {
			setupMainMenu();
		});
		
		var activeMainItem;
		
		//Mobile items
		var mobileMenuOpen = false;
		
		
		function setupMainMenu() {
			$("#mobileMenu").click(function() {
				    $("#navigationWrapper").css("display", "block");
					$("#mobileClose").css("display", "block");
					$(".topItem ul").css("display", "none");
					$(".topItem").find("a:first").css("backgroundPosition", "right 12px");
					TweenMax.to("#navigationWrapper", .5, {autoAlpha:1, ease:Circ.easeOut});
					TweenMax.to("#mobileClose", .5, {autoAlpha:1, ease:Circ.easeOut});
					mobileMenuOpen = true;
			});
			$("#mobileClose").click(function() {
				    $("#navigationWrapper").css("opacity", 1);
					$("#mobileClose").css("opacity", 1);
					TweenMax.to("#navigationWrapper", .5, {autoAlpha:0, ease:Circ.easeOut});
					TweenMax.to("#mobileClose", .5, {autoAlpha:0, ease:Circ.easeOut});
					mobileMenuOpen = false;
			});
			$(".topItem").find("a:first").click(function(event) {
				event.preventDefault();
			});
			$(".topItem").click(function() {
				$(".topItem").find("a:first").css("backgroundPosition", "right 12px");
				$(".topItem ul").css("display", "none");
				activeMainItem = $(this).find("a:first");
				activeMainItem.css("backgroundPosition", "right -76px");
				activeDropdown = $(this).find("ul");
				activeDropdown.css("display", "block");
			});
		}
		
	
		
		
		
		
		