$(window).load(function() {
			startVoicesSlides();
			setupVoicesButtons();
		});
		
		
		///////////////////////////////
		///////////////////////////////
		//HERO CODE
		///////////////////////////////
		///////////////////////////////
		
		var totalVoicesSlides = 4;
		var currentVoicesSlide = 1;
		var previousVoicesSlide = $("#voicesSlide4");
		var voicesSlidesMoving = false;
		var voicesEaseLength = 2;
		var voicesEaseType = "Circ.easeInOut";
		var firstVoicesLoad = true;
		
		function startVoicesSlides() {
			timeVoicesSlides();
			changeVoicesSlideRight(1);
		}
		
		function timeVoicesSlides() {
			timer = setTimeout("changeVoicesSlides()",8000);
		}
		
		function setupVoicesButtons() {
			$("#voicesDot1").click(function() {
				clearTimeout(timer);
				changeVoicesSlideLeft(1);
			});
			$("#voicesDot2").click(function() {
				clearTimeout(timer);
				if (currentVoicesSlide > 2) {
					changeVoicesSlideLeft(2);
				} else {
					changeVoicesSlideRight(2);
				}
			});
			$("#voicesDot3").click(function() {
				clearTimeout(timer);
				if (currentVoicesSlide > 3) {
					changeVoicesSlideLeft(3);
				} else {
					changeVoicesSlideRight(3);
				}
			});
			$("#voicesDot4").click(function() {
				clearTimeout(timer);
				changeVoicesSlideRight(4);
			});
			$("#voicesWrapper .slideArrowRight").click(function() {
				if (!voicesSlidesMoving) {
					voicesRightButton();
					clearTimeout(timer);
				}
			});
			$("#voicesWrapper .slideArrowLeft").click(function() {
				if (!voicesSlidesMoving) {
					voicesLeftButton();
					clearTimeout(timer);
				}
			});
		}
		
		function changeVoicesSlides() {
			voicesRightButton();
			timeVoicesSlides();
		}
		
		
		function voicesRightButton() {
			if (currentVoicesSlide == totalVoicesSlides) {
				changeVoicesSlideRight(1);
			} else {
				changeVoicesSlideRight(currentVoicesSlide+1);
			}
		}
		
		function voicesLeftButton() {
			if (currentVoicesSlide == 1) {
				changeVoicesSlideLeft(totalVoicesSlides);
			} else {
				changeVoicesSlideLeft(currentVoicesSlide-1);
			}
		}
		
		function checkPreviousVoicesSlide() {
			switch(currentVoicesSlide) {
				case 1:
					previousVoicesSlide = $("#voicesSlide1");
				  break;
				case 2:
					previousVoicesSlide = $("#voicesSlide2");
				  break;
				  case 3:
				  	previousVoicesSlide = $("#voicesSlide3");
				  break;
				  case 4:
				    previousVoicesSlide = $("#voicesSlide4");
				  break;
				default:
				  //do nothing
			}
		}
		
		function changeVoicesSlideRight(voicesSlideNumber) {
			if (!firstVoicesLoad) {
				checkPreviousVoicesSlide();
				var dotNumber = "#voicesDot"+currentVoicesSlide;
				$(dotNumber).removeClass("activeDot");
			}
			voicesSlidesMoving = true;
			switch(voicesSlideNumber) {
				case 1:
					$("#voicesDot1").addClass("activeDot");
					currentVoicesSlide = 1;
					if (firstVoicesLoad) {
						TweenMax.to("#voicesSlide1", voicesEaseLength, {left:"0px", autoAlpha:1, ease:voicesEaseType});
						firstVoicesLoad = false;
					} else {
						$("#voicesSlide1").css("visibility", "visible");
						$("#voicesSlide1").css("left", "1040px");
						TweenMax.to("#voicesSlide1", voicesEaseLength, {left:"0px", ease:voicesEaseType});
					}
					TweenMax.to(previousVoicesSlide, voicesEaseLength, {left:"-1040px", ease:voicesEaseType, onComplete:hidePreviousVoicesSlide});
				  break;
				case 2:
					$("#voicesDot2").addClass("activeDot");
					currentVoicesSlide = 2;
					$("#voicesSlide2").css("visibility", "visible");
					$("#voicesSlide2").css("left", "1040px");
					TweenMax.to("#voicesSlide2", voicesEaseLength, {left:"0px", ease:voicesEaseType});
					TweenMax.to(previousVoicesSlide, voicesEaseLength, {left:"-1040px", ease:voicesEaseType, onComplete:hidePreviousVoicesSlide});
				  break;
				  case 3:
				  	$("#voicesDot3").addClass("activeDot");
					currentVoicesSlide = 3;
					$("#voicesSlide3").css("visibility", "visible");
					$("#voicesSlide3").css("left", "1040px");
					TweenMax.to("#voicesSlide3", voicesEaseLength, {left:"0px", ease:voicesEaseType});
					TweenMax.to(previousVoicesSlide, voicesEaseLength, {left:"-1040px", ease:voicesEaseType, onComplete:hidePreviousVoicesSlide});
				  break;
				  case 4:
				    $("#voicesDot4").addClass("activeDot");
					currentVoicesSlide = 4;
					$("#voicesSlide4").css("visibility", "visible");
					$("#voicesSlide4").css("left", "1040px");
					TweenMax.to("#voicesSlide4", voicesEaseLength, {left:"0px", ease:voicesEaseType});
					TweenMax.to(previousVoicesSlide, voicesEaseLength, {left:"-1040px", ease:voicesEaseType, onComplete:hidePreviousVoicesSlide});
				  break;
				default:
				  //do nothing
			}
		}
		
		function changeVoicesSlideLeft(voicesSlideNumber) {
			checkPreviousVoicesSlide();
			var dotNumber = "#voicesDot"+currentVoicesSlide;
			$(dotNumber).removeClass("activeDot");
			voicesSlidesMoving = true;
			switch(voicesSlideNumber) {
				case 1:
				    $("#voicesDot1").addClass("activeDot");
					currentVoicesSlide = 1;
					$("#voicesSlide1").css("visibility", "visible");
					$("#voicesSlide1").css("left", "-1040px");
					TweenMax.to("#voicesSlide1", voicesEaseLength, {left:"0px", ease:voicesEaseType});
					TweenMax.to(previousVoicesSlide, voicesEaseLength, {left:"1040px", ease:voicesEaseType, onComplete:hidePreviousVoicesSlide});
				  break;
				case 2:
					$("#voicesDot2").addClass("activeDot");
					currentVoicesSlide = 2;
					$("#voicesSlide2").css("visibility", "visible");
					$("#voicesSlide2").css("left", "-1040px");
					TweenMax.to("#voicesSlide2", voicesEaseLength, {left:"0px", ease:voicesEaseType});
					TweenMax.to(previousVoicesSlide, voicesEaseLength, {left:"1040px", ease:voicesEaseType, onComplete:hidePreviousVoicesSlide});
				  break;
				  case 3:
				    $("#voicesDot3").addClass("activeDot");
					currentVoicesSlide = 3;
					$("#voicesSlide3").css("visibility", "visible");
					$("#voicesSlide3").css("left", "-1040px");
					TweenMax.to("#voicesSlide3", voicesEaseLength, {left:"0px", ease:voicesEaseType});
					TweenMax.to(previousVoicesSlide, voicesEaseLength, {left:"1040px", ease:voicesEaseType, onComplete:hidePreviousVoicesSlide});
				  break;
				  case 4:
				  	$("#voicesDot4").addClass("activeDot");
					currentVoicesSlide = 4;
					$("#voicesSlide4").css("visibility", "visible");
					$("#voicesSlide4").css("left", "-1040px");
					TweenMax.to("#voicesSlide4", voicesEaseLength, {left:"0px", ease:voicesEaseType});
					TweenMax.to(previousVoicesSlide, voicesEaseLength, {left:"1040px", ease:voicesEaseType, onComplete:hidePreviousVoicesSlide});
				  break;
				default:
				  //do nothing
			}
		}
		
		function hidePreviousVoicesSlide() {
			voicesSlidesMoving = false;
			previousVoicesSlide.css("visibility", "hidden");
		}