// login via AJAX
jQuery(document).ready(function($) {
	$('#overlay-login form#loginform').on('submit', function(e){
	    $('#overlay-login form#loginform p.status').show().text(ajax_login_object.loadingmessage);
	    $.ajax({
	        type: 'POST',
	        dataType: 'json',
	        url: ajax_login_object.ajaxurl,
	        data: { 
	            'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
	            'username': $('#overlay-login form#loginform #username').val(), 
	            'password': $('#overlay-login form#loginform #password').val(), 
	            'rememberme': $('#overlay-login form#loginform #rememberme').is(':checked'), 
	            'security': $('#overlay-login form#loginform #security').val() },
	        success: function(data){
	            $('#overlay-login form#loginform p.status').html(data.message);
	            if (data.loggedin == true){
	            	
	            	// redirect to different pages depending on what type of user partner or admin
	            	if (data.caps == 'admin')
	            		document.location.href = ajax_login_object.redirecturl_admin;
	            	else document.location.href = ajax_login_object.redirecturl;
	            }
	        }
	    });
	    e.preventDefault();
	});
});