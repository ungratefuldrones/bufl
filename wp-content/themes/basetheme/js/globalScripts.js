$(document).ready(function() {
			setupMainMenu();
		});
		
		//resources area
		var activeMainItem;
		var menuItem;
		var activeDropdown;
		var previousMainItem;
		var previousDropdown;
		var dropdownHot = false;
		var firstLoad = true;
		var depth = 200;
		var menuTimer;
		
		//Mobile items
		var mobileMenuOpen = false;
		var mobileSearchOpen = false;
		
		
		function setupMainMenu() {
			$('.topItem').hover(
			  function () {
				 activeMainItem = $(this).find("a:first");
				 if (!firstLoad && previousMainItem != activeMainItem) {
					 previousDropdown.css("z-index", depth-1); 
				  }
				  //activeMainItem.css("backgroundPosition", "100% 0px");
				  if ($(this).hasClass('topItem1')) {
					  menuItem = 1;
					  activeMainItem.css("backgroundPosition", "0px 0px");
				  } else if ($(this).hasClass('topItem2')) {
					  menuItem = 2;
					  activeMainItem.css("backgroundPosition", "-300px 0px");
				  } else if ($(this).hasClass('topItem3')) {
					  menuItem = 3;
					  activeMainItem.css("backgroundPosition", "-600px 0px");
				  } else if ($(this).hasClass('topItem4')) {
					  menuItem = 4;
					  activeMainItem.css("backgroundPosition", "-900px 0px");
				  }
				 activeDropdown = $(this).find("ul");
				 activeDropdown.css("z-index", depth);
				 activeDropdown.css("display", "block");
				 TweenMax.to(activeDropdown, 1, {autoAlpha:1, overwrite:"all", ease:Circ.easeOut});
				 previousMainItem = activeMainItem;
				 previousDropdown = activeDropdown;
				 firstLoad = false;
			  }, 
			  function () {
					 if (menuItem == 1) {
					  	previousMainItem.css("backgroundPosition", "0px 40px");
					  } else if (menuItem == 2) {
						  previousMainItem.css("backgroundPosition", "-300px 40px");
					  } else if (menuItem == 3) {
						  previousMainItem.css("backgroundPosition", "-600px 40px");
					  } else if (menuItem == 4) {
						  previousMainItem.css("backgroundPosition", "-900px 40px");
					  }
					 previousDropdown.css("opacity", 0);
					 previousDropdown.css("display", "none");
					TweenMax.to(previousDropdown, 1, {delay:.25, autoAlpha:0, overwrite:"all", ease:Circ.easeOut, onComplete:deactivateDropdown});
			  }
			);
			
			$('.topItem ul').hover(
			  function () {
				 dropdownHot = true;
				 
			  }, 
			  function () {
				  
				 if (previousMainItem != activeMainItem) {
					TweenMax.to(previousDropdown, 1, {delay:.25, autoAlpha:0, overwrite:"all", ease:Circ.easeOut, onComplete:deactivateDropdown});
			  	}
			  }
			);
		}
		
		function deactivateDropdown() {
			dropdownHot = false;
		}
		
	
		





		
		
		