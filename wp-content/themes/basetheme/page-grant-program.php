
<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>

<div id="pageWrapper" <?php if ( embed() ) echo "class='embedded-tool'"; ?> class="grant-program" >
	
	<div id="pageTitle">		
		<h1><?php the_title(); ?></h1>
	</div>
	
	<div id="pageContent" class="clearfix">
		<img src="<?php echo bloginfo('template_directory'); ?>/images/grant-program-header-graphic.png" style="max-width: 100%;" />
		<div class="content-wrapper">
		
			<div class="share">
				Share
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
				<a class="addthis_button_facebook"></a>
				<a class="addthis_button_twitter"></a>
				<a class="addthis_button_email"></a>
				</div>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-537112717c9a1c63"></script>
				<!-- AddThis Button END -->
			</div>
			
			<?php the_content(); ?>
	
			<div class="button"><a href="/grant-application"><?php the_field ('application_button_text'); ?></a></div>
		</div>
	</div>
	

</div>

	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer() ?>
