module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    fileName: '<%= grunt.config.get("pkg").name.toLowerCase().replace(/ /g, "-") %>',
    jsFiles: [
      'js/underscore-min.js',
      'js/backbone-min.js',
      'js/member-portal/app.js',
      'js/member-portal/routers/*.js',
      'js/member-portal/models/*.js',
      'js/member-portal/views/*.js'      
    ],

    distDir: 'assets/dist/',

    /* CSS
    cssFiles: [
      'assets/css/*.css'
    ], */

    lessFiles: ['css/member-portal.less'],
    less: {
      uncompressed: {
        options: {
          compress: false
        },
        files: {
          'css/member-portal.css': '<%= lessFiles %>'
        }
      }
    },

    concat: {
      options: {
        banner: '/*! <%= fileName %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        stripBanners: true
      },
      js: {
        src: '<%= jsFiles %>',
        dest: '<%= distDir %><%= fileName %>.all.js'
      },
      
      /* CSS
      css: {
        src: '<%= cssFiles %>',
        dest: '<%= distDir %><%= fileName %>.all.css'
      } */

    },
    uglify: {
      options: {
        banner: '/*! <%= fileName %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: '<%= distDir %><%= fileName  %>.all.js',
        dest: '<%= distDir %><%= fileName %>.all.min.js'
      }
    },
    watch: {
      js: {
        files: '<%= jsFiles %>',
        tasks: ['concat:js']
      },

      less: {
        files: '<%= lessFiles %>',
        tasks: ['less']
      },

      /* CSS
      css: {
		    files: '<%= cssFiles %>',
		    tasks: ['concat:css']
      } */
    }
  });
  
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  return grunt.registerTask('default', ['concat', 'less']);
};
