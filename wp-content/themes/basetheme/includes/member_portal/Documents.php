<?php

class MemberPortal_Documents {
	
	public static function register_ajax_functions () {

		// mp_approve_document
		add_action('wp_ajax_mp_approve_document', array(get_called_class(), 'mp_approve_document'));
		add_action('wp_ajax_nopriv_mp_approve_document', array(get_called_class(), 'mp_approve_document'));

		// mp_reject_document
		add_action('wp_ajax_mp_reject_document', array(get_called_class(), 'mp_reject_document'));
		add_action('wp_ajax_nopriv_mp_reject_document', array(get_called_class(), 'mp_reject_document'));

		// mp_get_document
		add_action('wp_ajax_mp_get_document', array(get_called_class(), 'mp_get_document'));
		add_action('wp_ajax_nopriv_mp_get_document', array(get_called_class(), 'mp_get_document'));

		// mp_get_document_list
		add_action('wp_ajax_mp_get_document_list', array(get_called_class(), 'mp_get_document_list_ajax'));
		add_action('wp_ajax_nopriv_mp_get_document_list', array(get_called_class(), 'mp_get_document_list_ajax'));

		// mp_pending_document
		add_action('wp_ajax_mp_pending_document', array(get_called_class(), 'mp_pending_document'));
		add_action('wp_ajax_nopriv_mp_pending_document', array(get_called_class(), 'mp_pending_document'));

		// mp_destroy_document
		add_action('wp_ajax_mp_destroy_document', array(get_called_class(), 'mp_destroy_document'));
		add_action('wp_ajax_nopriv_mp_destroy_document', array(get_called_class(), 'mp_destroy_document'));
	}

	// called by admin_ajax
	public static function mp_approve_document () {
		if (MemberPortal::mp_get_user_role() !== 'administrator') return false; //only admins can do this.
		
		$response = array();

		$document_id = $_POST['document_id'];
		if (get_post_status($document_id) == 'pending' && current_user_can('edit_post', $document_id)) {
			wp_publish_post($document_id);
			$response['success'] = true;
			$response['message'] = 'Document successfully published.';
		} else {
			$response['success'] = false;
			$response['message'] = 'Document publish error. ID: ' . $document_id . ', status: ' . get_post_status($document_id);
		}
		$response['document'] = self::_mp_get_document_fields($document_id);
		echo json_encode($response);
		die();
	}

	// called by admin_ajax
	public static function mp_reject_document () {
		if (MemberPortal::mp_get_user_role() !== 'administrator') return false; //only admins can do this.
		
		$response = array();

		$document_id = $_POST['document_id'];
		$trashed = wp_trash_post($document_id);

		if ($trashed) {
			$response['success'] = true;
			$response['message'] = 'Document successfully rejected.';
		} else {
			$response['success'] = false;
			$response['message'] = 'Document reject error. ID: ' . $document_id . ', status: ' . get_post_status($document_id);
		}
		$response['document'] = self::_mp_get_document_fields($document_id);
		echo json_encode($response);
		die();
	}

	// called by admin_ajax. 
	// Move document status to pending
	public static function mp_pending_document () {
		// TODO ALLOW ADMINS AND POST AUTHORS TO DO THIS
		$response = array();
		if (current_user_can('edit_post', $_POST['document_id'])) {
			$document_id = $_POST['document_id'];
			$document_args = array(
				'ID' => $document_id,
				'post_status' => 'pending',
			);
			$success = wp_update_post($document_args);
			if ($success) {
				$response['success'] = true;
				$response['message'] = 'Document successfully pending.';
				$response['document'] = self::_mp_get_document_fields($document_id);
			} else {
				$response['success'] = false;
				$response['message'] = 'Document pending error. ID: ' . $document_id . ', status: ' . get_post_status($document_id);
			}
		} else {
			$response['success'] = false;
			$response['message'] = 'user did not have permission to edit post with id: ' . $document_id;
		}
		
		echo json_encode($response);
		die();
	}

	// called by admin_ajax. 
	// Destroy a document forever
	public static function mp_destroy_document () {
		// TODO ALLOW ADMINS AND POST AUTHORS TO DO THIS
		$response = array();
		if (current_user_can('delete_post', $_POST['document_id'])) {
			$document_id = $_POST['document_id'];
			$success = wp_delete_post($document_id, true);
			if ($success) {
				$response['success'] = true;
				$response['message'] = 'Document successfully rejected.';
				$response['document'] = self::_mp_get_document_fields($document_id);
			} else {
				$response['success'] = false;
				$response['message'] = 'Document destroy error. ID: ' . $document_id . ', status: ' . get_post_status($document_id);
			}
		} else {
			$response['success'] = false;
			$response['message'] = 'user did not have permission to edit post with id: ' . $document_id;
		}
		
		echo json_encode($response);
		die();
	}

	// called by admin_ajax
	public static function mp_get_document() {
		$result = array();
		$document_id = $_POST['document_id'];
		if ($document_id) {
			$result['document'] = self::_mp_get_document_fields($document_id);
			$result['success'] = true;
		} else {
			$result['success'] = false;
			$result['message'] = 'document ID did not evaluate to true, doc id: ' . $document_id;
		}
		echo json_encode($result);
		die();
	}

	public static function mp_get_document_list_ajax() {
		$result['documents'] = self::mp_get_document_list();
		$result['success'] = true;
		echo json_encode($result);
		die();
	}

	public static function mp_get_document_list() {
		$result = array();
		if (MemberPortal::mp_get_user_role() == 'administrator') {
			$document_args = array( 'post_type' => 'document',
				'posts_per_page' => -1, 
				'post_status' => array('pending', 'publish', 'trash') 
			);
			$documents = new WP_Query($document_args);
			while ($documents->have_posts()) { $documents->the_post();
				$result[] = self::_mp_get_document_fields();
			}
		} else if (MemberPortal::mp_get_user_role() == 'member') {
			// need to check the users ID and only get documents that they are allowed to see.
			$document_args = array( 
				'post_type' => 'document', 
				'posts_per_page' => -1, 
				'post_status' => array('pending', 'publish', 'trash'),
				'author' => get_current_user_id()
			);
			$documents = new WP_Query($document_args);
			while ($documents->have_posts()) { $documents->the_post();
				$result[] = self::_mp_get_document_fields();
			}
			$document_args = array( 
				'post_type' => 'document', 
				'posts_per_page' => -1, 
				'post_status' => array('publish'),
				'meta_key' => 'for_partners',
				'meta_value' => true
			);
			$documents = new WP_Query($document_args);
			while ($documents->have_posts()) { $documents->the_post();
				$result[] = self::_mp_get_document_fields();
			}
		}
		return $result;
	}

	public static function mp_get_document_types() {
		// this should probably be done recursively. limiting the number of subcategories right now
		$document_types = get_categories(
			array('parent' => 6, 'hide_empty' => 0)
		);
		foreach ($document_types as $section) {
			$section->tabs = get_categories(
				array('parent' => $section->cat_ID, 'hide_empty' => 0)
			);
			foreach ($section->tabs as $tab) {
				$tab->folders = get_categories(
					array('parent' => $tab->cat_ID, 'hide_empty' => 0)
				);
			}
		}
		return $document_types;
	}

	// pass the document id otherwise it assumes you are in the loop
	private static function _mp_get_document_fields($document_id = null) {
		if (is_null($document_id)) {
			$document_id = get_the_ID();
		}
		if ($document_id) {
			$document = get_post($document_id);
			$document->id = $document->ID;
			$document->comments = get_field('comments', $document_id);
			$document->document = get_field('document', $document_id);
			$document->document_type = get_field('type', $document_id);
			$document->document_language = get_field('language', $document_id);
			$document->audience = get_field('for_partners', $document_id);
			$document->document_mime_type = get_post_mime_type( $document->document['id'] );
			$document->category_id = get_the_category($document_id)[0]->cat_ID;
			$document->category_slug = get_the_category($document_id)[0]->slug;
			$document->category_name = get_the_category($document_id)[0]->name;
			$hospital_partner = get_field('hospital_partner', $document_id);
			$document->hospital_partner = $hospital_partner; // not  used right now
			$document->hospital_title = $hospital_partner->post_title;
			$document->hospital_name = $hospital_partner->post_name;
			return $document;
		} else {
			return false;
		}
	}

}

?>