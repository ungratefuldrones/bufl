<?php 

require_once('Documents.php');

class MemberPortal {
	
	private static $documents;
	private static $messages;
	private static $document_types;
	private static $userinfo;
	private static $hospital_partners;

	public static function permission() {
		if (!is_user_logged_in()) {
			header("Location: http://" . $_SERVER['SERVER_NAME'] . '/wp-login.php?redirect_to=' . $_SERVER['REQUEST_URI']);
		}
	}

	public static function load_data() {
		
		MemberPortal_Documents::register_ajax_functions();

		// get messages

		// get user data
		self::$userinfo = wp_get_current_user();

		// get hospital stuff
		$args = array( 'post_type' => 'hospitalpartners',
			'posts_per_page' => -1, 
			'post_status' => array('pending', 'publish', 'trash') 
		);
		$hospital_partners = new WP_Query($args);
		while ($hospital_partners->have_posts()) { 
			$hospital_partners->the_post();
			$result = get_post();
			$result->administrator = get_field('administrator');
			$acf_user = 'user_' . $result->administrator['ID'];
			$result->contact_information = get_field('contact_information', $acf_user);
			if ($result->administrator != false) {
				self::$hospital_partners[] = $result;
			}			
		}

		// get documents
		self::$documents = MemberPortal_Documents::mp_get_document_list();
		self::$document_types = MemberPortal_Documents::mp_get_document_types();

	}

	public static function print_data($json = true) {
		$output = array();
		if ($json) {
			$output['hospital_partners'] = self::$hospital_partners;
			$output['documents'] = self::$documents;
			$output['document_types'] = self::$document_types;
			$output['userinfo'] = self::$userinfo;
			echo json_encode($output);
		}
	}

	// member portal form handler
	public static function mp_form_handler () {

		// mp_form_type is a hidden field for all forms in the member portal that need to be saved somewhere in wordpress
		if (isset($_POST['mp_form_type'])) {
			
			$user_id = get_current_user_id();
			$acf_user = 'user_' . $user_id;

			// functionality inside here should be broken into other classes/functions
			// also meant to handle messages
			switch ($_POST['mp_form_type']) :
				
				case 'upload' :

					$custom_field_keys = array(
						'language' => 'field_54afdeb130ce2',
						'document' => 'field_54afdecb30ce3',
						'comments' => 'field_54b0124683f4e',
						'type' => 'field_54afde9630ce1',
						'for_partners' => 'field_54eced6f181d5',
						'hospital_partner' => 'field_54b54796e3096'
					);

					$post_args = array(
						'post_title' => $_POST['doc_title'],
						'post_type' => 'document',
						'post_status' => 'pending',
						'post_author' => $user_id
					);

					if ($_POST['document_id']) { // edited an existing document
						$post_args['ID'] = $_POST['document_id'];
					}

					// base fields for all docs
					$custom_field_args = array(
						$custom_field_keys['language'] => $_POST['doc_language'],
						$custom_field_keys['comments'] => $_POST['doc_comments'],
						$custom_field_keys['type'] => $_POST['doc_type'],
					);

					// process the uploaded file
					$attach_id = process_uploaded_file('doc_file');
					if ($attach_id) {
						$custom_field_args[$custom_field_keys['document']] = $attach_id;
					}

					switch (self::mp_get_user_role()) :
						case 'member' :
							// not sure if this makes sense here
							$custom_field_args[$custom_field_keys['hospital_partner']] = get_field('hospital_partner', $acf_user)->ID;
							if (isset($_POST['document_id'])) {
								if (current_user_can('edit_post', $_POST['document_id'])) {
									$post_args['post_status'] = 'pending';
								} else {
									return false;
								}
							} else {
								if (!current_user_can('edit_documents')) {
									echo "<h1>debug:</h1>";
									echo "<pre>";
									var_dump(current_user_can('edit_documents'));
									echo "</pre>";
									echo "<pre>";
									var_dump(wp_get_current_user());
									echo "</pre>";
									die();
								}
							}
							break;
						case 'administrator' : 
							$custom_field_args[$custom_field_keys['for_partners']] = (bool)$_POST['for_partners'];
							$post_args['post_status'] = 'pending';
							break;
						default:
							echo "<h1>Your role: <pre>"; var_dump(self::mp_get_user_role()); echo "</pre></h1>";
							echo "<h1>Something went wrong. <a href='/'>Buckle Up for Life</a></h1>";
							die();
							break;
					endswitch;

					$post_id = wp_insert_post($post_args);

					foreach ($custom_field_args as $key => $value) {
						update_field($key, $value, $post_id);
					}
					$success = wp_set_post_categories($post_id, array($_POST['doc_id']));

					// echo "<h1>User: $user_id</h1>";
					// echo "<pre>";
					// var_dump($success);
					// echo "</pre>";
					// die();

					// set session variable for confirmation message
					$_SESSION['message_type'] = 1;

					break;
				
				case 'message' :

					// set session variable for confirmation message
					$_SESSION['message_type'] = 2;

					break;

				default:
					
					break;

			endswitch;

			// avoid reposting with redirect
			// http://stackoverflow.com/questions/4142809/simple-post-redirect-get-code-example
			header("Location: " . $_SERVER['REQUEST_URI']);
			exit();

		}
	}

	public static function mp_get_user_role() {
		global $current_user;

		$user_roles = $current_user->roles;
		$user_role = array_shift($user_roles);

		return $user_role;
	}
	
}

?>