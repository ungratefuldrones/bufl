<!doctype html>

<html <?php language_attributes(); ?>>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:image" content="http://buckleupforlife.org/wp-content/uploads/2013/11/bufl-square.jpg" />

<title><?php bloginfo('name'); ?>  <?php wp_title(); ?></title>
<?php if (is_page_template('page-pledge.php')) { 
	// line for meta description removed per 10166
	?>
<?php } else { ?>
	<meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true);
    } else {
        bloginfo('name'); echo " - "; bloginfo('description');
    }
    ?>" />
<?php } ?>

<?php embed_js() // adds true/false embed variable for javascript to reference ?>

<?php global $post;
if( is_single() || is_page()) :
	$tags = get_the_tags($post->ID);
	if($tags) :
		foreach($tags as $tag) :
			$sep = (empty($keywords)) ? '' : ', ';
			$keywords .= $sep . $tag->name;
		endforeach;
?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php
	endif;
endif;
?>

<meta name="viewport" content="initial-scale=1,maximum-scale=1">

<script type="text/javascript" src="//use.typekit.net/jrb8gkh.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7324852/730822/css/fonts.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico?refresh=<?php echo md5(rand(1000,9000)); ?>" />
<?php

	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	wp_head();
?>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43751336-1', 'buckleupforlife.org');
  ga('send', 'pageview');
</script>

<!--jquery-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<!--TweenMax-->
<script src="<?php bloginfo('template_directory'); ?>/js/TweenMax.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/TimelineMax.min.js"></script>

<script src="<?php bloginfo('template_directory'); ?>/audiojs/audio.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.cycle2.min.js"></script>


<?php if (pll_current_language() == 'en') {
	$globalHeader = 13; //page ID for the english global header page
} else {
	$globalHeader = 18; //page ID for the spanish global header page
} ?>


<!-- Fancybox -->
<link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php bloginfo( 'template_directory' ); ?>/js/fancybox/jquery.fancybox.pack.js"></script>
<script>
	if (window.outerWidth < 481) {
		//do nothing
	} else {
		$(".fancypdf").fancybox({
			type: 'iframe',
			width: '50%',
			height: '50%',
		});
	}
	$("#embed").fancybox({
		padding: 0,
	});
</script>

<!-- jquery rotate -->
<script type="text/javascript" src="<?php bloginfo( 'template_directory' ); ?>/js/jQueryRotateCompressed.js"></script>

 <!--Custom Javascript-->
<script src="<?php bloginfo('template_directory'); ?>/js/scripts.js"></script>

<script type="text/javascript">

	if (window.outerWidth < 481 || embed_tool) {
		var viewPortTag=document.createElement('meta');
		viewPortTag.id="viewport";
		viewPortTag.name = "viewport";
		viewPortTag.content = "user-scalable=0,width=320;";
		document.getElementsByTagName('head')[0].appendChild(viewPortTag);
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = '<?php bloginfo('template_directory'); ?>/js/siteMobileScripts.js';
		head.appendChild(script);
	} else {
		var viewPortTag=document.createElement('meta');
		viewPortTag.id="viewport";
		viewPortTag.name = "viewport";
		viewPortTag.content = "width=1000";
		document.getElementsByTagName('head')[0].appendChild(viewPortTag);
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = '<?php bloginfo('template_directory'); ?>/js/siteScripts.js';
		head.appendChild(script);
	}
</script>


</head>

<?php 
if (pll_current_language() == 'en') $body_class [] = 'english';
else $body_class [] = 'spanish';
?>
<body <?php body_class($body_class); ?>>

<div id="menu">
	<div id="menuTop" class="clearfix">
    	<div id="menuClose"><div id="menuX"><p>&nbsp;</p></div></div>
    </div>
    <div id="menuBottom" class="clearfix">
    	<div id="menuNavigation">
            <?php if (pll_current_language() == 'en') {
				wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu' => 'english-menu' ) );
			} else {
				wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu' => 'spanish-menu' ) );
			} ?>
        </div>
        <div id="menuRight">
            <div id="menuSignup">
                <h4><?php the_field('signup_title', $globalHeader); ?></h4>
                <p><?php the_field('signup_text', $globalHeader); ?></p>
                <?php if (pll_current_language() == 'en') {
                    echo do_shortcode( '[contact-form-7 id="38" title="Newsletter Form"]' );
                    } else {
                        echo do_shortcode( '[contact-form-7 id="40" title="Spanish Newsletter Form"]' );
                    } ?>
            </div>
            <?php 
            if (!is_user_logged_in()):
            ?>
            <div class="member-login-wrapper">
            	<a class="trigger-overlay member-login-button" href="#overlay-login">Member Login</a>
            </div>
            <?php else: 
            $user = wp_get_current_user(); ?>
            <div class="member-login-wrapper">
            	<?php if ('member' == $user->caps): ?>
            	<a class="member-login-button" href="/member-portal-partner-home">Go to Member Portal</a>
            	<?php else: ?>
            	<a class="member-login-button" href="/member-portal-home">Go to Member Portal</a>
            	<?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div id="headerWrapper" <?php if ( embed() ) { echo 'class="embedded-tool"'; } ?>>
	<div id="header" class="minWidth">

    	<div id="headerLogo">
            <a href="<?php echo (pll_current_language() == 'en' ? "/" : "/es") ?>"><img src="<?php the_field('main_logo', $globalHeader); ?>"></a>
        </div>

        <a id="toyota-overlay" target="_blank" href="http://www.toyota.com"></a>
        <a id="cc-overlay" target="_blank" href="http://www.cincinnatichildrens.org"></a>

        <div id="headerLogoMobile">
            <a href="<?php echo (pll_current_language() == 'en' ? "/" : "/es/" ) ?>" <?php if (embed()) echo " target=_blank"; ?> ><img src="<?php the_field('main_logo_mobile', $globalHeader); ?>"></a>
        </div>

	<?php if(! embed() ){ ?>
        <div id="menuButton">
   	   		<img src="<?php bloginfo('template_directory'); ?>/images/menu_button.gif">
        </div>
	<?php } ?>

        <div id="utilityLinks">
        	<img src="<?php the_field('language_image_url', $globalHeader); ?>" />

			<a href="<?php echo the_field('language_link_url', $globalHeader).get_embed_url(); ?>">
				<?php the_field('language_link', $globalHeader); ?>
			</a>

		<?php if ( ! embed() ) : ?>
			| <a href="<?php echo the_field('about_link_url', $globalHeader); ?>">
				<?php the_field('about_link', $globalHeader); ?>
			</a>
			<?php if (!is_user_logged_in()): ?>
			| <a href="#overlay-login" class="trigger-overlay red">Member Login</a>
			<?php else: ?>
			| <a href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
			<?php endif; ?>
		<?php endif ?>

        </div>

    </div>
</div>
