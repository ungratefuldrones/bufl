<?php get_header('mp-member'); ?>
    <div class="modules-wrapper my-account">
      <div class="module">
              <div class="module-description">
        <h1><span class="icon-bg icon-home"></span>My Account</h1>
        </div>
                    <form class="my-account">
                            <div class="module-header">
          <div class="subtitle-wrap clearfix">
            <h3 class="subtitle">Account Details</h3>
          </div>
        </div>
                <fieldset>

    <ul>
      <li>
        <label for="username">Username</label>
        <input id="username" name="username" type="text" placeholder="Your_Username23" disabled>
      </li>
      <li>
        <label for="email">Email</label>
        <input id="email" name="email" type="email" placeholder="example@gmail.com">
      </li>
      <li>
        <label for="currentpassword">Current Password</label>
        <input id="currentpassword" name="currentpassword" type="password" placeholder="">
        <a href="#overlay-forgot-password" class="trigger-overlay forgotpassword">Forgot your password?</a>
      </li>
     <li>
        <label for="currentpassword"> </label>
        <input id="newpassword" name="newpassword" type="password" placeholder="New Password">
      </li>
     <li>
        <label for="currentpassword"> </label>
        <input id="confirmpassword" name="confirmpassword" type="password" placeholder="Re-Type New Password">
      </li>
    </ul>
  </fieldset>
                              <div class="module-header">
          <div class="subtitle-wrap clearfix">
            <h3 class="subtitle">Profile Settings</h3>
          </div>
        </div>



                <fieldset>

    <ul>
      <li>
        <label for="hospital">Hospital</label>
         <select name="hospital">
            <option value="#">Cincinnati Children's Medical Hospital Center</option>
            <option value="#">Cincinnati Children's Medical Hospital Center</option>
          </select>
      </li>
      <li>
        <label for="firstname">Name</label>
        <input id="firstname" name="firstname" type="text" placeholder="First">
        <input id="lastname" name="lastname" type="text" placeholder="Last">
      </li>
      <li>
        <label for="address1">Address</label>
        <input id="address1" name="address1" type="text" placeholder="Address Line 1">
      </li>
            <li>
        <label for="address2"> </label>
        <input id="address2" name="address2" type="text" placeholder="Address Line 2">
      </li>
      <li>
        <label for="workphone">Phone Number</label>
        <input id="workphone" name="workphone" type="tel" placeholder="Work Phone">
        <input id="cellphone" name="cellphone" type="tel" placeholder="Cell Phone">
      </li>
      <li class="photo-label-wrapper">
        <label class="profile-photo-label">Profile Photo</label>
      </li>
            <li class="photo-label">
        <label> </label>
        <div class="profile-image"><img src="/wp-content/themes/basetheme/images/headshot_square-FPO.png" /></div>
      </li>
                  <li>
        <label> </label>
        <div class="form-btn upload-btn">Choose File</div>
        <a href="#" class="profile-image-link">Profile-Photo-2014.jpg</a>        
      </li>


    </ul>
  </fieldset>

  <ul class="form-buttons clearfix">
    <li><div class="form-btn save-btn">Save Changes</div></li>
    <li><div class="form-btn cancel-btn">Cancel</div></li>
  </ul>



            </form>
      </div>
    </div>
  </div>
</div>
</div>
<?php get_footer ('mp'); ?>