<?php 

ini_set('html_errors', 0);
define('SHORTINIT', true);

require '../../../../wp-load.php';
require( ABSPATH . WPINC . '/formatting.php' );
require( ABSPATH . WPINC . '/meta.php' );
require( ABSPATH . WPINC . '/post.php' );
require('../../../themes/basetheme/functions.php');
wp_plugin_directory_constants();



$language = $_POST['language'];
$person_id = $_POST['language'];
$date = $_POST['date'];
$pre_post_class = $_POST['pre_post_class'];
$ethnicity = $_POST['ethnicity'];
$age_group = $_POST['age_group'];
$q1 = $_POST['q1'];
$q2 = $_POST['q2'];
$q3 = $_POST['q3'];
$q4 = $_POST['q4'];
$q5 = $_POST['q5'];
$q6 = $_POST['q6'];
$q7 = $_POST['q7'];
$q8 = $_POST['q8'];
$q9 = $_POST['q9'];
$q10 = $_POST['q10'];
$user_id = $_POST['user_id'];

// IDs below must be changed for LIVE!!!
// TZ dev
if (strpos ($_SERVER ['HTTP_HOST'], 'bufl.tzvety.hfwebdev.com') !== false) {
	$form_id = 12;
	$language_form_field_id = 147;
	$person_id_form_field_id = 148;
	$date_form_field_id = 149;
	$pre_post_class_form_field_id = 150;
	$ethnicity_form_field_id = 151;
	$age_group_form_field_id = 152;
	$q1_form_field_id = 153;
	$q2_form_field_id = 154;
	$q3_form_field_id = 155;
	$q4_form_field_id = 156;
	$q5_form_field_id = 157;
	$q6_form_field_id = 158;
	$q7_form_field_id = 159;
	$q8_form_field_id = 160;
	$q9_form_field_id = 161;
	$q10_form_field_id = 162;
}

// Staging
if (strpos ($_SERVER ['HTTP_HOST'],'bufl.hfwebdev.com') !== false) {
	$form_id = 7;
	$language_form_field_id = 61;
	$person_id_form_field_id = 62;
	$date_form_field_id = 63;
	$pre_post_class_form_field_id = 64;
	$ethnicity_form_field_id = 65;
	$age_group_form_field_id = 66;
	$q1_form_field_id = 67;
	$q2_form_field_id = 68;
	$q3_form_field_id = 69;
	$q4_form_field_id = 70;
	$q5_form_field_id = 71;
	$q6_form_field_id = 72;
	$q7_form_field_id = 73;
	$q8_form_field_id = 74;
	$q9_form_field_id = 75;
	$q10_form_field_id = 76;
}

$values = array(
				$language_form_field_id => $language, //change 25 to your field ID and 'value' to your value
				$person_id_form_field_id => $person_id,
				$date_form_field_id => $date,
				$pre_post_class_form_field_id => $pre_post_class,
				$ethnicity_form_field_id => $ethnicity,
				$age_group_form_field_id => $age_group,
				$q1_form_field_id => $q1,
				$q2_form_field_id => $q2,
				$q3_form_field_id => $q3,
				$q4_form_field_id => $q4,
				$q5_form_field_id => $q5,
				$q6_form_field_id => $q6,
				$q7_form_field_id => $q7,
				$q8_form_field_id => $q8,
				$q9_form_field_id => $q9,
				$q10_form_field_id => $q10				
		);
// enter the info
global $frm_entry;
$frm_entry->create(array(
		'form_id' => $form_id, //change 5 to your form id
		'item_key' => 'entry', //change entry to a dynamic value if you would like
		'frm_user_id' => $user_id,
		'item_meta' => $values
));

insert_entries ($form_id, $values);



// function insert_entries ($form_id, $values) {
// 	global $frm_entry;
// 	var_dump($frm_entry);
// 	$frm_entry->create(array(
// 			'form_id' => $form_id, //change 5 to your form id
// 			'item_key' => 'entry', //change entry to a dynamic value if you would like
// 			'frm_user_id' => $user_id,
// 			'item_meta' => $values
// 	));
// }

die(var_dump($frm_entry));
?>