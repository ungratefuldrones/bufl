<?php get_header('mp'); ?>
<?php
$user = wp_get_current_user();
$user_ID = $user->ID; 

if (strpos ($_SERVER ['HTTP_HOST'], 'bufl.tzvety.hfwebdev.com') !== false) {
		$form_id = 12;
}
if (strpos ($_SERVER ['HTTP_HOST'],'bufl.hfwebdev.com') !== false) {
	$form_id = 7;
}

?>
<div class="survey-title">
  <div class="container-alt">
    <h1>Survey Form</h1>
    <a href="/member-portal-partner-home" class="blue-survey-btn">Go Back to Member Portal</a>
  </div>
</div>

<div id="survey">
  <div class="container">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eleifend volutpat ultrices. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer congue varius nisl, sit amet gravida dolor suscipit vel. Phasellus in tempus eros. Proin ex sem cursus sit amet dapibus vel, mattis quis ex. Fusce sagittis sagittis consectetur. Curabitur sed congue purus. Vestibulum consequat dolor sit amet magna mattis egestas.</p>
    <div class="wrap">
      <form data-user-id="<?php echo $user_ID; ?>">

        <div class="pre pre1 active">
          <div class="title">Select Language</div>
          <div class="center-buttons">
            <a href="" data-info="english" class="blue-survey-btn">English</a>
            <a href="" data-info="espanol" class="blue-survey-btn">Espa&ntilde;ol</a>
          </div>
        </div>

        <div class="pre pre2">
          <fieldset>
            <div>
              <div class="left">
                <span>ID#</span>
                <input id="id-number" type="text" name="id" placeholder="#">
              </div>
              <div class="right">
                <span>Date</span>
                <input id="date" type="text" name="date" placeholder="MM/DD/YYYY">
              </div>
            </div>
            <div class="radio">
              <input id="class1" type="radio" name="class-type" value="pre-class"><span>Pre-Class</span>
              <input id="class2" class="last" type="radio" name="class-type" value="post-class"><span>Post-Class</span>
            </div>
            <a href="" class="purple-survey-btn active">Continue</a>
            <div id="errors">
              <div class="id"><p>Please enter an id#.</p></div>
              <div class="date"><p>Please enter a valid date: MM/DD/YYYY.</p></div>
              <div class="class"><p>Please choose a class.</p></div>
            </div>
          </fieldset>
        </div>

        <div class="pre pre3">
          <div class="title">Choose Ethnicity</div>
          <div class="center-buttons">
            <a href="" data-info="hispanic" class="blue-survey-btn">Hispanic</a>
            <a href="" data-info="african-american" class="blue-survey-btn">African American</a>
            <a href="" data-info="other" class="blue-survey-btn">Other</a>
          </div>
        </div>

        <div class="pre pre4">
          <a href="" data-info="adult" class="blue-survey-btn"><img src="<?php bloginfo('template_directory'); ?>/images/mp-survey-icon-adult.png">Adult Survey</a>
          <a href="" data-info="teen" class="blue-survey-btn"><img src="<?php bloginfo('template_directory'); ?>/images/mp-survey-icon-teen.png">Teen Survey</a>
        </div>

        <div class="questions english-adult">
          <div class="top">

            <fieldset class="active">
              <h2><span class="number">1</span>Question number 1 english adult survey?</h2>
              <div class="item">
                <input type="radio" name="q1" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">2</span>Question number 2?</h2>
              <div class="item">
                <input type="radio" name="q2" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">3</span>Where is the safest location in the vehicle for a child?</h2>
              <div class="item">
                <input type="radio" name="q3" value="Option 1"><span>Any place</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 2"><span>In the back seat</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 3"><span>In the front seat as long as he/she is buckled up</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">4</span>Question number 4?</h2>
              <div class="item">
                <input type="radio" name="q4" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">5</span>Question number 5?</h2>
              <div class="item">
                <input type="radio" name="q5" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">6</span>Question number 6?</h2>
              <div class="item">
                <input type="radio" name="q6" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">7</span>Question number 7?</h2>
              <div class="item">
                <input type="radio" name="q7" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">8</span>Question number 8?</h2>
              <div class="item">
                <input type="radio" name="q8" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">9</span>Question number 9?</h2>
              <div class="item">
                <input type="radio" name="q9" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">10</span>Question number 10?</h2>
              <div class="item">
                <input type="radio" name="q10" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

          </div>

          <div class="bar-links">
            <div class="links">
              <a href="" class="back purple-survey-btn">Back</a>
              <a href="" class="next purple-survey-btn">Next Question</a>
            </div>
            <div class="bar">
              <div class="bar-animation">
                <div class="blue"></div>
              </div>
              <p><span>0</span> of 10 questions answered</p>
            </div>
          </div>

        </div>

        <div class="questions espanol-adult">
          <div class="top">

            <fieldset class="active">
              <h2><span class="number">1</span>Question number 1 espanol adult survey?</h2>
              <div class="item">
                <input type="radio" name="q1" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">2</span>Question number 2?</h2>
              <div class="item">
                <input type="radio" name="q2" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">3</span>Where is the safest location in the vehicle for a child?</h2>
              <div class="item">
                <input type="radio" name="q3" value="Option 1"><span>Any place</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 2"><span>In the back seat</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 3"><span>In the front seat as long as he/she is buckled up</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">4</span>Question number 4?</h2>
              <div class="item">
                <input type="radio" name="q4" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">5</span>Question number 5?</h2>
              <div class="item">
                <input type="radio" name="q5" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">6</span>Question number 6?</h2>
              <div class="item">
                <input type="radio" name="q6" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">7</span>Question number 7?</h2>
              <div class="item">
                <input type="radio" name="q7" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">8</span>Question number 8?</h2>
              <div class="item">
                <input type="radio" name="q8" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">9</span>Question number 9?</h2>
              <div class="item">
                <input type="radio" name="q9" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">10</span>Question number 10?</h2>
              <div class="item">
                <input type="radio" name="q10" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

          </div>

          <div class="bar-links">
            <div class="links">
              <a href="" class="back purple-survey-btn">Back</a>
              <a href="" class="next purple-survey-btn">Next Question</a>
            </div>
            <div class="bar">
              <div class="bar-animation">
                <div class="blue"></div>
              </div>
              <p><span>0</span> of 10 questions answered</p>
            </div>
          </div>

        </div>

        <div class="questions english-teen">
          <div class="top">

            <fieldset class="active">
              <h2><span class="number">1</span>Question number 1 english teen survey?</h2>
              <div class="item">
                <input type="radio" name="q1" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">2</span>Question number 2?</h2>
              <div class="item">
                <input type="radio" name="q2" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">3</span>Where is the safest location in the vehicle for a child?</h2>
              <div class="item">
                <input type="radio" name="q3" value="Option 1"><span>Any place</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 2"><span>In the back seat</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 3"><span>In the front seat as long as he/she is buckled up</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">4</span>Question number 4?</h2>
              <div class="item">
                <input type="radio" name="q4" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">5</span>Question number 5?</h2>
              <div class="item">
                <input type="radio" name="q5" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">6</span>Question number 6?</h2>
              <div class="item">
                <input type="radio" name="q6" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">7</span>Question number 7?</h2>
              <div class="item">
                <input type="radio" name="q7" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">8</span>Question number 8?</h2>
              <div class="item">
                <input type="radio" name="q8" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">9</span>Question number 9?</h2>
              <div class="item">
                <input type="radio" name="q9" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">10</span>Question number 10?</h2>
              <div class="item">
                <input type="radio" name="q10" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

          </div>

          <div class="bar-links">
            <div class="links">
              <a href="" class="back purple-survey-btn">Back</a>
              <a href="" class="next purple-survey-btn">Next Question</a>
            </div>
            <div class="bar">
              <div class="bar-animation">
                <div class="blue"></div>
              </div>
              <p><span>0</span> of 10 questions answered</p>
            </div>
          </div>

        </div>

        <div class="questions espanol-teen">
          <div class="top">

            <fieldset class="active">
              <h2><span class="number">1</span>Question number 1 espanol teen survey?</h2>
              <div class="item">
                <input type="radio" name="q1" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q1" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">2</span>Question number 2?</h2>
              <div class="item">
                <input type="radio" name="q2" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q2" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">3</span>Where is the safest location in the vehicle for a child?</h2>
              <div class="item">
                <input type="radio" name="q3" value="Option 1"><span>Any place</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 2"><span>In the back seat</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 3"><span>In the front seat as long as he/she is buckled up</span>
              </div>
              <div class="item">
                <input type="radio" name="q3" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">4</span>Question number 4?</h2>
              <div class="item">
                <input type="radio" name="q4" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q4" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">5</span>Question number 5?</h2>
              <div class="item">
                <input type="radio" name="q5" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q5" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">6</span>Question number 6?</h2>
              <div class="item">
                <input type="radio" name="q6" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q6" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">7</span>Question number 7?</h2>
              <div class="item">
                <input type="radio" name="q7" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q7" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">8</span>Question number 8?</h2>
              <div class="item">
                <input type="radio" name="q8" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q8" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">9</span>Question number 9?</h2>
              <div class="item">
                <input type="radio" name="q9" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q9" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

            <fieldset>
              <h2><span class="number">10</span>Question number 10?</h2>
              <div class="item">
                <input type="radio" name="q10" value="Option 1"><span>Answer 1</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 2"><span>This is a second answer</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 3"><span>This is a third, longer answer to see</span>
              </div>
              <div class="item">
                <input type="radio" name="q10" value="Option 4"><span>None of the above</span>
              </div>
            </fieldset>

          </div>

          <div class="bar-links">
            <div class="links">
              <a href="" class="back purple-survey-btn">Back</a>
              <a href="" class="next purple-survey-btn">Next Question</a>
            </div>
            <div class="bar">
              <div class="bar-animation">
                <div class="blue"></div>
              </div>
              <p><span>0</span> of 10 questions answered</p>
            </div>
          </div>

        </div>

        <div class="final">
          <div class="title">Thank you!</div>
          <div>
            <a href="" class="blue-survey-btn reload">Take Another Survey</a>
            <a href="/member-portal-partner-home" class="blue-survey-btn">Go Back to Member Portal</a>
          </div>
        </div>

      </form>
    </div>

    <div class="counter-wrapper">Number of quizzes completed: <span class="counter"><?php echo get_frm_number_of_entries ($form_id); ?></span></div>
    
  </div>
</div>