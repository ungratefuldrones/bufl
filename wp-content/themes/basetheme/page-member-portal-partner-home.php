<?php get_header('mp-member'); ?>

    <div class="alerts-wrapper">
      <div class="alert">
        <h3><span class="icon-bg icon-megaphone"></span>Important Message:</h3><p>Please submit your quarterly update data reports no later than Friday of next week, October 31st. This will help ensure that all...</p><a href="#overlay-delete" class="trigger-overlay alert-close"></a>
      </div>
      <div class="alert">
        <h3><span class="icon-bg icon-megaphone"></span>Important Message:</h3><p>Please submit your quarterly update data reports no later than Friday of next week, October 31st. This will help ensure that all...</p><a href="#overlay-delete" class="trigger-overlay alert-close"></a>
      </div>
    </div>
    <div class="modules-wrapper portal-partner">
      <div class="module">
        <div class="module-description">
          <h1><span class="icon-bg icon-home"></span>Member Portal Home</h1>
        </div>
        <div id="tabs" class="tabs">
          <ul class="tabs-nav"><li><a href="#tab-1"><span>Uploaded By You</span></a></li><li><a href="#tab-2"><span>Recent Downloads</span></a></li></ul>
          <div id="tab-1" class="tab">
           <div class="search-for-tab">        
             <div class="module-header">
              <div class="subtitle-wrap clearfix">
                <form class="search-portal">
                  <input class="text-input" type="search" name="search-partners" value="Search">
                </form>
                <div class="pagination">
                  <span class="pagi-count">1 - 8 of 223</span><div class="pagi pagi-left"><a href="#"><span></span></a></div><div class="pagi pagi-right"><a href="#"><span></span></a></div>
                </div>
              </div>
            </div></div>
            <div class="table-div table-partner-portal-home">
              <div class="row-div heading-div">
                <div class="cell-div">Document Title</div>
                <div class="cell-div">Created/Modified</div>
                <div class="cell-div">Category</div>
                <div class="cell-div">Type</div>
                <div class="cell-div">Status</div>
                <div class="cell-div cell-actions-heading"> </div>
                <div class="cell-div cell-trash-heading"> </div>
              </div>
              <div class="row-div">
                <div class="cell-div">Lorem ipsum dolor set ameti</div>
                <div class="cell-div">10/28/2014, 2:23PM</div>
                <div class="cell-div"><a href="#">Order Form</a></div>
                <div class="cell-div">PDF</div>
                <div class="cell-div cell-status status-pending">Pending</div>
                <div class="cell-div cell-actions">
                  <ul class="dropdown">
                    <li><a href="#" class="drop-link">Actions</a>
                      <ul class="dropdown-options">
                        <li><a href="#">Download</a></li>
                        <li><a href="#">Email/Share</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
                <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
              </div>
              <div class="row-div">
                <div class="cell-div">Lorem ipsum dolor set ameti</div>
                <div class="cell-div">10/28/2014, 2:23PM</div>
                <div class="cell-div"><a href="#">Order Form</a></div>
                <div class="cell-div">PDF</div>
                <div class="cell-div cell-status status-rejected">Rejected</div>
                <div class="cell-div cell-actions">
                  <ul class="dropdown">
                    <li><a href="#" class="drop-link">Actions</a>
                      <ul class="dropdown-options">
                        <li><a href="#">Download</a></li>
                        <li><a href="#">Email/Share</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
                <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
              </div>
              <div class="row-div">
                <div class="cell-div">Lorem ipsum dolor set ameti</div>
                <div class="cell-div">10/28/2014, 2:23PM</div>
                <div class="cell-div"><a href="#">Order Form</a></div>
                <div class="cell-div">PDF</div>
                <div class="cell-div cell-status status-approved">Approved</div>
                <div class="cell-div cell-actions">
                  <ul class="dropdown">
                    <li><a href="#" class="drop-link">Actions</a>
                      <ul class="dropdown-options">
                        <li><a href="#">Download</a></li>
                        <li><a href="#">Email/Share</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
                <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
              </div>
            </div>
          </div>
          <div id="tab-2" class="tab">
          This view not shown in mockup.
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php get_footer ('mp'); ?>