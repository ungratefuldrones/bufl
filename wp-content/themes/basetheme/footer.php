
<div id="footerWrapper">

<?php if (pll_current_language() == 'en') {
	$globalFooter = 41; //page ID for the english global footer page
} else { 
	$globalFooter = 47; //page ID for the spanish global footer page
} ?>

	<?php if ( ! embed() ) : ?>
	
	<div id="footer">
    
    	<div id="footerTop" class="clearfix">
        	<div id="footerTopLeft">
            	<span class="sponsorText"><?php the_field('sponsor_text', $globalFooter); ?></span>
            	<div id="sponsorLogos">
                    <a href="<?php the_field('sponsor_logo1_link', $globalFooter); ?>" target="_blank" class="rightBorder"><img src="<?php the_field('sponsor_logo1', $globalFooter); ?>"/></a>
                    <a href="<?php the_field('sponsor_logo2_link', $globalFooter); ?>" target="_blank"><img src="<?php the_field('sponsor_logo2', $globalFooter); ?>"/></a>
                </div>
            </div>
            <div id="footerTopRight">
            	<img src="<?php the_field('site_footer_logo', $globalFooter); ?>"/>
            </div>
        </div>
        
        <div id="footerBottom" class="clearfix">
        	
            <div class="footerLeftColumn footerNav footerColumn">
            	<?php the_field('column1_content', $globalFooter); ?>
            </div>
            <div class="footerLeftColumn footerSocial footerColumn">
            	<?php the_field('column2_content', $globalFooter); ?>
            </div>
            <div class="footerLeftColumn footerAddress footerColumn">
            	<?php the_field('column3_content', $globalFooter); ?>
            </div>
            <div class="footerRightColumn footerCopyright footerColumn">
            	<?php the_field('column4_content', $globalFooter); ?>
            </div>
            
        </div>
	
    </div>
	
	<?php else: // display legal link for embedded tool.  Right now there is only an English legal page, but this supports the addition of a spanish version (mostly) ?>
	
	<div id="footer" class="embedded-tool">
		<a class="embedded-legal-link" href="<?php echo (pll_current_language() == 'en' ? 
			"/legal".get_embed_url() : 
			"/es/legal".get_embed_url() ); ?>">
			Legal
		</a> 
	</div>
	

	<?php endif; ?>
    <?php //echo FrmEntriesController::show_form(3, $key = '', $title=true, $description=true); ?>

</div>
<div class="overlays-wrapper">
<?php include ('overlay-includes/inc-login-overlay.php'); ?>
<?php include ('overlay-includes/inc-forgot-password-overlay.php'); ?>
</div>

<script src="<?php bloginfo('template_directory'); ?>/js/youtube-metrics.js"></script>
<?php wp_footer(); ?>

</body>
</html>
