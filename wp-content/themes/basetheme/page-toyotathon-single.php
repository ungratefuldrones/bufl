<?php
/*
Template Name: Toyotathon
*/
?>

<?php get_header(); ?>

<div id="pageWrapper">

	<div id="pageTitle">
		<p style="float:right; color:white; padding-top:15px; font-size:0.8em"><a href=" http://toyota.com/toyotathon" style="color:white">Toyotathon</a> | <a href="#faq" style="color:white">FAQ</a/></p>
		<h1>Help Us #BuckleUpForLife</h1>
	</div>

	<div id="pageContent" class="clearfix">
		<div id="pageImage">
			<img src="<?php the_field('page_image'); ?>" class="pageImageLarge"/>
			<img src="<?php the_field('page_image_mobile'); ?>" class="pageImageSmall"/>
		</div>
		<div style="width:600px; height:450px; margin:auto;">
			<div class="plyfe-widget" data-type="sg" data-id="266" data-treatment="madrid" data-theme="light" data-height="450"></div>
			<script async="true" data-domain="plyfe.me" src="https://d1ldbzs0x7elv3.cloudfront.net/components/plyfe-widgets-bootstrap/dist/plyfe-widgets-bootstrap-v0.min.js"></script>
		</div>
	</br>
		<div id="faq" style="padding:15px; border-top: 1px solid #272829">
		<h1 style="color:#666666; padding: 20px 20px 0 20px">FAQ</h1>
		<div style="margin: 0 20px; padding:20px">
			<ol>
				<li><b>What is this?</b></br>Buckle Up for Life, a collaboration between Toyota and Cincinnati Children’s Hospital Medical Center, is a national program that educates the entire family on critical passenger safety behaviors and provides free child car seats to families in need. Toyota is partnering with Buckle Up for Life and Cincinnati Children’s to celebrate the organization’s 10th anniversary, and we need your help. During Toyotathon, we’re asking drivers to buckle up an important person or thing in their life, take a picture, and share it on Instagram or Twitter. For every photo uploaded, Toyota will donate 10 child car seats. You raise awareness, we help families in&nbsp;need.</li>
				<li><b>How can I participate?</b></br>Take a photo showcasing something or someone you love buckled up in the seat of your car, and share it on Twitter or Instagram using the hashtag, #BuckleUpForLife.</li>
				<li><b>When does the campaign end?</b></br>You can share photos until December 31, 2014.</li>
				<li><b>Why doesn’t my photo appear on the site?</b></br>All photos go into a moderation queue and require approval before appearing on the campaign site, which may take up to 72 hours. Buckle Up for Life and Toyota reserve the right to reject any photo content deemed inappropriate or inconsistent with the spirit of the campaign. <b>To check to see if your child is properly restrained in their car seat visit <a href="/">www.buckleupforlife.org</a>.</b></li>
				<li><b>Who receives the car seats that get donated?</b></br>Buckle Up for Life and Cincinnati Children’s work in close collaboration with local hospital partners and churches in cities around the country including Cincinnati, Chicago, Houston, Las Vegas, Greenville, Philadelphia, Boston, San Antonio, New York, Phoenix, Memphis, and Los Angeles.  To date, we’ve provided funding for over 40,000 car seats.</li>
				<li><b>Where can I learn more about Buckle Up for Life?</b></br>Visit <a href="http://buckleupforlife.org/about-our-program/ ">http://buckleupforlife.org/about-our-program/ </a> for more&nbsp;information.</li>
			</ol>
		</div>
	</br>
		<div id="terms" style="padding:20px; border-top: 1px solid #272829">

			<h1 style="color:#666666; padding: 20px 0px;">Buckle Up For Life</br>Terms &amp; Conditions</h1>

			<div>

			<p><b>NO PURCHASE, PAYMENT OR DONATION NECESSARY TO PARTICIPATE. </b>The following Promotion is intended for participants in the fifty (50) United States and the District of Columbia only and shall be governed by U.S. laws. Participants must be 18 years of age or older and not a minor in participant’s primary state of residence. Do not proceed if you are not a legal U.S. resident currently located and residing in the fifty (50) United States or the District of Columbia and/or a minor. Participants are providing information to Toyota Motor Sales, U.S.A., Inc., 19001 South Western Avenue, Torrance, California, 90501 (“<b>Sponsor</b>”). The administrator of this Promotion is Vayner Media ("<b>Administrator</b>"). </p>

			<p><b>1. PROMOTION DESCRIPTION &amp; PARTICIPATION: </b>The “<b>Buckle Up For Life Promotion</b>” (the “<b>Promotion</b>”) will offer “Participants” the opportunity to “take action” by engaging with the Sponsor’s philanthropic post (the “<b>Message</b>”) on Facebook or Twitter (the <b>“Social Media Platforms”</b>). For purpose of this Promotion, “take action” is defined as directly engaging with the Message by uploading a photo responsive to the call to action posted on the Social Media Platforms (the “<b>Eligible Action</b>”). Photos uploaded to the Social Media Platforms as part of the Promotion must comply with our Submission Guidelines (below). Sponsor reserves the right to remove any photos that do not meet the Submission Guidelines. Beginning on or about 11/26/14 interested Participants will be encouraged to engage with the Message through an Eligible Action. Each Eligible Action will result in the Sponsor making a making a contribution of ten (10) car seats to Cincinnati Children’s Hospital (the “<b>Benficiary</b>”) towards Sponsor’s maximum contribution goal of 10,000 car seats (the “<b>Maximum Donation Goal</b>”). The Promotion will end when the Maximum Donation Goal is met or on 12/31/14 at 11:59 pm Pacific Time (the “<b>End Date</b>”), whichever comes first. If the Maximum Donation Goal is not met by the End Date, the contribution to the Beneficiary will be determined by the total number of Eligible Actions and their associated donation amount as of End Date. Sponsor, at its sole discretion, reserves the right to contribute the balance to achieve the Maximum Donation Goal, if applicable. </p>

			<p><b>2. Submission Guidelines and Restrictions</b>: The following Guidelines and Restrictions govern your participation in the Promotion and your submission of photographs (“<b>Photographs</b>”). We have the right to review all Photographs submitted and we may reject or remove any Photographs or other content you submit that we determine:</p>

			<p>· is unlawful, harassing, defamatory, abusive, hateful, threatening, obscene, harmful, tortious, libelous, or invasive of another's privacy;</p>

			<p>· attacks the character or damages the reputation of other users, name-calls, insults, ridicules, mocks, electronically stalks or otherwise harasses another individual;</p>

			<p>· contains material, language or an image that is profane, pornographic, sexually graphic, ethnically offensive, “off color”, political, or propaganda;</p>

			<p>· contains crude, vulgar or offensive images or nudity;</p>

			<p>· infringes or violates any party's intellectual property rights, including, but not limited to, using third-party copyrighted materials or the names or likenesses of others without appropriate permission and attribution (note that photographs with children may be submitted only by their parents or legal guardians), using third-party trademarks without appropriate permission or attribution, where needed, and using or distributing third-party information (whether or not protected as a trade secret) in violation of a duty of confidentiality;</p>

			<p>· has been broadcasted, streamed, published or televised in any form of media, including television, books and movies;</p>

			<p>· contains any advertising, promotional materials, chain letters, spam, junk mail, or any other type of unsolicited mass email to people or entities that have not agreed to be part of such mailings;</p>

			<p>· is intended to buy or solicit any goods, services, or money to advertise or sell products or services of others;</p>

			<p>· discloses or references any personally identifiable information belonging to you or a third party;</p>

			<p>· depicts or describes any activities that would violate the personal privacy rights of others, including but not limited to collecting and distributing information about others without their permission;</p>

			<p>· impersonates any person or entity; falsely states or otherwise misrepresents an affiliation with any person or entity; intentionally omits, deletes, forges, or misrepresents transmission information, including headers, return mailing, and Internet protocol addresses; or otherwise manipulates identifiers to disguise the origin of any user content transmitted to the Service;</p>

			<p>· contains any worms, viruses, or other harmful, disruptive, or destructive files, code, or Promotions;</p>

			<p>· promotes or depicts drugs or alcohol, violence, illegal or inappropriate activities or dangerous behavior that may result in harm;</p>

			<p>· We otherwise determine to be inappropriate or inconsistent with Toyota’s image and reputation.</p>

			<p><b>3. Your Representations and Warranties</b>: By submitting Photographs for posting in the Promotion, you agree that you have read and are familiar with the Guidelines and Restrictions above. By submitting Photographs for the Promotion, you also: (1) represent and warrant that you have obtained all necessary licenses, consents, waivers, releases, authorizations and/or permissions to post or transmit the Photographs; (2) represent and warrant that the Photographs do not constitute confidential information and are free of any claims of proprietary or personal rights; (3) agree that you are solely responsible for all Photographs that you submit for posting; (4) agree that such Photographs are original with you and that you own all right, title and interest in the Photographs and any elements you include in the Photographs; and (5) grant to Sponsor an irrevocable, worldwide, nonexclusive, perpetual, fully sub-licensable, transferable, royalty-free right and license to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, transmit, publicly perform, and publicly display such Photographs (in whole or part) on any website or social media page owned or operated by Sponsor or its affiliates, as well as on any social media sites associated with the Promotion, related to the Promotion, to promote the Promotion or the Charity, and in communications about the Promotion, without compensation, notification, approval or any other obligation, to anyone, including yourself. You will not have any claim against Sponsor with respect to any use or non-use of Photographs, and to the extent a claim is made against Sponsor or any of affiliated entities by any third party with respect to Photographs, you agree to indemnify and hold Sponsor and its affiliated entities harmless with respect to any such claims, including, without limitation, attorneys’ costs and expenses incurred in connection therewith.</p>

			<p><b>4. Notice and Procedure for Making Claims of Copyright Infringement</b>: Sponsor respects the intellectual property of others, and we ask all participants in the Promotion to do the same. If you are the owner of a United States copyright and you believe that your work has been copied on or used in the Promotion in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please contact and provide our Copyright Agent with the following information:</p>

			<p>· a physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest;</p>

			<p>· a description of the copyrighted work or other intellectual property that you claim has been infringed;</p>

			<p>· a description of where the material that you claim is infringing is located on the Sites;</p>

			<p>· your address, telephone number and email address;</p>

			<p>· a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent or the law; and</p>

			<p>· a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner's behalf.<br />
			<br />
			</p>

			<p>Our agent for notice of claims of copyright or other intellectual property infringement can be reached as follows:<br />
			<br />
			By mail:<br />
			Copyright Agent<br />
			c/o Toyota Customer Experience, WC11<br />
			Toyota Motor Sales, U.S.A., Inc.<br />
			19001 South Western Avenue<br />
			Torrance, California 90501<br />
			<br />
			By phone: 1-800-331-4331<br />
			By facsimile: 310-468-7814</p>

			<p><br />
			<b>5. CONDITIONS OF PARTICIPATION/RELEASES: </b>Participants shall indemnify, defend and hold harmless the Sponsor, Administrator, Beneficiary, each Social Media Platform and each of their respective parents, affiliates, divisions, distributors, dealer associations, subsidiaries, successors, assigns and licensees, and each of their respective employees, shareholders, officers, directors, contractors, advertising and promotion agencies, agents and representatives (collectively, the “<b>Released Parties</b>”) from and against any and all liability, claims, loss, damage, injury or expense, including reasonable outside attorneys’ fees, arising out of or in connection with any third-party action arising out of or resulting from Participant’s participation in the Promotion. Each Participant authorizes the Sponsor and its licensees and affiliates to use Participant’s handle or username associated with the applicable Social Media Platform, photo, name, voice, likeness, biographical data, opinion, and city and state of residence in advertising, publicity, marketing, programing, and promotional materials, worldwide in perpetuity, if applicable, without compensation unless prohibited by law. Sponsor is not obligated to use any of the above mentioned information or materials, but may do so and may edit such information or materials, at their sole discretion, without obligation or compensation. The Released Parties assume no responsibility for any damage to any person’s computer system which is occasioned by participating in the Promotion, or for any computer system, phone line, hardware, software or Promotion malfunctions, or other errors, failures, delayed computer transmissions or network connections that are human or technical in nature. Without limiting the generality of the foregoing, the Released Parties are not responsible for lost, interrupted, inaccessible or unavailable networks, servers, satellites, Internet service providers, websites, or other connections; or for miscommunications, failed, jumbled, scrambled, delayed, or misdirected computer, telephone or cable transmissions; or for any technical malfunctions, failures, difficulties or other errors of any kind or nature; or for the incorrect or inaccurate capture of information, or the failure to capture any information. Sponsor reserves the right to modify, extend, suspend, or terminate the Promotion if it determines, in its sole discretion, that the Promotion is technically impaired or corrupted or that fraud or technical problems, failures or malfunctions or other causes beyond Sponsor’s control have destroyed or severely undermined or to any degree impaired the integrity, administration, security, and/or feasibility of the Promotion. </p>

			<p>CAUTION: ANY ATTEMPT TO DELIBERATELY DAMAGE OR UNDERMINE THE LEGITIMATE OPERATION OF THE PROMOTION MAY BE IN VIOLATION OF CRIMINAL AND CIVIL LAWS AND WILL RESULT IN AN INDIVIDUAL BEING BANNED FROM PARTICIPATING. SHOULD SUCH AN ATTEMPT BE MADE, SPONSOR RESERVES THE RIGHT TO SEEK REMEDIES AND DAMAGES (INCLUDING ATTORNEY FEES) TO THE FULLEST EXTENT OF THE LAW, INCLUDING CRIMINAL PROSECUTION. PARTICIPANT AGREES THAT TO THE EXTENT PERMITTED BY APPLICABLE LAW: (1) ANY AND ALL DISPUTES, CLAIMS AND CAUSES OF ACTION ARISING OUT OF OR CONNECTED WITH THE PROMOTION WILL BE RESOLVED INDIVIDUALLY, WITHOUT RESORT TO ANY FORM OF CLASS ACTION; (2) ANY AND ALL CLAIMS, JUDGMENTS AND AWARDS WILL BE LIMITED TO ACTUAL THIRD-PARTY, OUT-OF-POCKET COSTS INCURRED, (IF ANY), NOT TO EXCEED TWO HUNDRED FIFTY DOLLARS ($250.00), BUT IN NO EVENT WILL ATTORNEYS’ FEES BE AWARDED OR RECOVERABLE; AND 3) UNDER NO CIRCUMSTANCES WILL ANY PARTICIPANT BE PERMITTED TO OBTAIN ANY AWARD FOR, AND PARTICIPANT HEREBY KNOWINGLY AND EXPRESSLY WAIVES ALL RIGHTS TO SEEK, PUNITIVE, INCIDENTAL, CONSEQUENTIAL OR SPECIAL DAMAGES, LOST PROFITS, AND/OR ANY RIGHTS TO HAVE DAMAGES MULTIPLIED OR OTHERWISE INCREASED. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATIONS OR EXCLUSION OF LIABILITY, SO THE ABOVE MAY NOT APPLY TO YOU. </p>

			<p><b>6. GOVERNING LAW: </b>Any and all disputes, claims and causes of action arising out of, or connected with, this Promotion, including without limitation, any alleged violation of these Terms &amp; Conditions, any controversy relating to the arbitrability of any dispute, or any claim that the Promotion, or any part thereof, is invalid, illegal or otherwise voidable (or void), shall be resolved exclusively by arbitration to be held solely in Los Angeles, California under the auspices of the American Arbitration Association and pursuant to its Commercial Dispute Resolution Rules and Procedures. Judgment upon the arbitration award may be entered in any court having jurisdiction thereof. This arbitration provision shall be deemed to be self-executing, and in the event that either party fails to appear at any properly noticed arbitration proceeding, an award may be entered against such party notwithstanding said failure to appear. In no event shall a Participant seek or be entitled to rescission, injunctive or other equitable relief, or to enjoin or restrain the operation of this Promotion. All issues and questions concerning the construction, validity, interpretation and enforceability of this Promotion, or the rights and obligations of Participants and Sponsor, shall be governed by, and construed in accordance with, the laws of the State of California, U.S.A., without giving effect to any choice of law or conflict of law rules (whether of the State of California or any other jurisdiction), which would cause the application of the laws of any jurisdiction other than the State of California. </p>

			<p><b>7. PRIVACY POLICY: </b>Participant understands that information is being collected by Sponsor. The information provided to Sponsor will only be used by Sponsor as provided in Sponsor’s privacy policy. See Sponsor’s privacy policy at http://www.toyota.com/help/privacy.html for details. </p>

			<p><b>The Promotion is in no way sponsored, endorsed or administered by Facebook or Twitter. Facebook and Twitter are completely released of all liability by each Participant in this Promotion.</b></p>

		</div>

	</div>
	</div>

</div>

<?php get_footer(); ?>