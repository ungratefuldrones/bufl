<?php get_header('mp-member'); ?>
    <div class="modules-wrapper">
      <div class="module module-mailbox">
              <div class="module-description">
        <h1><span class="icon-bg icon-archived"></span>My Mailbox</h1>
        </div>
        <div id="tabs" class="tabs">
          <ul class="tabs-nav"><li><a href="#tab-1"><span>All Messages & Alerts</span></a></li><li><a href="#tab-2"><span>Starred</span></a></li><li><a href="#tab-3"><span>Sent Messages</span></a></li></ul>
          <div id="tab-1" class="tab">
            <div class="search-for-tab">        
              <div class="module-header">
                <div class="subtitle-wrap clearfix">
                  <form class="search-portal">
                    <input class="text-input" type="search" name="search-partners" value="Search">
                  </form>
                  <div class="pagination">
                    <span class="pagi-count">1 - 8 of 43</span><div class="pagi pagi-left"><a href="#"><span></span></a></div><div class="pagi pagi-right"><a href="#"><span></span></a></div>
                  </div>
                </div>
              </div></div>
              <div class="table-div table-mailbox">
                <div class="row-div">
                  <div class="cell-div">Buckle Up for Life</div>
                  <div class="cell-div"><span class="bold">Lorem ipsum dolor set ameti -</span> Vestibulum lorem ipsum dolorphalliconsecetur adisc...</div>
                  <div class="cell-div">Oct. 28, 2014, 2:23 PM</div>
                  <div class="cell-div cell-favorites"><a href="#"><div class="icon-star"></div></a></div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Buckle Up for Life</div>
                  <div class="cell-div"><span class="bold">Lorem ipsum dolor set ameti -</span> Vestibulum lorem ipsum dolorphalliconsecetur adisc...</div>
                  <div class="cell-div">Oct. 28, 2014, 2:23 PM</div>
                  <div class="cell-div cell-favorites"><a href="#"><div class="icon-star"></div></a></div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Buckle Up for Life</div>
                  <div class="cell-div"><span class="bold">Lorem ipsum dolor set ameti -</span> Vestibulum lorem ipsum dolorphalliconsecetur adisc...</div>
                  <div class="cell-div">Oct. 28, 2014, 2:23 PM</div>
                  <div class="cell-div cell-favorites"><a href="#"><div class="icon-star"></div></a></div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Buckle Up for Life</div>
                  <div class="cell-div"><span class="bold">Lorem ipsum dolor set ameti -</span> Vestibulum lorem ipsum dolorphalliconsecetur adisc...</div>
                  <div class="cell-div">Oct. 28, 2014, 2:23 PM</div>
                  <div class="cell-div cell-favorites"><a href="#"><div class="icon-star"></div></a></div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
              </div>
            </div>
            <div id="tab-2" class="tab">
	            <div class="search-for-tab">        
	              <div class="module-header">
	                <div class="subtitle-wrap clearfix">
	                  <form class="search-portal">
	                    <input class="text-input" type="search" name="search-partners" value="Search">
	                  </form>
	                  <div class="pagination">
	                    <span class="pagi-count">1 - 8 of 43</span><div class="pagi pagi-left"><a href="#"><span></span></a></div><div class="pagi pagi-right"><a href="#"><span></span></a></div>
	                  </div>
	                </div>
	              </div>
	            </div>
	              <div class="table-div table-mailbox">
	                <div class="row-div">
	                  <div class="cell-div">Buckle Up for Life</div>
	                  <div class="cell-div"><span class="bold">Lorem ipsum dolor set ameti -</span> Vestibulum lorem ipsum dolorphalliconsecetur adisc...</div>
	                  <div class="cell-div">Oct. 28, 2014, 2:23 PM</div>
	                  <div class="cell-div cell-favorites"><a href="#"><div class="icon-star star-on"></div></a></div>
	                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
	                </div>
	                <div class="row-div">
	                  <div class="cell-div">Buckle Up for Life</div>
	                  <div class="cell-div"><span class="bold">Lorem ipsum dolor set ameti -</span> Vestibulum lorem ipsum dolorphalliconsecetur adisc...</div>
	                  <div class="cell-div">Oct. 28, 2014, 2:23 PM</div>
	                  <div class="cell-div cell-favorites"><a href="#"><div class="icon-star star-on"></div></a></div>
	                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
	                </div>                
	              </div>
            </div>
            <div id="tab-3" class="tab">
            
            	<div class="search-for-tab">        
	              <div class="module-header">
	                <div class="subtitle-wrap clearfix">
	                  <form class="search-portal">
	                    <input class="text-input" type="search" name="search-partners" value="Search">
	                  </form>
	                  <div class="pagination">
	                    <span class="pagi-count">1 - 8 of 43</span><div class="pagi pagi-left"><a href="#"><span></span></a></div><div class="pagi pagi-right"><a href="#"><span></span></a></div>
	                  </div>
	                </div>
	              </div>
	            </div>
	              <div class="table-div table-mailbox">
	                <div class="row-div">
	                  <div class="cell-div">Buckle Up for Life</div>
	                  <div class="cell-div"><span class="bold">Lorem ipsum dolor set ameti -</span> Vestibulum lorem ipsum dolorphalliconsecetur adisc...</div>
	                  <div class="cell-div">Oct. 28, 2014, 2:23 PM</div>
	                  <div class="cell-div cell-favorites"><a href="#"><div class="icon-star"></div></a></div>
	                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
	                </div>
	                <div class="row-div">
	                  <div class="cell-div">Buckle Up for Life</div>
	                  <div class="cell-div"><span class="bold">Lorem ipsum dolor set ameti -</span> Vestibulum lorem ipsum dolorphalliconsecetur adisc...</div>
	                  <div class="cell-div">Oct. 28, 2014, 2:23 PM</div>
	                  <div class="cell-div cell-favorites"><a href="#"><div class="icon-star star-on"></div></a></div>
	                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
	                </div>                
	              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer ('mp'); ?>