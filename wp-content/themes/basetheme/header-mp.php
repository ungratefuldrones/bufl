<!DOCTYPE html>
<html>

<?php 
  session_start();
  MemberPortal::permission();
  MemberPortal::mp_form_handler(); 
?>

<head>
    <meta charset="utf-8">
    <title>Buckle Up For Life Member Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="a website description">

    <link rel="stylesheet" type="text/css" href="/wp-content/themes/basetheme/css/member-portal.css" />
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    
    <!--TweenMax-->
  <script src="<?php bloginfo('template_directory'); ?>/js/TweenMax.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/js/TimelineMax.min.js"></script>
  
  <script src="<?php bloginfo('template_directory'); ?>/audiojs/audio.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/js/jquery.cycle2.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/js/scripts.js"></script>
  
  <script src="<?php bloginfo('template_directory'); ?>/assets/dist/bufl-member-portal.all.js"></script>


    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7324852/730822/css/fonts.css" />

  <script>
    var member_data = <?php MemberPortal::print_data(); ?>;
    
    <?php if (isset($_SESSION['message_type'])) : ?>
      member_data.message_type = <?php echo $_SESSION['message_type']; unset($_SESSION['message_type']); ?>;
    <?php endif; ?>
  
    console.log(member_data);

  </script>

  <?php 
  if (is_user_logged_in()) {
    $user = wp_get_current_user();
    
    if ('member' == $user->caps) {
      $body_class [] = 'portal-partner';
    }
    else {
      $body_class [] = 'portal-admin';
    }
  }
  ?>

</head>


<body <?php body_class($body_class); ?>>

<?php require('js/member-portal/templates.js'); ?>

<div class="wrapper" id="member-portal">
  <div class="top-line"></div>
  <div class="header clearfix">
    <a href="<?php echo home_url(); ?>"><img class="logo" src="/wp-content/themes/basetheme/images/BUFL_Logo_2x.jpg" /></a>
    <div class="settings-user">
     <ul class="dropdown">
        <li><a href="/my-account" class="drop-link">My Account</a>
          <ul class="dropdown-options">
            <li class="route" data-route="profile">Profile Settings</li>
            <li class="route" data-route="mailbox">Mailbox</li>
            <li class="route" data-route="logout">Log out</li>
          </ul>
        </li>
    </ul>
    <span class="seperator">|</span>
    <a href="<?php echo home_url(); ?>" class="link-home">BuckleUpforLife.org</a>
    </div>
    </div>