<?php
//Some simple code for our widget-enabled sidebar
if ( function_exists('register_sidebar') )
    register_sidebar();

//Add support for Widget Areas
add_action( 'init', 'news_post_type' );
add_action( 'init', 'hospital_partners_post_type' );
add_action( 'init', 'video_post_type' );
add_action( 'init', 'vignette_post_type' );
add_action( 'init', 'register_my_menu' );
add_action( 'init', 'footer_widgets' );
add_action( 'init', 'member_portal_post_types' );
add_action( 'init', 'member_portal_caps' );

function member_portal_caps() {
    // this should all be moved into theme activation or a plugin, stored in the db so doesnt need to run on every page.
    $members = get_role( 'member' );
    $members->add_cap( 'edit_document' );
    $members->add_cap( 'edit_documents' );
    $members->add_cap( 'read_document' );
    $members->add_cap( 'read_private_documents' );
    $members->add_cap( 'delete_document' );
    $members->add_cap( 'delete_documents' );

    $admins = get_role( 'administrator' );
    $admins->add_cap( 'edit_document' );
    $admins->add_cap( 'edit_documents' );
    $admins->add_cap( 'edit_other_documents' );
    $admins->add_cap( 'read_document' );
    $admins->add_cap( 'read_private_documents' );
    $admins->add_cap( 'delete_document' );
    $admins->add_cap( 'delete_documents' );  
    $admins->add_cap( 'publish_documents' );   
}

function member_portal_post_types () {

	// Custom Post Types for Member Port
	register_post_type(
		'document', array(
			'labels' => array(
				'name' => __('Documents'),
				'singular_name' => __('Document')
			),
			'public' => true,
			'has_archive' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'taxonomies' => array('category'),
			'hierarchical' => false,
			'supports' => array(
				'title',
				'thumbnail'
			),
			'capabilities' => array(
		        'edit_post' => 'edit_document',
		        'edit_posts' => 'edit_documents',
		        'read_post' => 'read_document',
		        'read_private_posts' => 'read_private_documents',
		        'delete_post' => 'delete_document',
		        'delete_posts' => 'delete_documents'
		    ),
		    'map_meta_cap' => true
		)
	);

	// Custom Post Types for Member Port
	register_post_type(
		'message', array(
			'labels' => array(
				'name' => __('Messages'),
				'singular_name' => __('Message')
			),
			'public' => true,
			'has_archive' => false,
			'show_ui' => true,
			'show_in_menu' => false,
			'hierarchical' => false,
			'supports' => array(
				'title',
				'thumbnail',
			)
		)
	);

}

function news_post_type ( ) {

	// Custom Post Type
	register_post_type('news', array(
			'labels' => array(
					'name' => __('News'),
					'singular_name' => __('Article'),
					'add_new_item' => __('Add New Article'),
					'edit_item' => __('Edit Article'),
					'view_item' => __('View Article'),
			),
			'public' => true,
			'has_archive' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'hierarchical' => false,
			'rewrite' => array(
					'slug' => 'news',
					'with_front' => false
			),
			'supports' => array(
					'title',
					'editor',
					'thumbnail',
			)
	));

}

function hospital_partners_post_type ( ) {

	// Custom Post Type
	register_post_type('hospital partners', array(
			'labels' => array(
					'name' => __('Hospital Partners'),
					'singular_name' => __('Hospital Partner'),
					'add_new_item' => __('Add New Hospital Partner'),
					'edit_item' => __('Edit Hospital Partner'),
					'view_item' => __('View Hospital Partner'),
			),
			'public' => true,
			'has_archive' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'hierarchical' => false,
			'rewrite' => array(
					'slug' => 'hospital_partners',
					'with_front' => false
			),
			'supports' => array(
					'title',
					'editor',
					'thumbnail',
			)
	));

}

function video_post_type ( ) {

	// Custom Post Type
	register_post_type('video', array(
			'labels' => array(
					'name' => __('Videos'),
					'singular_name' => __('Video'),
					'add_new_item' => __('Add New Video'),
					'edit_item' => __('Edit Video'),
					'view_item' => __('View Video'),
			),
			'public' => true,
			'has_archive' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'hierarchical' => false,
			'rewrite' => array(
					'slug' => 'video',
					'with_front' => false
			),
			'supports' => array(
					'title',
					'editor'
			)
	));

}

function vignette_post_type ( ) {

	// Custom Post Type
	register_post_type('vignette', array(
			'labels' => array(
					'name' => __('Vignettes'),
					'singular_name' => __('Vignette'),
					'add_new_item' => __('Add New Vignette'),
					'edit_item' => __('Edit Vignette'),
					'view_item' => __('View Vignette'),
			),
			'public' => true,
			'has_archive' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'hierarchical' => false,
			'rewrite' => array(
					'slug' => 'vignette',
					'with_front' => false
			),
			'supports' => array(
					'title',
					'editor'
			)
	));

}

//Create Primary Navigation
function register_my_menu() {
	register_nav_menu( 'primary-menu', __( 'Primary Menu' ) );
}

//Create Footer Widgets
function footer_widgets() {

	register_sidebar( array(
		'name' => __( 'Footer Widget Area'),
		'id' => 'footer-widget-area',
		'before_widget' => '<li>',
		'after_widget' => '</li>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

}

// Return true if there is a $_GET variable set for embed
function embed() {
	return isset($_GET['embed']);
}

// Return embed url.  Will be empty string if not embedding
function get_embed_url() {
	return (embed() ? "?embed=true" : "");
}

// Add javascript embed variable
function embed_js() {
	echo (embed() ? "<script> var embed_tool = true </script>" : "<script> var embed_tool = false </script>" );
}
//Code for custom background support
add_theme_support( 'custom-background' );

//Enable post and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );

// Enable post thumbnails
add_theme_support('post-thumbnails');
set_post_thumbnail_size(520, 250, true);

//Custom thumbnails
add_image_size('hero-mobile', 320, 175, true);	// hero mobile slides
add_image_size('know-mobile', 220, null, true);	// did you know mobile slides

// Used for news pagers
add_filter('get_previous_post_join', 'clear_post_join', 99);
add_filter('get_next_post_join', 'clear_post_join', 99);

// member portal is separated
require_once('includes/member_portal/MemberPortal.php');
MemberPortal::load_data();

function clear_post_join ( $join ) {
	return '';
}

function hfc_output_pagination ( $options, $wp_query = null ) {

	$defaults = array(
			'total' => 1,
			'current' => 1,
			'mid_size' => 4,
			'type' => 'list',
			'prev_text' => 'Previous',
			'next_text' => 'Next',

			// Non paginate_links variables
			'pretty_permalinks' => null, // true or false, null for auto
			'url_path' => parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),
			'url_query' => parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY),
	);

	// Override defaults with WP_Query
	if ( is_a($wp_query, 'WP_Query') ) {
		$defaults['total'] = $wp_query->max_num_pages;
		$defaults['current'] = max($wp_query->get('paged'), 1);
	}

	// Merge options with defaults
	$options = wp_parse_args($options, $defaults);

	// Pretty permalinks setting
	if ( is_null($options['pretty_permalinks']) ) {
		// Auto detect pretty permalinks
		$pretty_permalinks = strlen($options['url_query']) == 0;
	} else {
		$pretty_permalinks = (bool) $options['pretty_permalinks'];
	}

	// URL paths and query strings
	$url_path = $options['url_path'];
	$url_query = $options['url_query'];

	// Fill in missing options
	if ( ! array_key_exists('format', $options) ) {
		if ( $pretty_permalinks ) {
			$url_path = preg_replace('#/page/[0-9]+/#', '', $url_path); // Strip page number out
			$format = 'page/%#%/'; // A leading "/" will be added to the base
		} else {
			$url_query = preg_replace('#&?paged=[0-9]+#', '', $url_query); // Strip page number out
			$format = ( strlen($url_query) ? '&' : '' ) . 'paged=%#%'; // If there is still a query string (after removing page number), add an "&". A leading "?" will be added to the base
		}

		$options['format'] = $format;
	}

	if ( ! array_key_exists('base', $options) ) {
		$options['base'] = trailingslashit($url_path); // Add leading "/"
		if ( $pretty_permalinks ) $options['base'] .= '%_%'; // Add pretty permalinks base
		if ( strlen($url_query) || ! $pretty_permalinks ) $options['base'] .= '?' . $url_query; // Add url_query for pages with a query string or not using pretty permalinks
		if ( ! $pretty_permalinks ) $options['base'] .= '%_%'; // Add query string base
	}

	echo paginate_links($options);
}

/**
 * Member portal login via AJAX
 */
function ajax_login_init(){

	wp_register_script('ajax-login-script', get_template_directory_uri() . '/js/ajax-login-script.js', array('jquery') );
	wp_enqueue_script('ajax-login-script');

	wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'redirecturl' => home_url() . '/member-portal-partner-home',
			'redirecturl_admin' => home_url() . '/member-portal-home',
			'loadingmessage' => __('Sending user info, please wait...')
	));

	// Enable the user with no privileges to run ajax_login() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
	add_action('init', 'ajax_login_init');
}

function ajax_login(){

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	// Nonce is checked, get the POST data and sign user on
	$info = array();
	$info['user_login'] = $_POST['username'];
	$info['user_password'] = $_POST['password'];
	$info['remember'] = $_POST['rememberme'];

	$user_signon = wp_signon( $info, false );
	
	// check what this user is Admin or Partner; this will be passed to Ajax to know where to redirect
	if (isset ($user_signon->caps)) {
		if ($user_signon->caps ['administrator']) $caps = 'admin';
		else if ($user_signon->caps ['member']) $caps = 'member';
		else $caps = '';
	}
	
	if ( is_wp_error($user_signon) ){
		echo json_encode(array('loggedin'=>false, 'message'=>__('<span class="red">Wrong username or password.</span>')));
	} else {
		echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...'), 'caps' => $caps));
	}

	die();
}

/**
 * END Member portal login via AJAX
 */


// modify the Hospital drop-down on the Forgot pass form (Formidable)
function frm_set_checked ($values, $field){

	if ($field->id == 51 ) { // field ID	51 on Tz's Dev

		$args = array(
			'post_type' 	=> 'hospitalpartners',			
			'orderby'       => 'name',
			'order'         => 'ASC',
			'posts_per_page' => -1
	
	);
	$hospitals = get_posts ($args);
	
	$data = array ();
	$data [] = 'Hospital';
	foreach ($hospitals as $h) {
		if (!in_array ($h->post_title, $data)) $data [] = $h->post_title;
	}

	$values['options'] = $data; //Replace Option1 and Option2 with the options you want in the field
	}
	return $values;
}
add_filter('frm_setup_new_fields_vars', 'frm_set_checked', 20, 2);

function isSiteAdmin(){
	$currentUser = wp_get_current_user();
	return in_array('administrator', $currentUser->roles);
}

add_action( 'wp_ajax_mp_approve_document', 'mp_approve_document');
add_action( 'wp_ajax_nopriv_mp_approve_document', 'mp_approve_document');
function mp_approve_document () {
	if (mp_get_user_role() !== 'administrator') return false; //only admins can do this.
	
	$response = array();

	$document_id = $_POST['document_id'];
	if (get_post_status($document_id) == 'pending') {
		wp_publish_post($document_id);
		$response['success'] = true;
		$response['message'] = 'Document successfully published.';
	} else {
		$response['success'] = false;
		$response['message'] = 'Document publish error. ID: ' . $document_id . ', status: ' . get_post_status($document_id);
	}
	echo json_encode($response);
	die();
}

add_action( 'wp_ajax_mp_reject_document', 'mp_reject_document');
add_action( 'wp_ajax_nopriv_mp_reject_document', 'mp_reject_document');
function mp_reject_document () {
	if (mp_get_user_role() !== 'administrator') return false; //only admins can do this.
	
	$response = array();

	$document_id = $_POST['document_id'];
	$trashed = wp_trash_post($document_id);

	if ($trashed) {
		$response['success'] = true;
		$response['message'] = 'Document successfully rejected.';
	} else {
		$response['success'] = false;
		$response['message'] = 'Document reject error. ID: ' . $document_id . ', status: ' . get_post_status($document_id);
	}
	echo json_encode($response);
	die();
}

function mp_get_user_role() {
	global $current_user;

	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);

	return $user_role;
}

// returns the attachment ID 
function process_uploaded_file ($file_index) {
	
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );

	$attach_id = media_handle_upload($file_index, 0);

	if ( is_wp_error( $attach_id ) ) {
		// echo "<h2>" . $attach_id->get_error_message() . "</h2>";
		// echo "<pre>";
		// print_r($_FILES);
		// echo "</pre>";
		// die();
		return false; // this is okay.
	} else {
		// The file was uploaded successfully!
		return $attach_id;
	}

}


/* function insert_entries ($form_id, $values, $user_id) {
	global $frm_entry;
	
	$frm_entry->create(array(
			'form_id' => $form_id, //change 5 to your form id
			'item_key' => 'entry', //change entry to a dynamic value if you would like
			'frm_user_id' => $user_id,
			'item_meta' => $values
	));
} */

/**
 * Takes a Formidable form id
 * Returns the total number of entries for that form 
 * @param int $form_id
 */
function get_frm_number_of_entries ($form_id) {
	global $wpdb;
	
	$table = 'wp_frm_items';
	
	$sql = "SELECT count(id) count
			FROM $table
			WHERE form_id = $form_id";
	
	$results = $wpdb->get_results (
				$sql
			);
	
	if (!empty ($results)) return $results[0]->count;
	
	return false;
}

function bufl_ajax_survey_input_func () {

	$language = $_POST['language'];
	$person_id = $_POST['person_id'];
	$date = $_POST['date'];
	$pre_post_class = $_POST['pre_post_class'];
	$ethnicity = $_POST['ethnicity'];
	$age_group = $_POST['age_group'];
	$q1 = $_POST['q1'];
	$q2 = $_POST['q2'];
	$q3 = $_POST['q3'];
	$q4 = $_POST['q4'];
	$q5 = $_POST['q5'];
	$q6 = $_POST['q6'];
	$q7 = $_POST['q7'];
	$q8 = $_POST['q8'];
	$q9 = $_POST['q9'];
	$q10 = $_POST['q10'];
	$user_id = $_POST['user_id'];
	
	// IDs below must be changed for LIVE!!!
	// TZ dev
	if (strpos ($_SERVER ['HTTP_HOST'], 'bufl.tzvety.hfwebdev.com') !== false) {
		$form_id = 12;
		$language_form_field_id = 147;
		$person_id_form_field_id = 148;
		$date_form_field_id = 149;
		$pre_post_class_form_field_id = 150;
		$ethnicity_form_field_id = 151;
		$age_group_form_field_id = 152;
		$q1_form_field_id = 153;
		$q2_form_field_id = 154;
		$q3_form_field_id = 155;
		$q4_form_field_id = 156;
		$q5_form_field_id = 157;
		$q6_form_field_id = 158;
		$q7_form_field_id = 159;
		$q8_form_field_id = 160;
		$q9_form_field_id = 161;
		$q10_form_field_id = 162;
		$form_submission_id = 205;
	}
	
	// Staging
	if (strpos ($_SERVER ['HTTP_HOST'],'bufl.hfwebdev.com') !== false) {
		$form_id = 7;
		$language_form_field_id = 61;
		$person_id_form_field_id = 62;
		$date_form_field_id = 63;
		$pre_post_class_form_field_id = 64;
		$ethnicity_form_field_id = 65;
		$age_group_form_field_id = 66;
		$q1_form_field_id = 67;
		$q2_form_field_id = 68;
		$q3_form_field_id = 69;
		$q4_form_field_id = 70;
		$q5_form_field_id = 71;
		$q6_form_field_id = 72;
		$q7_form_field_id = 73;
		$q8_form_field_id = 74;
		$q9_form_field_id = 75;
		$q10_form_field_id = 76;
		$form_submission_id = 119;
	}
	
	$num_entries_right_before_this = get_frm_number_of_entries ($form_id);
	
	$values = array(
			$language_form_field_id => $language, //change 25 to your field ID and 'value' to your value
			$person_id_form_field_id => $person_id,
			$date_form_field_id => $date,
			$pre_post_class_form_field_id => $pre_post_class,
			$ethnicity_form_field_id => $ethnicity,
			$age_group_form_field_id => $age_group,
			$q1_form_field_id => $q1,
			$q2_form_field_id => $q2,
			$q3_form_field_id => $q3,
			$q4_form_field_id => $q4,
			$q5_form_field_id => $q5,
			$q6_form_field_id => $q6,
			$q7_form_field_id => $q7,
			$q8_form_field_id => $q8,
			$q9_form_field_id => $q9,
			$q10_form_field_id => $q10,
			$form_submission_id => $num_entries_right_before_this + 1
	);
	// enter the info
	global $frm_entry;
	$frm_entry->create(array(
			'form_id' => $form_id, //change 5 to your form id
			'item_key' => 'entry', //change entry to a dynamic value if you would like
			'frm_user_id' => $user_id,
			'item_meta' => $values
	));

	// send back results
	header( "Content-Type: application/json" );
	
	// terminate ajax
	die();
}
add_action( 'wp_ajax_bufl_ajax_survey_input', 'bufl_ajax_survey_input_func');
add_action( 'wp_ajax_nopriv_bufl_ajax_survey_input', 'bufl_ajax_survey_input_func');



function bufl_ajax_observation_input_func () {

	$name = $_POST['name'];
	$location =  $_POST['location'];
	$date =  $_POST['date'];
	$from_time =  $_POST['from_time'];
	$to_time =  $_POST['to_time'];
	$pre_post = $_POST ['pre_post'];	
	$location =  $_POST['location'];
	
	// person 1 data
	$person1_age = $_POST ['person1_age'];
	$person1_restraint = $_POST['person1_restraint'];
	$person1_gender = $_POST ['person1_gender'];
	$person1_child_restraint = $_POST ['person1_child_restraint'];
	
	// person 2 data
	$person2_age = $_POST ['person2_age'];
	$person2_restraint = $_POST['person2_restraint'];
	$person2_gender = $_POST ['person2_gender'];
	$person2_child_restraint = $_POST ['person2_child_restraint'];
	
	// person 3 data
	$person3_age = $_POST ['person3_age'];
	$person3_restraint = $_POST['person3_restraint'];
	$person3_gender = $_POST ['person3_gender'];
	$person3_child_restraint = $_POST ['person3_child_restraint'];
	
	// person 4 data
	$person4_age = $_POST ['person4_age'];
	$person4_restraint = $_POST['person4_restraint'];
	$person4_gender = $_POST ['person4_gender'];
	$person4_child_restraint = $_POST ['person4_child_restraint'];
	
	// person 5 data
	$person5_age = $_POST ['person5_age'];
	$person5_restraint = $_POST['person5_restraint'];
	$person5_gender = $_POST ['person5_gender'];
	$person5_child_restraint = $_POST ['person5_child_restraint'];
	
	// person 6 data
	$person6_age = $_POST ['person6_age'];
	$person6_restraint = $_POST['person6_restraint'];
	$person6_gender = $_POST ['person6_gender'];
	$person6_child_restraint = $_POST ['person6_child_restraint'];
	
	// person 7 data
	$person7_age = $_POST ['person7_age'];
	$person7_restraint = $_POST['person7_restraint'];
	$person7_gender = $_POST ['person7_gender'];
	$person7_child_restraint = $_POST ['person7_child_restraint'];
	
	// person 8 data
	$person8_age = $_POST ['person8_age'];
	$person8_restraint = $_POST['person8_restraint'];
	$person8_gender = $_POST ['person8_gender'];
	$person8_child_restraint = $_POST ['person8_child_restraint'];
	
	// person 9 data
	$person9_age = $_POST ['person9_age'];
	$person9_restraint = $_POST['person9_restraint'];
	$person9_gender = $_POST ['person9_gender'];
	$person9_child_restraint = $_POST ['person9_child_restraint'];
	
	if (strpos ($_SERVER ['HTTP_HOST'], 'bufl.tzvety.hfwebdev.com') !== false) {
		$form_id = 13;
		$name_field_id = 163;
		$location_field_id = 164;
		$date_field_id = 165;
		$from_time_field_id = 166;
		$to_time_field_id = 167;
		$pre_post_field_id = 168;
		
		//person 1
		$person1_age_field_id = 169;
		$person1_restrained_field_id = 178;
		$person1_gender_field_id = 187;
		$person1_child_restraint_field_id = 196;
		
		//person 2
		$person2_age_field_id = 170;
		$person2_restrained_field_id = 179;
		$person2_gender_field_id = 188;
		$person2_child_restraint_field_id = 197;
		
		//person 3
		$person3_age_field_id = 171;
		$person3_restrained_field_id = 180;
		$person3_gender_field_id = 189;
		$person3_child_restraint_field_id = 198;
		
		// person 4
		$person4_age_field_id = 172;
		$person4_restrained_field_id = 181;
		$person4_gender_field_id = 190;
		$person4_child_restraint_field_id = 199;
		
		// person 5
		$person5_age_field_id = 173;
		$person5_restrained_field_id = 182;
		$person5_gender_field_id = 191;
		$person5_child_restraint_field_id = 200;
		
		// person 6
		$person6_age_field_id = 174;
		$person6_restrained_field_id = 183;
		$person6_gender_field_id = 192;
		$person6_child_restraint_field_id = 201;
		
		// person 7
		$person7_age_field_id = 175;
		$person7_restrained_field_id = 184;
		$person7_gender_field_id = 193;
		$person7_child_restraint_field_id = 202;
		
		// person 8
		$person8_age_field_id = 176;
		$person8_restrained_field_id = 185;
		$person8_gender_field_id = 194;
		$person8_child_restraint_field_id = 203;
		
		// person 9
		$person9_age_field_id = 177;
		$person9_restrained_field_id = 186;
		$person9_gender_field_id = 195;
		$person9_child_restraint_field_id = 204;
	}
	
	// Staging
	if (strpos ($_SERVER ['HTTP_HOST'], 'bufl.hfwebdev.com') !== false) {
		$form_id_field_id = 8;
		$name_field_id = 77;
		$location_field_id = 78;
		$date_field_id = 79;
		$from_time_field_id = 80;
		$to_time_field_id = 81;
		$pre_post_field_id = 82;		
		
		
		//person 1
		$person1_age_field_id = 83;
		$person1_restrained_field_id = 84;
		$person1_gender_field_id = 85;
		$person1_child_restraint_field_id = 86;
		
		//person 2
		$person2_age_field_id = 87;
		$person2_restrained_field_id = 88;
		$person2_gender_field_id = 89;
		$person2_child_restraint_field_id = 90;
		
		//person 3
		$person3_age_field_id = 91;
		$person3_restrained_field_id = 92;
		$person3_gender_field_id = 93;
		$person3_child_restraint_field_id = 94;
		
		// person 4
		$person4_age_field_id = 95;
		$person4_restrained_field_id = 96;
		$person4_gender_field_id = 97;
		$person4_child_restraint_field_id = 98;
		
		// person 5
		$person5_age_field_id = 99;
		$person5_restrained_field_id = 100;
		$person5_gender_field_id = 101;
		$person5_child_restraint_field_id = 102;
		
		// person 6
		$person6_age_field_id = 103;
		$person6_restrained_field_id = 104;
		$person6_gender_field_id = 105;
		$person6_child_restraint_field_id = 106;
		
		// person 7
		$person7_age_field_id = 107;
		$person7_restrained_field_id = 108;
		$person7_gender_field_id = 109;
		$person7_child_restraint_field_id = 110;
		
		// person 8
		$person8_age_field_id = 111;
		$person8_restrained_field_id = 112;
		$person8_gender_field_id = 113;
		$person8_child_restraint_field_id = 114;
		
		// person 9
		$person9_age_field_id = 115;
		$person9_restrained_field_id = 116;
		$person9_gender_field_id = 117;
		$person9_child_restraint_field_id = 118;
	}
	
	$values = array(
			$name_field_id => $name,
			$location_field_id => $location,
			$date_field_id => $date,
			$from_time_field_id => $from_time,
			$to_time_field_id => $to_time,
			$pre_post_field_id => $pre_post			
	);
	
	if (isset ($person1_age) && $person1_age != '') $values [$person1_age_field_id] = $person1_age;
	if (isset ($person1_restraint) && $person1_restraint != '') $values [$person1_restrained_field_id] = $person1_restraint;
	if (isset ($person1_gender) && $person1_gender != '') $values [$person1_gender_field_id] = $person1_gender;
	if (isset ($person1_child_restraint) && $person1_child_restraint != '') $values [$person1_child_restraint_field_id] = $person1_child_restraint;
	
	if (isset ($person2_age) && $person2_age != '') $values [$person2_age_field_id] = $person2_age;
	if (isset ($person2_restraint) && $person2_restraint != '') $values [$person2_restrained_field_id] = $person2_restraint;
	if (isset ($person2_gender) && $person2_gender != '') $values [$person2_gender_field_id] = $person2_gender;
	if (isset ($person2_child_restraint) && $person2_child_restraint != '') $values [$person2_child_restraint_field_id] = $person2_child_restraint;
	
	if (isset ($person3_age) && $person3_age != '') $values [$person3_age_field_id] = $person3_age;
	if (isset ($person3_restraint) && $person3_restraint != '') $values [$person3_restrained_field_id] = $person3_restraint;
	if (isset ($person3_gender) && $person3_gender != '') $values [$person3_gender_field_id] = $person3_gender;
	if (isset ($person3_child_restraint) && $person3_child_restraint != '') $values [$person3_child_restraint_field_id] = $person3_child_restraint;
	
	if (isset ($person4_age) && $person4_age != '') $values [$person4_age_field_id] = $person4_age;
	if (isset ($person4_restraint) && $person4_restraint != '') $values [$person1_restrained_field_id] = $person4_restraint;
	if (isset ($person4_gender) && $person4_gender != '') $values [$person4_gender_field_id] = $person4_gender;
	if (isset ($person4_child_restraint) && $person4_child_restraint != '') $values [$person4_child_restraint_field_id] = $person4_child_restraint;
	
	if (isset ($person5_age) && $person5_age != '') $values [$person5_age_field_id] = $person5_age;
	if (isset ($person5_restraint) && $person5_restraint != '') $values [$person5_restrained_field_id] = $person5_restraint;
	if (isset ($person5_gender) && $person5_gender != '') $values [$person5_gender_field_id] = $person5_gender;
	if (isset ($person5_child_restraint) && $person5_child_restraint != '') $values [$person5_child_restraint_field_id] = $person5_child_restraint;
	
	if (isset ($person6_age) && $person6_age != '') $values [$person6_age_field_id] = $person6_age;
	if (isset ($person6_restraint) && $person6_restraint != '') $values [$person6_restrained_field_id] = $person6_restraint;
	if (isset ($person6_gender) && $person6_gender != '') $values [$person6_gender_field_id] = $person6_gender;
	if (isset ($person6_child_restraint) && $person6_child_restraint != '') $values [$person6_child_restraint_field_id] = $person6_child_restraint;
	
	if (isset ($person7_age) && $person7_age != '') $values [$person7_age_field_id] = $person7_age;
	if (isset ($person7_restraint) && $person7_restraint != '') $values [$person7_restrained_field_id] = $person7_restraint;
	if (isset ($person7_gender) && $person7_gender != '') $values [$person7_gender_field_id] = $person7_gender;
	if (isset ($person7_child_restraint) && $person7_child_restraint != '') $values [$person7_child_restraint_field_id] = $person7_child_restraint;
	
	if (isset ($person8_age) && $person8_age != '') $values [$person8_age_field_id] = $person8_age;
	if (isset ($person8_restraint) && $person8_restraint != '') $values [$person8_restrained_field_id] = $person8_restraint;
	if (isset ($person8_gender) && $person8_gender != '') $values [$person8_gender_field_id] = $person8_gender;
	if (isset ($person8_child_restraint) && $person8_child_restraint != '') $values [$person8_child_restraint_field_id] = $person8_child_restraint;
	
	if (isset ($person9_age) && $person9_age != '') $values [$person9_age_field_id] = $person9_age;
	if (isset ($person9_restraint) && $person9_restraint != '') $values [$person9_restrained_field_id] = $person9_restraint;
	if (isset ($person9_gender) && $person9_gender != '') $values [$person9_gender_field_id] = $person9_gender;
	if (isset ($person9_child_restraint) && $person9_child_restraint != '') $values [$person9_child_restraint_field_id] = $person9_child_restraint;
	
	// enter the info
	global $frm_entry;
	$frm_entry->create(array(
			'form_id' => $form_id, //change 5 to your form id
			'item_key' => 'entry', //change entry to a dynamic value if you would like
			'item_meta' => $values
	));

	// terminate ajax
	die();
}
add_action( 'wp_ajax_bufl_ajax_observation_input', 'bufl_ajax_observation_input_func');
add_action( 'wp_ajax_nopriv_bufl_ajax_observation_input', 'bufl_ajax_observation_input_func');

?>
