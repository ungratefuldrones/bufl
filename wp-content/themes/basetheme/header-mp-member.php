<?php get_header('mp'); ?>

<div class="main">
<?php get_sidebar ('mp-member'); ?>
    <div class="content">
      <div class="toolbar clearfix">
        <ul class="tools">
          <li><a href="javascript:void(0);" class="trigger-overlay upload"><span class="icon-bg icon-upload"></span>Upload</a></li>
          <li><a href="javascript:void(0);" class="trigger-overlay compose"><span class="icon-bg icon-compose"></span>Compose</a></li>
          <?php if (isSiteAdmin()) : ?>
            <li class="archived-docs"><a href="javascript:void(0);" class="route" data-route="archive"><span class="icon-bg icon-archives"></span>Archived Documents</a></li>
          <?php endif; ?>
        </ul>
        <div class="logout">
          <a href="<?php echo wp_logout_url( home_url() ); ?>"><span class="icon-bg icon-lock"></span>Sign Out</a>
        </div>
      </div>
      <div class="alerts-wrapper">
      </div>