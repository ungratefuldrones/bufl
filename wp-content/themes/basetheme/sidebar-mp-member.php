<div class="sidebar">
  <div class="hello-box route" data-route="/">
    <div class="hello-inner">
      <img class="hello-img" src="http://placebear.com/40/40" />
      <div class="hello-msg">
        <span class="hello">Hello,</span> <div class="hello-name"><?php if (isSiteAdmin()) echo 'Gloria'; else echo 'Partner XX'; ?>!</div>
      </div>
    </div>
  </div>
  <ul class="sidebar-nav">
    <li><a class="route" data-route="mailbox" href="/my-mailbox">
    		<span class="icon-bg nav-icon icon-mailbox"></span>
    		Mailbox<span class="mail-count">14</span>
    		<span class="icon-bg nav-arrow"></span>
    	</a>
    	<ul class="sub-menu">
    		<li class="menu-item">
    			<a href="#" class="item-title">Some Menu Item</a>
    			<div class="item-date">Oct 10</div>
    			<div class="item-content">Veggies es bonus vobis, proinde vos postulo essum magis kohlrabi ...</div>
    			<div class="item-utilities">
    				<div class="buttons">
    					<div class="button view">View/Reply</div>
    					<div class="button archive">Archive</div>
    				</div>
    			</div>
    		</li>
    		<li class="menu-item view-all">
    			<a href="/my-mailbox">View All Messages &rsaquo;</a>
    		</li>
    	</ul>
    </li>
    <li><a class="route" data-route="program-implementation" href="/program-implementation"><span class="icon-bg nav-icon icon-heart"></span>Program Implementation<span class="icon-bg nav-arrow"></span></a></li>
    <li><a class="route" data-route="reporting" href="/reporting"><span class="icon-bg nav-icon icon-reporting"></span>Reporting<span class="icon-bg nav-arrow"></span></a></li>
    <li><a class="route" data-route="media-toolbox" href="/media-toolbox"><span class="icon-bg nav-icon icon-briefcase"></span>Media Toolbox<span class="icon-bg nav-arrow"></span></a></li>
    <li><a class="route" data-route="additional-resources" href="#"><span class="icon-bg nav-icon icon-drawer"></span>Additional Resources<span class="icon-bg nav-arrow"></span></a></li>
  </ul>
  <?php $homepage = get_page_by_title( 'Member Portal Home'); ?>
  <div class="sidebar-content">
    <form class="search-side">
      <input class="text-input" type="search" name="search-side" value="Search">
    </form>
    <div class="callout">
    	<a class="thumbnail" href="<?php the_field ('mp_sidebar_toolkit_attachment', $homepage->ID); ?>" target="_blank"><img src="<?php the_field ('mp_sidebar_toolkit_thumbnail', $homepage->ID); ?>" /></a>
    	<p><?php the_field ('mp_sidebar_toolkit_blurb', $homepage->ID); ?></p>
    	<a class="button download" href="<?php the_field ('mp_sidebar_toolkit_attachment', $homepage->ID); ?>" target="_blank"><?php the_field ('mp_sidebar_toolkit_button_text', $homepage->ID); ?></a>
    </div>
    <div class="callout blue">
    	<h2>Observation Survey</h2>    	
    	<p><?php the_field ('mp_sidebar_observation_survey_blurb', $homepage->ID); ?></p>
    	<a href="/wp-content/themes/basetheme/observation-form.html" style="width: 180px;"><img src="<?php echo get_bloginfo('template_directory').'/images/'; ?>observation-survey-thumbnail.png" /></a>
    </div>    
  </div> 
  <div class="copyright">
      <?php the_field ('mp_sidebar_copyright', $homepage->ID); ?>
   </div>
</div>