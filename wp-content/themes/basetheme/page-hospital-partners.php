
<?php get_header(); ?>

<div id="pageWrapper" <?php if ( embed() ) echo "class='embedded-tool'"; ?> >

	<div id="pageTitle">
		<?php $legal = (strpos($_SERVER["REQUEST_URI"], 'legal') ? $legal = true : $legal = false); ?>
		<?php if ($legal) : ?>
			<a class="back-to-tool" href="<?php echo (pll_current_language() == 'en' ? "/".get_embed_url() : "/es/".get_embed_url()); ?>">&#8249; Back to Tool</a>
		<?php endif ?>
		<h1><?php the_title(); ?></h1>
	</div>

	<div id="pageContent" class="clearfix">
		<div id="pageImage">
			<img src="<?php the_field('page_image'); ?>" class="pageImageLarge"/>
			<img src="<?php the_field('page_image_mobile'); ?>" class="pageImageSmall"/>
		</div>

		<div class="partner-wrap">

			<div class="grid">

				<h1>Hospital Partners</h1>

				<?php
	                $args = array(
	                    'post_type' => 'hospitalpartners',
	                    'post_status' => 'publish',
	                    'orderby' => 'title',
	                    'order' => 'ASC',
	                    'posts_per_page' => -1,
	                );
	                $hospitalGrid = get_posts ($args);

	                $partnerNumber = 1;
	            ?>

	            <?php
	                if (count($hospitalGrid) > 0):
	            ?>

					<?php foreach ($hospitalGrid as $hospitalSingle): ?>
						<?php $logoImgUrl = wp_get_attachment_url( get_post_thumbnail_id($hospitalSingle->ID) ); ?>

						<div class="detail <?php echo $partnerNumber; ?>">
							<img src="<?php echo $logoImgUrl; ?>">
						</div>

						<?php $partnerNumber++; ?>

					<?php endforeach;?>

	           	<?php endif; ?>

	        </div>

			<div id="partner-slider" class="detailed-wrap">

				<?php
	                $args = array(
	                    'post_type' => 'hospitalpartners',
	                    'post_status' => 'publish',
	                    'orderby' => 'title',
	                    'order' => 'ASC',
	                    'posts_per_page' => -1,
	                );
	                $hospitalPartners = get_posts ($args);
	            ?>

	            <?php
	                if (count($hospitalPartners) > 0):
	            ?>

					<?php foreach ($hospitalPartners as $hospitalPartner): ?>

						<div class="detail">
							<div class="left">
								<div class="wrap">
									<?php $logoImgUrl = wp_get_attachment_url( get_post_thumbnail_id($hospitalPartner->ID) ); ?>
									<div class="logo"><img src="<?php echo $logoImgUrl; ?>"></div>
									<div class="title"><?php echo $hospitalPartner->post_title; ?></div>
									<div class="location"><?php echo get_field('location', $hospitalPartner->ID); ?></div>
									<div class="blurb"><?php echo $hospitalPartner->post_content; ?></div>
								</div>
								<div class="view-all">< View all partners</div>
							</div>
							<div class="right">
								<div class="stat-wrap">
									<div class="stat">
										<img src="<?php bloginfo( 'template_directory' ); ?>/images/partner-adults-educated.png">
										<span><?php echo get_field('total_adults_educated', $hospitalPartner->ID); ?></span> Total<br/>Adults Educated
									</div>
									<div class="stat">
										<img src="<?php bloginfo( 'template_directory' ); ?>/images/partner-teens-educated.png">
										<span><?php echo get_field('total_teens_educated', $hospitalPartner->ID); ?></span> Total<br/>Teens Educated
									</div>
									<div class="stat">
										<img src="<?php bloginfo( 'template_directory' ); ?>/images/partner-children-educated.png">
										<span><?php echo get_field('total_children_educated', $hospitalPartner->ID); ?></span> Total<br/>Children Educated
									</div>
									<div class="stat last">
										<img src="<?php bloginfo( 'template_directory' ); ?>/images/partner-car-seats.png">
										<span><?php echo get_field('car_seats_distributed', $hospitalPartner->ID); ?></span> Car Seats Distributed
									</div>
								</div>
								<div class="extra">* <?php echo get_field('extra_info', $hospitalPartner->ID); ?></div>
							</div>
						</div>

					<?php endforeach;?>

           		<?php endif; ?>

           		<div class="cycle-prev">
           			<img src="<?php bloginfo( 'template_directory' ); ?>/images/partner-arrow-prev.jpg">
           		</div>
           		<div class="cycle-next">
           			<img src="<?php bloginfo( 'template_directory' ); ?>/images/partner-arrow-next.jpg">
           		</div>

			</div>

		</div>


		
	</div>

</div>

<?php get_footer() ?>
