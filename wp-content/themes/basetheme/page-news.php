<?php
/*
Template Name: News
*/
?>

<?php get_header(); ?>

<div id="pageWrapper">

	<div id="pageTitle">
		<h1>News &amp; Events</h1>
	</div>

	<div id="pageContent" class="clearfix">
		<div id="pageImage">
			<div id="pledgeButton">
				<a href="<?php the_field('pledge_button_link'); ?>"><img src="<?php the_field('pledge_button'); ?>" /></a>
			</div>
			<img src="<?php the_field('page_image'); ?>" class="pageImageLarge"/>
			<img src="<?php the_field('page_image_mobile'); ?>" class="pageImageSmall"/>
		</div>

		<div id="pageNews">

			<div id="pageLeft">
				<h2>News</h2>
			<?php
				$news = new WP_Query(array(
					'post_type' => 'news',
					'posts_per_page' => 10,
					'paged' => $wp_query->get('paged') ? $wp_query->get('paged') : 1
				));

				while ( $news->have_posts() ) :
					$article = $news->next_post();
			?>
				<div class="article">
					<b><?php echo date('F j, Y', strtotime($article->post_date)); ?></b><br>
					<?php if ( get_field('source', $article->ID) ) : ?><i><?php echo get_field('source', $article->ID); ?></i><br><?php endif; ?>
					<a href="<?php echo get_permalink($article->ID); ?>"><?php echo get_the_title($article); ?></a>
				</div>
			<?php
				endwhile;

				hfc_output_pagination(array(
					'total' => $news->max_num_pages,
					'current' => max($wp_query->get('paged'), 1),
					'prev_text' => '&lt;',
					'next_text' => '&gt;',
					'pretty_permalinks' => true
				));
			?>
			</div>

			<?php if (get_field('twitter_widget') || get_field('facebook_widget')) { ?>

			<div id="pageWidgets">

				<?php if (get_field('twitter_widget')) { ?>
				<div class="pageWidget">
					<h2>Tweets</h2>
					<a class="twitter-timeline"  href="https://twitter.com/BuckleUpForLife"  data-tweet-limit="5" data-link-color="#ff0000" width="100%" height="600" data-widget-id="372056826268762112" data-chrome="noheader nofooter transparent">Tweets by @BuckleUpForLife</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
				<?php } ?>

				<?php if (get_field('facebook_widget')) { ?>
				<div class="pageWidget">
					<div class="facebookBox">
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fbuckleupforlife&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=361848313907575" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:62px; background-color:#d9eaf3;" allowTransparency="true"></iframe>
					</div>

					<div class="facebookBox">
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fabrochate&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=361848313907575" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:62px; background-color:#d9eaf3;" allowTransparency="true"></iframe>
					</div>
				</div>
				<?php } ?>

			</div>

			<?php } ?>

			<br style="clear: both"><hr class="shadow">

			<div id="pageBottom">
				<h3>Testimonials</h3>
			<?php
				$testimonials = get_field('testimonials');

				foreach ( $testimonials as $testimonial ) :
			?>
				<div class="testimonial clearfix">
					<img src="<?php echo $testimonial['testimonial_image'] ? $testimonial['testimonial_image']['sizes']['thumbnail'] : bloginfo('template_directory') . '/images/testimonial_image.jpg'; ?>" />
					<p>
						<b><?php echo $testimonial['testimonial_name']; ?></b><br>
						<?php if ( $testimonial['testimonial_title'] ) : ?><i><?php echo $testimonial['testimonial_title']; ?></i><br><?php endif; ?>
						<?php echo $testimonial['testimonial_quote']; ?>
					</p>
				</div>
			<?php
				endforeach;
			?>
			</div>

		</div>

		<?php if (get_field('page_top_content')) { ?>
			<div id="pageTop" class="clearfix">
				<?php the_field('page_top_content'); ?>
			</div>
		<?php } ?>

	</div>

</div>

<?php get_footer() ?>
