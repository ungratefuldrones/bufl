<?php get_header('mp-member'); ?>
    <div class="modules-wrapper">
      <div class="module">
              <div class="module-description">
        <h1><span class="icon-bg icon-archived"></span>Archived Documents</h1>
        </div>
        <div id="tabs" class="tabs">
          <ul class="tabs-nav"><li><a href="#tab-1"><span>Uploaded By You</span></a></li><li><a href="#tab-2"><span>Uploaded By Partners</span></a></li></ul>
          <div id="tab-1" class="tab">
            <div class="search-for-tab">        
              <div class="module-header">
                <div class="subtitle-wrap clearfix">
                  <!--             <h3 class="subtitle">Hospital Partners</h3> -->            
                  <form class="search-portal">
                    <input class="text-input" type="search" name="search-partners" value="Search">
                  </form>
                  <div class="pagination">
                    <span class="pagi-count">1 - 8 of 43</span><div class="pagi pagi-left"><a href="#"><span></span></a></div><div class="pagi pagi-right"><a href="#"><span></span></a></div>
                  </div>
                </div>
              </div></div>
              <div class="table-div table-archived-documents">
                <div class="row-div heading-div">
                  <div class="cell-div">Document Title</div>
                  <div class="cell-div">Uploaded By</div>
                  <div class="cell-div">Created/Modified</div>
                  <div class="cell-div">Category</div>
                  <div class="cell-div">Type</div>
                  <div class="cell-div cell-actions-heading"> </div>
                  <div class="cell-div"> </div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set amet consectetur adisciping...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set amet consectetur adisciping...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set amet consectetur adisciping...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set amet consectetur adisciping...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set amet consectetur adisciping...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
              </div>
            </div>
            <div id="tab-2" class="tab">
             <div class="search-for-tab">        
               <div class="module-header">
                <div class="subtitle-wrap clearfix">
                  <form class="search-portal">
                    <input class="text-input" type="search" name="search-partners" value="Search">
                  </form>
                  <div class="pagination">
                    <span class="pagi-count">1 - 8 of 223</span><div class="pagi pagi-left"><a href="#"><span></span></a></div><div class="pagi pagi-right"><a href="#"><span></span></a></div>
                  </div>
                </div>
              </div></div>
              <div class="table-div table-uploaded-partners">
                <div class="row-div heading-div">
                  <div class="cell-div">Document Title</div>
                  <div class="cell-div">Hospital</div>
                  <div class="cell-div">Uploaded By</div>
                  <div class="cell-div">Created/Modified</div>
                  <div class="cell-div">Category</div>
                  <div class="cell-div">Type</div>
                  <div class="cell-div cell-actions-heading"> </div>
                  <div class="cell-div"> </div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set am</div>
                  <div class="cell-div">Cincinnati Children's Hospital...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set am</div>
                  <div class="cell-div">Cincinnati Children's Hospital...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set am</div>
                  <div class="cell-div">Cincinnati Children's Hospital...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set am</div>
                  <div class="cell-div">Cincinnati Children's Hospital...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
                <div class="row-div">
                  <div class="cell-div">Lorem ipsum dolor set am</div>
                  <div class="cell-div">Cincinnati Children's Hospital...</div>
                  <div class="cell-div">Dr. Maria Gibson, MD</div>
                  <div class="cell-div">Oct. 28, 2014</div>
                  <div class="cell-div">Order Form</div>
                  <div class="cell-div cell-type">PDF</div>
                  <div class="cell-div cell-actions">Actions</div>
                  <div class="cell-div cell-trash"><a class="trigger-overlay" href="#overlay-delete"><div class="icon-trash"></div></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer ('mp'); ?>