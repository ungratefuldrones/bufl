<?php 
/*
 * Template Name: Offline Observation Form
 */
?>
<!DOCTYPE HTML>
<html manifest="/wp-content/themes/basetheme/observation.manifest">
<head>
<title>BUFL | Observation Form</title>
<link rel="stylesheet" href="/wp-content/themes/basetheme/css/observation-form.css" />
<script type="text/javascript" src="//use.typekit.net/jrb8gkh.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7324852/730822/css/fonts.css" />
<script src="/wp-content/themes/basetheme/js/observation-form.js" type="text/javascript"></script>
<script src="/wp-content/themes/basetheme/js/member-portal/observation-form/jquery1.11.2.js" type="text/javascript"></script>

<script type="text/javascript">
function store () {
	localStorage.setItem("bar", document.getElementById('input-test').value)
	alert (localStorage.getItem ('bar'));
}

function clear_this () {	
	document.getElementById('input-test').value = '';
	document.getElementById('input-test1').value = '';
}
function store_and_proceed () {
	var name = document.getElementById('observer-name').value.trim();
	var location = document.getElementById('observation-location').value.trim();
	var month = document.getElementById('observation-month').value.trim();
	var day = document.getElementById('observation-day').value.trim();
	var year = document.getElementById('observation-year').value.trim();
	
	// check for empty fields
	if (0) ; //(name == "" || location== "" || month == "" || day == "" || year == "") alert ( "Please fill out all fields before proceeding" );
	
	// all fields complete, store and proceed with form
	else {
		// store data
		localStorage.setItem("name", name);
		localStorage.setItem("location", location);
		localStorage.setItem("month", month);
		localStorage.setItem("day", day);
		localStorage.setItem("year", year);
		
		
		// show observation form
		$('.form-intro').fadeOut(300, function () {
			$('div.form-wrapper').fadeIn (300);
		});
	}
}

function checkMarkedPerson (e) {
	var $target_icon = $(e.currentTarget);
	var num_clicked = 0;
	
	
	if ($target_icon.hasClass ('icon')) {
		var $parent_box = $target_icon.parents ('.person-box');
		
		num_clicked = $parent_box.find('.clicked').length;
		
		if (num_clicked) {
			$parent_box.addClass ('marked');
		}
		else {
			$parent_box.removeClass ('marked');
		}
	}
	else {		
		num_clicked = $('.person-box .clicked').length;		
	}
	
	return num_clicked;
}

function displayCarCount () {
	var count = localStorage.getItem ('car-count');
	$('.car-count span').text (count);
}

function updateCarCount () {	
	var car_count = localStorage.getItem("car-count");
	localStorage.setItem("car-count", parseInt (car_count)+1);
}

function clearForm () {
	$('.person-box .icon').removeClass ('clicked inactive');
	$('.person-box').removeClass ('marked');
	
}

function storeFormData () {
	
	var store = $.Deferred();
	
	var car_num = localStorage.getItem("car-count");
	
	//var data_object = {};
	
	var $marked = $('.marked');
	
	var people = {};
	
	$marked.each ( function (index, box) {
		var $box = $(this);
		
		people ['person-' + (index+1)] = {};
		
		//people = {};
		var attributes = {};
		
		var $clicked = $box.find('.clicked');
		$clicked.each ( function (i, icon) {

			var category = $(this).attr('data-category');
			var value = $(this).attr('data-value');
			
			attributes [category] = value;
			
		});
		
		people ['person-' + (index+1)] = attributes;
		
	});
	
	//console.log (JSON.stringify (people));
		
	localStorage.setItem('car-' + car_num, people);
	console.log (localStorage.getItem('car-' + car_num));
	
	updateCarCount();
	
	return store.resolve();
}

function init () {
	localStorage.setItem("car-count", 0);
	clearForm();
	displayCarCount();
}

$(document).ready ( function () {
	
	init();
	
	$('.person-box .icon').click ( function (e) {
		if (!$(this).hasClass ('inactive')) {
			if (!$(this).hasClass ('clicked')) {
				$(this).addClass ('clicked');
				
				// deactivate other icons on this row
				$(this).siblings ('.icon').removeClass ('clicked');
			}
			else {
				$(this).removeClass ('clicked');
			}
			checkMarkedPerson(e);
		}
		
	});
	
	$('.button.next').click ( function (e) {
		e.preventDefault();
		
		if (checkMarkedPerson(e)) {
			$.when (storeFormData()).done ( function() {
						
				clearForm();
				displayCarCount();
			});
			
		}
		else {
			alert ('No persons have been marked for this car');
		}
		
	});
});
</script>


</head>

<body>


<header>
	<div class="container">
		<div class="logo-wrapper"><a href="/"><img src="/wp-content/themes/basetheme/images/bufl-observation-fomr-logo.png" /></a></div>
	</div>
	<div class="gray-bar">
		<div class="container">Observation Form</div>
	</div>	
</header>
<div class="container">
	<!-- <form name="test-form">
		<input type="text" class="input" name="input-test" id="input-test" />
		<input type="text" class="input" name="input-test1" id="input-test1" /> <br />
		<div class="submit" onclick="store();">Submit</div>
		<div class="clear" onclick="clear_this();">Clear</div>
	</form> -->
	
	<div class="form-intro">
		<p>Aliquam sed arcu velit. Nam efficitur ligula id interdum porttitor. Pellentesque eleifend ante id sem consequat venenatis. Curabitur quis ex elementum, dictum ipsum a, ultrices ipsum. Praesent facilisis libero eget mi tristique ornare. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas ac scelerisque nunc. Phasellus dapibus eget urna sit amet suscipit. Maecenas porta iaculis ligula in eleifend. Quisque sit amet felis sapien. Quisque mattis euismod libero, non fringilla tortor vulputate nec. Duis neque velit, ornare ut urna ac, condimentum scelerisque ligula. Nam accumsan maximus magna et ullamcorper. Nam tristique nunc sodales lacus porta auctor non sit amet orci. Nunc iaculis, erat quis accumsan volutpat, massa dui ultricies ex, eget congue eros elit quis arcu. </p>
		<div class="form-intro-info">
			<input type="text" id="observation-location" name="observation-location" placeholder="Observation Location" />
			<input type="text" id="observer-name" name="observer-name" placeholder="Observer Name" />
			<p>Date</p>
			<input type="text" id="observation-month" name="observation-month" placeholder="Month" />
			<input type="text" id="observation-day" name="observation-day" placeholder="Day" />
			<input type="text" id="observation-year" name="observation-year" placeholder="Year" />
			<p>Time</p>			
		</div>
		<div class="form-intro-button-wrapper">
			<div class="form-intro-buttons">
				<div class="button-start button" onclick="store_and_proceed();">Start Observation Survey</div>
				<div class="button-download button">Download Printable PDF</div>
			</div>
		</div>
	</div>
	<div class="form-wrapper">
		<p>Aliquam sed arcu velit. Nam efficitur ligula id interdum porttitor. Pellentesque eleifend ante id sem consequat venenatis.</p>
		<div class="bottom">
			<div class="legend-wrapper">
				<h2>Adult/Child</h2>
				<div class="icon adult">Adult</div>
				<div class="icon child">Child</div>
				<div class="icon unknown">Unknown</div>
				<h2>Restrained</h2>
				<div class="icon check">Yes</div>
				<div class="icon no">No</div>
				<div class="icon unknown">Unknown</div>
				<h2>Gender</h2>
				<div class="icon male">Male</div>
				<div class="icon female">Female</div>
				<div class="icon unknown">Unknown</div>
			</div>
			<div class="right">
				<div class="car-count">Car Count: <span class='count'></span></div>
				<div class="form">
					<p>Front of the Car</p>
					<div class="people-boxes">
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
						
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
						
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
						
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
						
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
						
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
						
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
						
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
						
						<div class="person-box">
							<h2>Adult/Child</h2>
							<div>
								<div class="icon adult" data-category="age" data-value="adult"></div>
								<div class="icon child" data-category="age" data-value="child"></div>
								<div class="icon unknown" data-category="age" data-value="unknown"></div>
							</div>
							<h2>Restrained</h2>
							<div>
								<div class="icon check" data-category="restrained" data-value="yes"></div>
								<div class="icon no" data-category="restrained" data-value="no"></div>
								<div class="icon unknown" data-category="restrained" data-value="unknown"></div>
							</div>
							<h2>Gender</h2>
							<div>
								<div class="icon male" data-category="gender" data-value="male"></div>
								<div class="icon female" data-category="gender" data-value="female"></div>
								<div class="icon unknown" data-category="gender" data-value="unknown"></div>
							</div>
						</div>
					</div>
					<a class="button next" href="#next">Next</a>
				</div>
			</div>
			
		</div>
	</div>
	

</div>

</body>

</html>