<?php get_header('mp-member'); ?>
    <div class="modules-wrapper">
      <div class="module">
        <div class="module-description">
          <h1><span class="icon-bg icon-archived"></span>Program Implementation</h1>
          <p>Lorem ipsum dolor site amet, Lorem ipsum dolor site amet, Lorem ipsum dolor site amet, Lorem ipsum dolor site amet,  Lorem ipsum dolor site amet,  Lorem ipsum dolor site amet,  Lorem ipsum dolor site amet,  Lorem ipsum dolor site amet,  Lorem ipsum dolor site amet,  Lorem ipsum dolor site amet,  </p>
        </div>
        <div id="tabs" class="tabs">
          <ul class="tabs-nav"><li><a href="#tab-1"><span>Getting Started</span></a></li><li><a href="#tab-2"><span>Connecting With Partners</span></a></li><li><a href="#tab-3"><span>Preparing for Classes</span></a></li><li><a href="#tab-4"><span>Conducting Classes</span></a></li></ul>
          <div id="tab-1" class="tab">
            <div class="search-for-tab">        
              <div class="module-header">
                <div class="subtitle-wrap clearfix">
                  <form class="search-portal">
                    <input class="text-input" type="search" name="search-partners" value="Search">
                  </form>
                  <div class="pagination">
                    <span class="pagi-count">1 - 8 of 43</span><div class="pagi pagi-left"><a href="#"><span></span></a></div><div class="pagi pagi-right"><a href="#"><span></span></a></div>
                  </div>
                </div>
              </div></div>
              <div class="table-div table-program-implementation table-style-1">
                <div class="row-div heading-div">
                  <div class="cell-div cell-document-heading">Document</div>
                  <div class="cell-div">Title</div>
                  <div class="cell-div">Created/Modified</div>
                  <div class="cell-div">Type</div>
                  <div class="cell-div">Language</div>
                  <div class="cell-div"> </div>
                </div>
                <div class="row-div">
                  <div class="cell-div cell-document"><div class="icon-xls"></div></div>
                  <div class="cell-div">Program Budget</div>
                  <div class="cell-div">Updated: Oct. 28, 2014, 2:23 PM</div>
                  <div class="cell-div cell-type">xls</div>
                  <div class="cell-div cell-language">English Only</div>
                  <div class="cell-div cell-actions">Actions</div>
                </div>
                <div class="row-div">
                  <div class="cell-div cell-document"><div class="icon-doc"></div></div>
                  <div class="cell-div">Program Timeline</div>
                  <div class="cell-div">Updated: Oct. 28, 2014, 2:23 PM</div>
                  <div class="cell-div cell-type">doc</div>
                  <div class="cell-div cell-language">English Only</div>
                  <div class="cell-div cell-actions">Actions</div>
                </div>
              </div>
            </div>
            <div id="tab-2" class="tab">
            Mockup doesn't show this tab.
            </div>
            <div id="tab-3" class="tab">
            Mockup doesn't show this tab.
            </div>
            <div id="tab-4" class="tab">
            Mockup doesn't show this tab.
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer ('mp'); ?>