
<?php get_header(); ?>

<div id="pageWrapper" <?php if ( embed() ) echo "class='embedded-tool'"; ?> >

	<div id="pageTitle">
		<?php $legal = (strpos($_SERVER["REQUEST_URI"], 'legal') ? $legal = true : $legal = false); ?>
		<?php if ($legal) : ?>
			<a class="back-to-tool" href="<?php echo (pll_current_language() == 'en' ? "/".get_embed_url() : "/es/".get_embed_url()); ?>">&#8249; Back to Tool</a>
		<?php endif ?>
		<h1><?php the_title(); ?></h1>
	</div>

	<div id="pageContent" class="clearfix">
		<div id="pageImage">
			<div id="pledgeButton">
				<a href="<?php the_field('pledge_button_link'); ?>"><img src="<?php the_field('pledge_button'); ?>" /></a>
			</div>
			<img src="<?php the_field('page_image'); ?>" class="pageImageLarge"/>
			<img src="<?php the_field('page_image_mobile'); ?>" class="pageImageSmall"/>
		</div>

		<?php if (get_field('page_top_content')) { ?>
			 <div id="pageTop" class="clearfix">
			 
				<?php the_field('page_top_content'); ?>

				<div class="celebrate">
					<div class="share">
						Share
						<!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
							<a class="addthis_button_facebook"></a>
							<a class="addthis_button_twitter"></a>
							<a class="addthis_button_email"></a>
						</div>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-537112717c9a1c63"></script>
						<!-- AddThis Button END -->
					</div>
				</div>

				<iframe src="//www.youtube.com/embed/D6RNQx_iao8" height="360" width="100%" allowfullscreen="" frameborder="0"></iframe>

			</div>
		<?php } ?>


	</div>

</div>

<?php get_footer() ?>
