
<?php get_header(); ?>

<div id="pageWrapper">

	<div id="pageTitle">
		<h1><a href="/about-the-campaign/">Campaign Videos</a></h1>
	</div>

	<div id="pageContent" class="clearfix single-video">

	<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

		<div id="pageArticle">

			<div class="share">
				Share
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
				<a class="addthis_button_facebook"></a>
				<a class="addthis_button_twitter"></a>
				<a class="addthis_button_email"></a>
				</div>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-537112717c9a1c63"></script>
				<!-- AddThis Button END -->
			</div>

			<h3><?php the_title(); ?> (<?php echo get_field('language'); ?>)</h3>

			<?php the_content(); ?>
			
			<iframe width="100%" height="380" src="//www.youtube.com/embed/<?php echo get_field('video-id'); ?>" frameborder="0" allowfullscreen></iframe>

			<?php 
			// check if there are any vignettes
			$vignettes = get_field('associated_vignettes');
			if ($vignettes): 
			?>

				<div class="vignettes">
					<div class="description">Additional Vignettes</div>
					<div class="item-wrap">

						<?php foreach ($vignettes as $index=>$vignette): ?>
							<a href="<?php echo get_permalink($vignette->ID); ?>" class="item">
								<?php $vignetteImg = get_field('image', $vignette->ID); ?>
								<div class="vig-image" style="background-image: url('<?php echo $vignetteImg['url']; ?>');"></div>
								<div class="title"><?php echo $vignette->post_title; ?></div>
								<div class="play">
									<img src="<?php bloginfo('template_directory'); ?>/images/campaign-playbttn.png">
								</div>
							</a>
						<?php endforeach; ?>

					</div>
				</div>

			<?php 
				endif; ?>

		</div>

	<?php endwhile; endif; ?>

	</div>

</div>

<?php get_footer(); ?>