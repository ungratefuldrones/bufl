<div class="overlay overlay-delete" id="overlay-delete">	
	<h2>
		<div class="overlay-title">Delete Email</div>
		<div class="overlay-close" title="close"></div>
	</h2>
	<p>This item will be deleted from the list.</p>
	<div class="button-primary">OK</div>
</div>