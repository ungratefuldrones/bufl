<div class="overlay overlay-login" id="overlay-login">	
	<h2>
		<div class="overlay-title">Member Portal Login</div>
		<div class="overlay-close" title="close"></div>
	</h2>
	<div class="form-wrapper">
		<!-- Custom login for for AJAX processing -->
		<!-- SEE http://natko.com/wordpress-ajax-login-without-a-plugin-the-right-way/ -->
		<form id="loginform" action="login" method="post">	        
	        <p class="status"></p>
	        <p>
	        	<label for="username">Username</label>
	        	<input id="username" type="text" name="username">
	        </p>
	        <p>
	        	<label for="password">Password</label>
	        	<input id="password" type="password" name="password">
	        </p>
	        <p class="login-remember">
				<label>
					<input id="rememberme" type="checkbox" value="forever" name="rememberme">
					Remember Me
				</label>
			</p>
	        <!-- <a class="lost" href="<?php // echo wp_lostpassword_url(); ?>">Lost your password?</a> -->
	        <p class="login-submit">
	        	<input class="submit_button button-primary" type="submit" value="Submit">
	        </p>
	        
	        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
	    </form>
		<a class="trigger-overlay" href="#overlay-forgot-password">Forgot Password?</a>
		
	</div>
</div>