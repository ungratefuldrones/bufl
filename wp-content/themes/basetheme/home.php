<?php
/*
Template Name: Home
*/
?>

<?php
get_header();

if (pll_current_language() == 'en') $english = true;
else $english = false;

// hide mostly everything if we are embedding
if (!embed()) :
?>
<script> heroTxtColor = new Array();</script>

<div id="heroWrapper" class="minWidth">

	<div class="slideArrowRight">
    	<div class="slideArrow"><p>&nbsp;</p></div>
    </div>
    <div class="slideArrowLeft">
    	<div class="slideArrow"><p>&nbsp;</p></div>
    </div>

    <div id="heroNav">
    </div>

    <?php if(get_field('hero')): ?>
	<?php $heroNumber = 1; ?>
	<?php while(has_sub_field('hero')): ?>
    	<?php $attachment_id_hero = get_sub_field('hero_image');
			$image_attributes_hero_large =  wp_get_attachment_image_src( $attachment_id_hero, 'full');
			$image_attributes_hero_small =  wp_get_attachment_image_src( $attachment_id_hero, 'hero-mobile');
		?>
        <div id="heroSlide<?php echo $heroNumber; ?>" class="heroSlide" style="background-image:url(<?php echo $image_attributes_hero_large[0]; ?>);">

<script> heroTxtColor.push("<?php the_sub_field('hero_text_bg_color'); ?>");</script>

        	<div class="heroMobileSlide"><img src="<?php echo $image_attributes_hero_small[0]; ?>"/></div>
            <div class="heroSlideContent">
                <h1><div class="heroTitleDesktop"><?php the_sub_field('hero_title'); ?></div><div class="heroTitleMobile"><?php the_sub_field('hero_mobile_title'); ?></div></h1>
                <p><?php the_sub_field('hero_text'); ?></p>
                <a href="<?php the_sub_field('hero_button_link'); ?>" class="slideBlackButton"><?php the_sub_field('hero_button_label'); ?></a>
            </div>
    	</div>
 		<?php $heroNumber++; ?>
	<?php endwhile; ?>
	<?php endif; ?>
</div> <!-- End heroWrapper -->
<?php endif; ?>
<div id="toolWrapper" class="minWidth <?php if (embed()) { echo "embedded-tool"; } ?>">

    <div id="toolShadow">
    	<p>&nbsp;</p>
    </div>

    <div id="toolBottomShadow">
    	<p>&nbsp;</p>
    </div>

    <div id="tool">

	<a id="embed" href="#embed-code">
		<img src="<?php bloginfo('template_directory'); ?>/images/share-icon.png" />
		<span class="share"><?php echo ($english ? "Share" : "Compartir"); ?></span>
	</a>

	<div style="display: none;" id="embed-code">
		<h2>Share</h2>
		<script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "e45dc517-5fc9-42fa-a766-54552bd0986e", shorten: false, doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
		<div class="share-icons">
			<span class='st_facebook_vcount' displayText='Facebook'></span>
			<span class='st_twitter_vcount'
				st_url="http://buckleupforlife.org"
				st_title="Learn about car seat safety and watch how to properly install your car seat with our animated videos."></span>
		</div>
		<h3>Embed:</h3>
		<textarea readonly="readonly"><iframe src="http://buckleupforlife.org?embed=true" seamless="" scrolling="no" allowtransparency="true" frameBorder="0" style="height:746px; width:340px; overflow:hidden"></iframe></textarea>
	</div>

	<h2><?php the_field('tool_heading'); ?></h2>
        <?php /* <a href="<?php the_field('share_link_url'); ?>" class="shareLink"><?php the_field('share_link_label'); ?></a> */ ?>

        <div id="closeButtonMobile">
            <img src="<?php bloginfo('template_directory'); ?>/images/blank_button.gif" width="28" height="28" />
        </div>

        <div id="toolTabs">
            <div id="tab1" class="tab">
            	<div class="tabInside" style="background-image: url(<?php the_field('find_tab_icon'); ?>);">
                	<img src="<?php bloginfo('template_directory'); ?>/images/tab_arrow_cropped.png" id="arrow1" class="tab-arrow" />
                	<span class="tabLabel"><?php the_field('find_tab_label'); ?></span>
                </div>
	    </div>
            <div id="tab2" class="tab">
            	<div class="tabInside" style="background-image: url(<?php the_field('install_tab_icon'); ?>);">
                	<img src="<?php bloginfo('template_directory'); ?>/images/tab_arrow_cropped.png" id="arrow2" class="tab-arrow" />
                	<span class="tabLabel"><?php the_field('install_tab_label'); ?></span>
                </div>
            </div>
            <div id="tab3" class="tab lastItem">
            	<div class="tabInside" style="background-image: url(<?php the_field('help_tab_icon'); ?>);">
                	<img src="<?php bloginfo('template_directory'); ?>/images/tab_arrow_cropped.png" id="arrow3" class="tab-arrow" />
                	<span class="tabLabel"><?php the_field('help_tab_label'); ?></span>
                </div>
            </div>
        </div>

        <div id="toolSections">
        	<div id="closeButton">
       	    	<img src="<?php bloginfo('template_directory'); ?>/images/blank_button.gif" width="28" height="28" />
            </div>
        	<div id="section1" class="section">
            	<div id="page1-1" class="page clearfix">
                	<div id="page1-1Content">
                    	<div class="picHeading" style="background-image: url(<?php the_field('tab_1_page_1_heading_image'); ?>);"><?php the_field('tab_1_page_1_heading'); ?></div>
                    	<div data-selection="0-2" class="smallButton selectionButton"><span class="largeButtonText"><?php the_field('tab_1_page_1_option_1'); ?></span><span class="smallBottomButtonText"><?php the_field('tab_1_page_1_options_measurement'); ?></span></div>
                        <div data-selection="2-4" class="smallButton selectionButton"><span class="largeButtonText"><?php the_field('tab_1_page_1_option_2'); ?></span><span class="smallBottomButtonText"><?php the_field('tab_1_page_1_options_measurement'); ?></span></div>
                        <div data-selection="4+" class="smallButton selectionButton lastItem"><span class="largeButtonText"><?php the_field('tab_1_page_1_option_3'); ?></span><span class="smallBottomButtonText"><?php the_field('tab_1_page_1_options_measurement'); ?></span></div>
                    </div>
                </div>
                <div id="page1-2" class="page clearfix">
                	<div data-page="page1-1" class="navButton backButton"><?php the_field('tool_back_button_label'); ?></div>
                	<div id="page1-2Content">
                    	<div class="picHeading" style="background-image: url(<?php the_field('tab_1_page_2_heading_image'); ?>);"><?php the_field('tab_1_page_2_heading'); ?></div>
                    	<div data-selection="under height" class="smallButton selectionButton firstItem heightText"><span class="smallTopButtonText"><?php the_field('tab_1_page_2_option_1_measurement'); ?></span><?php the_field('tab_1_page_2_option_1'); ?></div>
                        <div data-selection="over height" class="smallButton selectionButton lastItem heightText"><span class="smallTopButtonText"><?php the_field('tab_1_page_2_option_2_measurement'); ?></span><?php the_field('tab_1_page_2_option_2'); ?></div>
                    </div>
                </div>
                <div id="page1-3" class="page clearfix">
                	<div data-page="page1-2" class="navButton backButton"><?php the_field('tool_back_button_label'); ?></div>
                	<div id="page1-3Content">
                    	<div class="picHeading" style="background-image: url(<?php the_field('tab_1_page_3_heading_image'); ?>);"><?php the_field('tab_1_page_3_heading'); ?></div>
                    	<div data-selection="infant carseat" class="seatButtonMargin seatButton option1"><div class="seatWrapper"><div class="seatImage" style="background-image: url(<?php the_field('tab_1_page_3_option_1_image'); ?>);"></div></div><div class="underText"><?php the_field('tab_1_page_3_option_1'); ?></div></div>
                        <div data-selection="rear carseat" class="seatButton option1"><div class="seatWrapper"><div class="seatImage" style="background-image: url(<?php the_field('tab_1_page_3_option_2_image'); ?>);"></div></div><div class="underText"><?php the_field('tab_1_page_3_option_2'); ?></div></div>
                        <div data-selection="forward carseat" class="seatButton option2 singleButton"><div class="seatWrapper"><div class="seatImage" style="background-image: url(<?php the_field('tab_1_page_3_option_3_image'); ?>);"></div></div><div class="underText"><?php the_field('tab_1_page_3_option_3'); ?></div></div>
                        <div data-selection="booster" class="seatButton option3 singleButton"><div class="seatWrapper"><div class="seatImage" style="background-image: url(<?php the_field('tab_1_page_3_option_4_image'); ?>);"></div></div><div class="underText"><?php the_field('tab_1_page_3_option_4'); ?></div></div>
                        <div data-selection="seatbelt" class="seatButton option4 singleButton"><div class="seatWrapper"><div class="seatImage" style="background-image: url(<?php the_field('tab_1_page_3_option_5_image'); ?>);"></div></div><div class="underText"><?php the_field('tab_1_page_3_option_5'); ?></div></div>
                    </div>
                </div>
                <div class="share">
                    <span class="title">Share</span>
                    <div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php echo get_permalink(); ?>#safety-step1" addthis:title="Buckle Up for Life - Find the right car seat">
                        <a class="addthis_button_facebook"></a>
                        <a class="addthis_button_twitter"></a>
                        <a class="addthis_button_email"></a>
                    </div>
                </div>
            </div>
            <div id="section2" class="section">

            	<div id="page2-1" class="page clearfix">
                	<div data-section="section1" class="navButton backButton"><?php the_field('tool_back_button_label'); ?></div>
                	<div id="page2-1Content">
                    	<div class="picHeading" style="background-image: url(<?php the_field('tab_2_page_1_heading_image'); ?>);"><?php the_field('tab_2_page_1_heading'); ?></div>
                    	<div data-selection="before 2002" class="wideButton selectionButton"><?php the_field('tab_2_page_1_option_1'); ?></div>
                        <div data-selection="after 2002" class="wideButton selectionButton"><?php the_field('tab_2_page_1_option_2'); ?></div>
                    </div>
                </div>

                <div id="page2-2" class="page clearfix">
                	<div data-page="page2-1" class="navButton backButton"><?php the_field('tool_back_button_label'); ?></div>
                	<div id="page2-2Content">
                    	<div class="picHeading" style="background-image: url(<?php the_field('tab_2_page_2_heading_image'); ?>);"><?php the_field('tab_2_page_2_heading'); ?></div>
                        	<div id="videoButtonContainer">
								<?php if(get_field('seats')): ?>
                                <?php while(has_sub_field('seats')): ?>
                                    <div data-height="<?php the_sub_field('seat_height'); ?>" data-carseat="<?php the_sub_field('seat_type'); ?>" data-age="<?php the_sub_field('seat_age'); ?>" data-vehicle="<?php the_sub_field('seat_vehicle_age'); ?>" data-video="<?php the_sub_field('seat_youtube_id'); ?>" class="videoButton">
                                    <div class="videoButtonImage"><img src="<?php the_sub_field('seat_thumbnail'); ?>" width="150" height="80" /></div>
                                    <span class="seatDescription"><?php the_sub_field('seat_name'); ?></span>
                                    <span class="videoDescription"><?php the_sub_field('seat_details'); ?></span>
                              		</div>
                                <?php endwhile; ?>
                                <?php endif; ?>
                          </div>
                    </div>
                </div>

                <div id="page2-3" class="page clearfix">
                	<div data-page="page2-2" class="navButton backButton"><?php the_field('tool_back_button_label'); ?></div>
                	<div id="page2-3Content">
                    	<div id="page2-3Heading"></div>
                        <div id="videoWrapper">
                    		<div id="videoHolder"></div>
                            <a href="" class="helpButton"><?php the_field('tab_2_page_3_install_button_label'); ?></a>
                        </div>
                    </div>
                </div>

                <div class="share">
                    <span class="title">Share</span>
                    <div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php echo get_permalink(); ?>#safety-step2" addthis:title="Buckle Up for Life - How to install your car seat">
                        <a class="addthis_button_facebook"></a>
                        <a class="addthis_button_twitter"></a>
                        <a class="addthis_button_email"></a>
                    </div>
                </div>

            </div>
            <div id="section3" class="section">

            	<div id="page3-1" class="page clearfix">
                    <div data-section="section2" class="navButton backButton"><?php the_field('tool_back_button_label'); ?></div>
                    <div id="page3-1Content">
                        <div class="picHeading" style="background-image: url(<?php the_field('tab_3_heading_image'); ?>);"><?php the_field('tab_3_heading'); ?></div>

                        <div class="locatorForm clearfix">
                            <div class="locatorImage"><img src="<?php the_field('tab_3_form_1_image'); ?>" width="150" height="80" /></div>
                            <div class="locatorText"><?php the_field('tab_3_form_1_text'); ?></div>
                            <div class="locatorFields">
                                <span class="fieldInstructions"><?php the_field('tab_3_form_label'); ?></span>
                                <form method="GET" action="<?php the_field('tab_3_form_1_url'); ?>">
                                    <input class="locateField" type="text" size="5" maxlength="5" name="zip" placeholder="<?php the_field('tab_3_form_placeholder_text'); ?>"></input>
                                    <input value="<?php the_field('tab_3_form_submit_button_label'); ?>" class="locateButton" type="submit">
                                </form>
                            </div>
                        </div> <!-- End locatorFom -->

                        <div class="locatorForm clearfix bottomForm">
                            <div class="locatorImage"><img src="<?php the_field('tab_3_form_2_image'); ?>" width="150" height="80" /></div>
                            <div class="locatorText"><?php the_field('tab_3_form_2_text'); ?></div>
                            <div class="locatorFields">
                                <span class="fieldInstructions"><?php the_field('tab_3_form_label'); ?></span>
                                <form method="GET" action="<?php the_field('tab_3_form_2_url'); ?>">
                                    <input class="locateField" type="text" size="5" maxlength="5" name="zip" placeholder="<?php the_field('tab_3_form_placeholder_text'); ?>"></input>
                                    <input value="<?php the_field('tab_3_form_submit_button_label'); ?>" class="locateButton" type="submit">
                                </form>
                            </div>
                        </div> <!-- End locatorFom -->

                    </div>
                    </div>

                    <div class="share">
                        <span class="title">Share</span>
                        <div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php echo get_permalink(); ?>#safety-step3" addthis:title="Buckle Up for Life - Find help in your area on car seat safety">
                            <a class="addthis_button_facebook"></a>
                            <a class="addthis_button_twitter"></a>
                            <a class="addthis_button_email"></a>
                        </div>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-537112717c9a1c63"></script>
                    </div>
                    
                </div>
                
            </div>
        </div>

    </div>

</div> <!-- End toolWrapper -->

<?php

if (!embed()) :
/*
<div id="missionWrapper">
	<div id="mission">
    	<h2>Cincinnati Children’s and Toyota believe that everyone deserves to be safe.</h2>
        <p>That’s why we created "Buckle Up for Life," or "Abrochate a la Vida," - to help educate communities on critical safety behaviors. Thank you for making child passenger safety a priority. Together, all of us have the ability to help keep our families safe.</p>
    </div>
</div>
*/?>
<div id="resourcesWrapper" class="minWidth">

    <div id="resources" class="clearfix">

    	<div id="resourcesHeading">
       	  <h2><?php the_field('resources_title'); ?></h2>
         	<?php /* <a href="<?php the_field('resources_link_url'); ?>" class="resourcesLink"><?php the_field('resources_link_label'); ?></a> */ ?>
        </div>


        <?php if(get_field('resources')): ?>
        <?php $resourceNumber = 1; ?>
        <?php while(has_sub_field('resources')): ?>
      		<div class="resourceItem <?php if ($resourceNumber == 4) { ?> lastItem <?php } ?>">
            	<?php if (get_sub_field('resource_video')) { ?>
               		<a href="<?php the_sub_field('resource_link'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/play_icon.png" width="49" height="49" class="resourcePlayIcon"/></a>
				<?php } ?>
            	<a href="<?php the_sub_field('resource_link'); ?>" class="fancypdf" target="_blank"><img src="<?php the_sub_field('resource_image'); ?>" class="resourceMainImage"/></a>
                <h3><a href="<?php the_sub_field('resource_link'); ?>" class="fancypdf" target="_blank"><?php the_sub_field('resource_title'); ?></a></h3>
                <p><?php the_sub_field('resource_description'); ?></p>
            </div>
            <?php $resourceNumber++; ?>
        <?php endwhile; ?>
        <?php endif; ?>

    </div>

</div> <!-- End resourcesWrapper -->


<div id="knowWrapper" class="minWidth">

	<div class="slideArrowRight">
    	<div class="slideArrow"><p>&nbsp;</p></div>
    </div>
    <div class="slideArrowLeft">
    	<div class="slideArrow"><p>&nbsp;</p></div>
    </div>

    <div id="knowNav">
    </div>

    <div id="knowShadow">
        <p>&nbsp;</p>
    </div>

    <div id="knowHeading">
      <h2><?php the_field('know_title'); ?></h2>
      <?php /*<a href="<?php the_field('know_link_url'); ?>" class="knowLink"><?php the_field('know_link_label'); ?></a> */ ?>
    </div>

    <?php if(get_field('know_slides')): ?>
	<?php $knowNumber = 1; ?>
	<?php while(has_sub_field('know_slides')): ?>
		<?php $attachment_id = get_sub_field('know_slide_image');
			$image_attributes_large =  wp_get_attachment_image_src( $attachment_id, 'full');
			$image_attributes_small =  wp_get_attachment_image_src( $attachment_id, 'know-mobile');
		?>
        <div id="knowSlide<?php echo $knowNumber; ?>" class="knowSlide" style="background-color: <?php the_sub_field('know_slide_background_color'); ?>;">
        	<div class="knowSlideContent">
                <img src="<?php echo $image_attributes_large[0]; ?>" class="knowSlideImage largeImage"/>
                <img src="<?php echo $image_attributes_small[0]; ?>" class="knowSlideImageMobile"/>
                <p><?php the_sub_field('know_slide_text'); ?></p>
                <?php /* <a href="<?php the_sub_field('know_slide_button_link'); ?>" class="slideOrangeButton"><?php the_sub_field('know_slide_button_label'); ?></a> */ ?>
            </div>
    	</div>
 		<?php $knowNumber++; ?>
	<?php endwhile; ?>
	<?php endif; ?>

</div> <!-- End knowWrapper -->

<div id="highlightsWrapper">

	<div id="highlightsContent" class="clearfix">

   	  <div id="highlightsLeft">
      	<?php /*<h2><?php the_field('highlights_title'); ?></h2> */ ?>
        <div class="highlightSmall">
            <?php if (get_field('highlight_2_link')) { ?>
                <a href="<?php the_field('highlight_2_link'); ?>"><img src="<?php the_field('highlight_2_image'); ?>" height="167" width="296"/></a>
            <?php } else { ?>
                <img src="<?php the_field('highlight_2_image'); ?>" height="167" width="296"/>
            <?php } ?>
        </div>
        <div class="highlightSmall lastItem">
            <?php if (get_field('highlight_3_link')) { ?>
                <a href="<?php the_field('highlight_3_link'); ?>" target="_blank"><img src="<?php the_field('highlight_3_image'); ?>" height="167" width="288"/></a>
            <?php } else { ?>
                <img src="<?php the_field('highlight_3_image'); ?>" height="167" width="288"/>
            <?php } ?>
        </div>
        <div class="highlightLarge">
       		<?php if (get_field('highlight_1_link')) { ?>
            	<a href="<?php the_field('highlight_1_link'); ?>"><img src="<?php the_field('highlight_1_image'); ?>" height="167" width="600"/></a>
			<?php } else { ?>
            	<img src="<?php the_field('highlight_1_image'); ?>" height="167" width="600"/>
            <?php } ?>
        </div>

      </div>

        <div id="highlightsRight">
            <?php if (get_field('in_community_link')) { ?>
            	<a href="<?php the_field('in_community_link'); ?>"><img src="<?php the_field('in_community_image'); ?>" height="351" width="300"/></a>
			<?php } else { ?>
            	<img src="<?php the_field('in_community_image'); ?>" height="351" width="300"/>
            <?php } ?>
        </div>

    </div>

</div> <!-- End highlightsWrapper -->

<div id="socialWrapper">

	<div id="socialContent" class="clearfix">

    	<div id="socialLeft">
        	<h2><a href="/news-events/"><?php the_field('news_title'); ?></a></h2>

            <?php
            	$news = new WP_Query(array(
					'post_type' => 'news',
					'posts_per_page' => 5,
				));

				while ( $news->have_posts() ) :
					$article = $news->next_post();
            ?>
                <div class="newsItem clearfix">
                    <div class="newsTitle">
                        <b><?php echo date('F j, Y', strtotime($article->post_date)); ?></b><br>
						<?php if ( get_field('source', $article->ID) ) : ?><i><?php echo get_field('source', $article->ID); ?></i><br><?php endif; ?>
						<a href="<?php echo get_permalink($article->ID); ?>"><?php echo get_the_title($article); ?></a>
                    </div>
            	</div>
            <?php
            	endwhile;
            ?>

        </div>

        <div id="socialRight">

        	<div class="tabberWrapper">

            <div class="tabberTabsWrapper">
            	<div class="tabberTabs">
            		<div class="tabberTab tabberSelectedTab" data-content="tabberContent1"><?php the_field('social_twitter_title'); ?></div>
                    <div class="tabberTab" data-content="tabberContent2"><?php the_field('social_facebook_title'); ?></div>
                </div>
            </div>

            <div id="tabberContent1" class="tabberContent">
            	<a class="twitter-timeline"  href="https://twitter.com/BuckleUpForLife"  data-tweet-limit="5" data-link-color="#ff0000" width="100%" height="500" data-widget-id="372056826268762112" data-chrome="noheader nofooter transparent">Tweets by @BuckleUpForLife</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>

            <div id="tabberContent2" class="tabberContent">
            	<div class="facebookBox">
                    <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fbuckleupforlife&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=361848313907575" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:62px; background-color:#d9eaf3;" allowTransparency="true"></iframe>
                </div>

                <div class="facebookBox">
                    <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fabrochate&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=361848313907575" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:62px; background-color:#d9eaf3;" allowTransparency="true"></iframe>
                </div>
            </div>

            </div> <!-- End tabberWrapper -->

        </div>

    </div>

</div> <!-- End socialWrapper -->

<div id="signupWrapper" class="minWidth">

    <div id="signupContent" class="clearfix">
    	<h2><?php the_field('signup_title'); ?></h2>
        <p><?php the_field('signup_text'); ?></p>
        <?php if (pll_current_language() == 'en') {
			echo do_shortcode( '[contact-form-7 id="38" title="Newsletter Form"]' );
		} else {
			echo do_shortcode( '[contact-form-7 id="40" title="Spanish Newsletter Form"]' );
		} ?>
    </div>

</div>

<!-- <div id="twitterFeed"></div> -->

<!-- <div id="facebookFeed"></div> -->
<?php endif; // end embed check ?>
<?php get_footer(); ?>