<!doctype html>

<html <?php language_attributes(); ?>>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php bloginfo('name'); ?>  <?php wp_title(); ?></title>

<!-- Meta dependent on what content is being viewed-->

<meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true);
    } else {
        bloginfo('name'); echo " - "; bloginfo('description');
    }
    ?>" />


<?php global $post;
if( is_single() || is_page()) :
	$tags = get_the_tags($post->ID);
	if($tags) :
		foreach($tags as $tag) :
			$sep = (empty($keywords)) ? '' : ', ';
			$keywords .= $sep . $tag->name;
		endforeach;
?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php
	endif;
endif;
?>

<script type="text/javascript" src="//use.typekit.net/jrb8gkh.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7324852/730822/css/fonts.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico?refresh=<?php echo md5(rand(1000,9000)); ?>" />
<?php

	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	wp_head();
?>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43751336-1', 'buckleupforlife.org');
  ga('send', 'pageview');
</script>

<!--jquery-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!--TweenMax-->
<script src="<?php bloginfo('template_directory'); ?>/js/TweenMax.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/TimelineMax.min.js"></script>

<?php if (pll_current_language() == 'en') {
	$globalHeader = 13; //page ID for the english global header page
} else {
	$globalHeader = 18; //page ID for the spanish global header page
} ?>


 <!--Mobile Javascript-->
<?php if (is_page_template('home.php')) { ?>
<script type="text/javascript">
    if (window.outerWidth < 481) {
		var viewPortTag=document.createElement('meta');
		viewPortTag.id="viewport";
		viewPortTag.name = "viewport";
		viewPortTag.content = "user-scalable=0,width=320;";
		document.getElementsByTagName('head')[0].appendChild(viewPortTag);

		$().ready(function() {
        	$('#locatorIframe').height($(window).height() - 143);
		});
		$(window).resize(function() {
			$('#locatorIframe').height($(window).height() - 143);
		});

	} else {
		var viewPortTag=document.createElement('meta');
		viewPortTag.id="viewport";
		viewPortTag.name = "viewport";
		viewPortTag.content = "width=1000";
		document.getElementsByTagName('head')[0].appendChild(viewPortTag);

		$().ready(function() {
        	$('#locatorIframe').height($(window).height() - 152);
		});
		$(window).resize(function() {
			$('#locatorIframe').height($(window).height() - 152);
		});

	}
</script>
<?php } ?>

</head>


	<body class="english">
<?php if (pll_current_language() == 'en') { ?>
	<!-- <body class="english" onLoad="resize_iframe();"> -->
<?php } else {  ?>
	<!-- <body class="spanish" onload="resize_iframe();"> -->
<?php } ?>

<div id="headerWrapper" class="clearfix">
	<div id="header" class="minWidth">

    	<div id="headerLogo">
            <a href="/"><img src="<?php the_field('main_logo', $globalHeader); ?>"></a>
        </div>

        <div id="headerLogoMobile">
            <a href="/"><img src="<?php the_field('main_logo_mobile', $globalHeader); ?>"></a>
        </div>

        <div id="returnButton">
   	   		<a href="/"><?php the_field('go_back_button_label', $globalHeader); ?></a>
        </div>

    </div>
</div>

<div id="iframeBorder">
	<p>&nbsp;</p>
</div>





