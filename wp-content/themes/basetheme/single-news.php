
<?php get_header(); ?>

<div id="pageWrapper">

	<div id="pageTitle">
		<h1><a href="/news-events/">News &amp; Events</a></h1>
	</div>

	<div id="pageContent" class="clearfix">

	<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

		<div id="pageArticle">
			<?php if ( get_post_thumbnail_id() ) : ?>
				<div class="image">
					<?php the_post_thumbnail('large'); ?>
				</div>
			<?php endif; ?>

			<div class="share">
				Share
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
				<a class="addthis_button_facebook"></a>
				<a class="addthis_button_twitter"></a>
				<a class="addthis_button_email"></a>
				</div>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-537112717c9a1c63"></script>
				<!-- AddThis Button END -->
			</div>

			<?php if ( get_field('source') ) : ?>
				<p class="source"><?php echo get_field('source'); ?></p>
			<?php endif;?>

			<h3><a href="<?php echo esc_attr(get_field('link_url')); ?>" target="_blank"><?php the_title(); ?></a></h3>

			<p class="date"><?php echo date('l, F j, Y', strtotime($post->post_date)); ?></p>

			<?php the_content(); ?>

			<?php if ( get_field('link_url') && get_field('link_url') != 'http://' ) : ?>
				<a href="<?php echo esc_attr(get_field('link_url')); ?>" target="_blank" class="link"><?php echo get_field('link_text') ? get_field('link_text') : 'Read More'; ?></a>
			<?php endif; ?>

			<div class="navigation">
				<?php previous_post_link('%link', '< Previous Article') ?> <?php next_post_link('%link', 'Next Article >') ?>
			</div>
		</div>

	<?php endwhile; endif; ?>

	</div>

</div>

<?php get_footer(); ?>
