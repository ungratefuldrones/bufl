
<?php get_header(); ?>

<div id="pageWrapper">

	<div id="pageTitle">
    	<h1>Page Not Found</h1>
    </div>

	<div id="pageContent" class="clearfix">
        
        	 <div id="pageTop" class="clearfix">
        		<h2>The page you were looking for was not found.</h2>
        	</div>

    
    </div>

</div>

<?php get_footer(); ?>