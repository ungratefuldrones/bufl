<?php
/*
Template Name: Pledge
*/
?>

<?php get_header(); ?>

<div id="pageWrapper">

	<div id="pageTitle">
		<h1><?php the_title(); ?></h1>
	</div>

	<div id="pageContent" class="clearfix pledgeContent">
		<div id="pageImage">
			<img src="<?php the_field('page_image'); ?>" class="pageImageLarge"/>
			<img src="<?php the_field('page_image_mobile'); ?>" class="pageImageSmall"/>
		</div>

		<span class="pledgeTitle"><?php the_field('pledge_title'); ?></span>

		<span class="pledgeSub-Title"><?php the_field('pledge_sub-title'); ?></span>

		<div class="pledgeText"><?php the_field('pledge_text'); ?></div>

		<span class="pledgeFormTitle"><?php the_field('pledge_form_title'); ?></span>

		<div id="pledgeForm">

		<script>
		function submitForm() {
		   // Get the first form with the name
		   // Hopefully there is only one, but there are more, select the correct index
		   var frm = document.getElementsByName('contact-form')[0];
		   frm.submit(); // Submit
		   frm.reset();  // Reset
		   return false; // Prevent page refresh
		}
		</script>

		<?php if (pll_current_language() == 'en') {
			echo do_shortcode( '[contact-form-7 id="118" title="Pledge Form"]' );
		} else {
			echo do_shortcode( '[contact-form-7 id="120" title="Pledge Form Spanish"]' );
		} ?>

		</div>

		<span class="pledgeShare"><?php the_field('pledge_share'); ?></span>

		<div class="pledgeShareColumns">
			<div class="pledgeShareSocial">
				<div id="facebookShare" data-status="<?php the_field('pledge_facebook_share_text'); ?>" ><img src="<?php the_field('pledge_facebook_button'); ?>" /></div>
				<div id="twitterShare" data-status="<?php the_field('pledge_twitter_share_text'); ?>" ><img src="<?php the_field('pledge_twitter_button'); ?>" /></div>
			</div>
			<div class="pledgeShareEmail">
				<?php if (pll_current_language() == 'en') {
					echo do_shortcode( '[contact-form-7 id="311" title="Pledge Email Form"]' );
				} else {
					echo do_shortcode( '[contact-form-7 id="312" title="Pledge Email Form Spanish"]' );
				} ?>
			</div>

	</div>

</div>

<?php get_footer(); ?>
