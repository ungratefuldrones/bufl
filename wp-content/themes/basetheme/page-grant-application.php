
<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>

<div id="pageWrapper" <?php if ( embed() ) echo "class='embedded-tool'"; ?> class="grant-application" >
	
	<div id="pageTitle">		
		<h1><?php the_title(); ?></h1>
	</div>
	
	<div id="pageContent" class="clearfix">
		<div class="content-wrapper">
			<?php the_content(); ?>
	
			<?php 
			$sections = get_field ('grant_application_sections');
			foreach ($sections as $index=>$section) {
				?>
			<div class="section-wrapper<?php if ($index == 0) echo ' first'; ?>">
				<a class="trigger-expand" href="#content-<?php echo sanitize_title ($section['grant_application_section_title']); ?>"><?php echo $section['grant_application_section_title']; ?></a>
				<div class="content-expand" id="content-<?php echo sanitize_title ($section['grant_application_section_title']); ?>">
					<?php echo $section ['grant_application_section_content']; ?>
				</div>
			</div>
				<?php 
			}
			?>
			<hr />
			
			<div class="view-on-mobile-blurb">We recommend that you fill out this application on a desktop computer. <a class="show-form" href="#show-form">View the form.</a></div>
			
			<?php 
			$form_id = 7;
			if (strpos ($_SERVER['SERVER_NAME'], 'hfwebdev.com') !== false) $form_id = 3;
			?>
			
			<div class="form-wrapper">
				<a href="<?php echo get_field ('grant_application_pdf_attachment'); ?>" class="download-pdf" target="_blank">Download Application</a>
				
			<?php echo FrmEntriesController::show_form($form_id, $key = '', $title=false, $description=false); ?>
			
			</div>
		</div>
	</div>
	

</div>

	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer() ?>
