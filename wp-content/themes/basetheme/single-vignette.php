
<?php get_header(); ?>

<div id="pageWrapper">

	<div id="pageTitle">
		<h1><a href="/about-the-campaign/">Campaign Vignettes</a></h1>
	</div>

	<div id="pageContent" class="clearfix single-video">

	<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

		<div id="pageArticle">

			<div class="share">
				Share
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
				<a class="addthis_button_facebook"></a>
				<a class="addthis_button_twitter"></a>
				<a class="addthis_button_email"></a>
				</div>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-537112717c9a1c63"></script>
				<!-- AddThis Button END -->
			</div>

			<h3><?php the_title(); ?></h3>

			<?php the_content(); ?>
			
			<iframe width="100%" height="380" src="//www.youtube.com/embed/<?php echo get_field('video-id'); ?>" frameborder="0" allowfullscreen></iframe>

			<a href="/about-the-campaign/" class="back">< Back to Campaign</a>

		</div>

	<?php endwhile; endif; ?>

	</div>

</div>

<?php get_footer(); ?>