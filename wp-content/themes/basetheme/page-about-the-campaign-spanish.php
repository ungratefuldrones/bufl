
<?php get_header(); ?>

<div id="pageWrapper" <?php if ( embed() ) echo "class='embedded-tool'"; ?> >

	<div id="pageTitle">
		<?php $legal = (strpos($_SERVER["REQUEST_URI"], 'legal') ? $legal = true : $legal = false); ?>
		<?php if ($legal) : ?>
			<a class="back-to-tool" href="<?php echo (pll_current_language() == 'en' ? "/".get_embed_url() : "/es/".get_embed_url()); ?>">&#8249; Back to Tool</a>
		<?php endif ?>
		<h1><?php the_title(); ?></h1>
	</div>

	<div id="pageContent" class="clearfix">
		<div id="pageImage">
			<img src="<?php the_field('page_image'); ?>" class="pageImageLarge"/>
			<img src="<?php the_field('page_image_mobile'); ?>" class="pageImageSmall"/>
		</div>

		<div class="campaign-wrap">

			<h2>Televisi&oacute;n</h2>

			<div class="television">

				<?php 
				// check if there are any videos
				$videos = get_field('television');
				if ($videos): 
				?>
					<div class="videos">
						<div class="left">
							<?php $featureVidNumber = 1; ?>
							<?php foreach ($videos as $index=>$video): ?>
								<?php 
								if ( $featureVidNumber == 1 ) {
								?>
									<iframe width="100%" height="265" src="//www.youtube.com/embed/<?php echo get_field('video-id', $video->ID); ?>" frameborder="0" allowfullscreen></iframe>
									<div class="title"><a href="<?php echo get_permalink($video->ID); ?>"><?php echo $video->post_title; ?> (<?php echo get_field('language', $video->ID); ?>)</a></div>
								<?php 
								} 
								$featureVidNumber++;
								?>
							<?php endforeach; ?>
						</div>
						<div class="right">
							<?php $otherVidNumber = 1; ?>
							<?php foreach ($videos as $index=>$video): ?>
								<?php 
								if ( $otherVidNumber != 1 ) {
								?>
									<div class="item">
										<?php $vidImg = get_field('image', $video->ID); ?>
										<a href="<?php echo get_permalink($video->ID); ?>" class="image" style="background-image: url('<?php echo $vidImg['url']; ?>')">
										</a>
										<div class="text">
											<div class="language"><?php echo get_field('language', $video->ID); ?></div>
											<div class="title"><?php echo $video->post_title; ?></div>
										</div>
									</div>
								<?php 
								} 
								$otherVidNumber++;
								?>
							<?php endforeach; ?>
						</div>
					</div>

					<?php $vigNumber = 1; ?>
					<?php foreach ($videos as $index=>$video): ?>
						<?php 
						if ( $vigNumber == 1 ) {
						?>
							<?php 
							// check if there are any vignettes
							$vignettes = get_field('associated_vignettes', $video->ID);
							if ($vignettes): 
							?>
								<div class="vignettes">
									<div class="description">Videos adicionales</div>
									<div class="item-wrap">

										<?php foreach ($vignettes as $index=>$vignette): ?>
											<a href="<?php echo get_permalink($vignette->ID); ?>" class="item">
												<?php $vignetteImg = get_field('image', $vignette->ID); ?>
												<div class="image" style="background-image: url('<?php echo $vignetteImg['url']; ?>');"></div>
												<div class="title"><?php echo $vignette->post_title; ?></div>
												<div class="play">
													<img src="<?php bloginfo('template_directory'); ?>/images/campaign-playbttn.png">
												</div>
											</a>
										<?php endforeach; ?>

									</div>
								</div>

							<?php 
								endif; ?>
						<?php 
						} 
						$vigNumber++;
						?>
					<?php endforeach; ?>

				<?php 
					endif; ?>
			</div>

			<h2>Radio</h2>

			<div class="radio">
				<?php if(get_field('radio')): ?>
	                <?php while(has_sub_field('radio')): ?>
	                <div class="item">
	                	<div class="left">
	                		<?php $mp3Url = get_sub_field('mp3'); ?>
	                		<audio src="<?php echo $mp3Url['url']; ?>" type="audio/mpeg" /></audio>
	                	</div>
	                	<div class="right">
	                		<span class="language"><?php the_sub_field('language'); ?></span>
	                   		<span class="title"><?php the_sub_field('title'); ?></span>
	                	</div>
	              	</div>
	                <?php endwhile; ?>
	            <?php endif; ?>
	        </div>

		</div>


		
	</div>

</div>

<?php get_footer() ?>
