<?php
/*
Plugin Name: Custom Role
Description: Adds a custom role
Version: 1
Author: Home Front
*/

if( ! class_exists('Custom_Role') ) {

	/**
	 * @author Tz Weiner
	 */
	class Custom_Role {

		private $parent_slug;
		private $slug;
		private $title;
		private $dir_path;
		private $old_category;
		private $new_category;

		public function __construct ( ) {
			
			
			$this->parent_slug = 'tools.php';
			$this->title = __('Custom Role', __CLASS__);
			$this->slug = __CLASS__;			
		}
		
		
		/**
		 * Import Form
		 */
		public function plugin_form ( ) {
		
		?>
			<style type="text/css">
				#import label {
					width: 10em;
					float: left;
				}
				#import .text {
					width: 30em;
				}
				#import p, #import pre {
					clear: left;
				}
			</style>
			<div id="import" class="wrap">
			
				<?php
				if ( isset($_POST["update_go"]) && $_POST["update_go"] != "" &&
						isset($_POST["role_in"]) && $_POST["role_in"] != "" ) :
			?>
				
				<pre><?php
					$this->update('Update Started.');

					$this->setRole ($_POST["role_in"]);
					
					$this->update('Update Done.');
				?></pre>
				</div>
			<?php
				else:
			?>
				<h2><?php echo $this->title; ?></h2>

				<form action="<?php echo $this->parent_slug; ?>?page=<?php echo $this->slug; ?>" method="post">
					<p>Add a custom role to the site</p>
					<p>
						<label for="year_in">New role:</label>
						<input type="text" name="role_in" id="role_in" value="<?php if(isset ($_POST['role_in'])) echo $_POST['role_in']; ?>" />
					</p>
					
					<p>
						<input type="submit" name="update_go" id="update_go" class="button-primary" value="<?php echo __( "Add Role", __CLASS__ ); ?>" />
					</p>
				</form>
			<?php
				endif;
			?>
			</div>
			<?php
		}
				

		/**
		 * admin_menu hook
		 *
		 * Adds an admin menu item for plugin
		 */
		public function admin_menu ( ) {

			$capability = 'edit_users';
			$function = array($this, 'plugin_form'); // Calls a function in this class
			add_submenu_page($this->parent_slug, $this->title, $this->title, $capability, $this->slug, $function);
		}

		
		/**
		 * renameCategory
		 *
		 * @param string $database
		 */
		private function setRole ($new_role) {
			global $current_user;
		
			set_time_limit(3600); // Give us 1 hour
		
			//remove_role( 'role-name' ); 
			add_role( sanitize_title ($new_role), __( $new_role ), array( 'read' => true ) );
		}
		
		
		
		/**
		 * Updates the display with a message
		 *
		 * @param string $message
		 */
		private function update ( $message ) {
		
			echo htmlspecialchars($message) . "\n";
		}
	}

}

if( class_exists('Custom_Role') ) {

	$custom_role = new Custom_Role();

	// add a admin menu option
	add_action('admin_menu', array(&$custom_role, 'admin_menu'));
}

?>